/*
 * trace.h
 *
 *  Created on: 12 ���. 2016 �.
 *      Author: Denis
 */

#ifndef TRACE_H_
#define TRACE_H_

#include "semihosting/Trace.h"

#ifdef DEBUG
#define PRINTF(...)			trace_printf(__VA_ARGS__)
#define PUTS(str)			trace_puts(str)
#define PUTC(c)				trace_putchar(c)
#else
#define PRINTF(...)
#define PUTS(str)
#define PUTC(c)
#endif


#endif /* TRACE_H_ */
