/*
 * common.h
 *
 *  Created on: 1 ���. 2016 �.
 *      Author: Denis Shreiber
 */

#ifndef COMMON_H_
#define COMMON_H_

#include <mbed.h>

#define USE_BARRIER_FEEDBACK				0

// Config default values
#define BEEPER_ON_DEF						false
#define ADVERTISE_DEF						true
#define SCAN_DEF							false
#define ADV_INTERVAL_DEF					20
#define AUTHORISATION_TIMEOUT_DEF			6		// sec
#define ATT_MTU_DEF							247
#define MIN_CONN_INTERVAL_MS_DEF			15.0f
#define MAX_CONN_INTERVAL_MS_DEF			125.0f
#define SLAVE_LATENCY_DEF					0
#define CONN_SUP_TIMEOUT_MS_DEF				2000.0f

#define DFU_FIRMWARE_MAX_SIZE				1024*1024
#define EVENTS_FLASH_ADDR					0x080C0000	// sector 10 (total 11)

#define TX_POWER_VALUES_NUMBER				9


#define FORMAT_SDCARD_CMD					1


#define BLE_TASK_STACK_SIZE					0x1000
#define NET_TASK_STACK_SIZE					0x4000

typedef enum {
	TASK_MAIN	= 0x1,
	TASK_BLE	= 0x2,
	TASK_NET	= 0x4,

	TASKS_NUM	= 3,

	TASK_ALL	= -1,
}Tasks_t;

enum ServiceMode {
	NORMAL_MODE,
	SERVICE_MODE
};

enum DeviceType {
	DEVICE_TYPE_IN,
	DEVICE_TYPE_OUT,
	DEVICE_TYPE_OFF,

	DEVICE_TYPE_NUM
};

enum NetMode {
	NET_MODE_DHCP,
	NET_MODE_STATIC,

	NET_MODE_NUM
};

enum LogsMode {
	LOGS_MODE_DISABLED,
	LOGS_MODE_ENABLED,

	LOGS_MODE_NUM
};

typedef struct {
	uint32_t devID;		/// @note Важно, чтобы он лежал первый в стуктуре настроек
	uint8_t dev_type;
	uint8_t dev_number;
	uint32_t ip;
	uint32_t gateway;
	uint32_t mask;
	uint8_t mac[6];
	uint32_t dns1;
	uint32_t dns2;
	uint8_t net_mode;				// Режим подклбчения к сети
	uint8_t con_timeout;			// Таймаут подключения
	int8_t tx_pwr;					// Мощность сигнала
	int8_t beac_trh;				// Порог сигнала бикона
	uint8_t logs_mode;				// включено или отключено логирование
	uint8_t enable_dist_sensor;		// вкл/выкл датчик расстояния
	uint8_t enable_gate_feedback;	// вкл/выкл обратная связь шлагбаума
	uint16_t dist_sensor_threshold;	// Порог срабатывания датчика расстояния
}Sett_t;

typedef struct __attribute__((packed)) {
	char billing;
	char type;
}users_t;

typedef struct __attribute__((packed)) {
	char mac[6];
	uint32_t id;
}beacons_t;

typedef struct __attribute__((packed)){
	uint8_t uuid[16];
	time_t tm;
	bool isUser;
	bool isBeacon;
}events_t;

typedef struct __attribute__((packed)){
	uint32_t user_id;
	users_t user;
}usersEvents_t;

typedef struct __attribute__((packed)){
	uint32_t id;
	char billing;
}sessions_t;

typedef enum {
	LED_SD,
	LED_NET,
	LED_BLE,
	LED_HEALTH
}LedType_t;

typedef enum {
	LED_ON = 0,
	LED_OFF = 1
}LedState_t;

typedef struct {
	DigitalOut led;
	uint32_t tr;
	LedState_t state;
}Led_t;

typedef struct {
	bool beeper;
	bool advertise;
	bool scan;
	uint16_t adv_interval;
	uint8_t auth_timeout;
	uint8_t att_mtu;
	float min_conn_interval;		// ms
	float max_conn_interval;		// ms
	uint16_t slave_latency;
	float supervision_timeout;		// ms
}Config_t;

enum OsSignals {
	SERVICE_MODE_ON = 0x01,
	SERVICE_MODE_OFF = 0x02,
	SHUTDOWN_MODE_ON = 0x04,
	SD_CLEANUP = 0x08,
	NET_READY_FOR_SD_CLEANUP = 0x10,
	SD_CLEANUP_END = 0x20,
	LOG_UPLOAD = 0x40,
	LOG_UPLOADED = 0x80,
};

// ==============================================================

/**
 * Attribute to put data into external SRAM
 * @example __ESRAM char buf[16];
 */
#define __ESRAM __attribute__ ((section (".esram")))
/**
 * Attribute to put data into CCM (64K core coupled memory)
 * @example __CCM char buf[16];
 */
#define __CCM __attribute__ ((section (".ccm")))



#endif /* COMMON_H_ */
