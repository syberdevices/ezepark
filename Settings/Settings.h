/*
 * Settings.h
 *
 *  Created on: 17 авг. 2016 г.
 *      Author: Denis
 */

#ifndef SETTINGS_H_
#define SETTINGS_H_

#include <mbed.h>
#include <rtos.h>
#include <crc32/crc32.h>

extern "C" {
#include <string.h>
#include <trace/trace.h>
}

// Значение из HAL
#define FLASH_TIMEOUT_VALUE       ((uint32_t)50000U)/* 50 s */

template <typename T>
class Settings {
public:
	Settings(void(*dflt)(void *data), int page, int addr) : isDefault(true), _dflt(dflt), _page(page), _addr(addr), _changed(false) {
		set_default();
	}

	/**
	 * Загрузить натсройки из ПЗУ. Если в ПЗУ ничего нет, то установить поумолчанию
	 * @param	set		Объект для копирования настроек
	 */
	void load() {
		_mutex.lock();

		uint32_t crc_calc = _crc.get((void*)_addr, sizeof(T));
		uint32_t crc_written = *(uint32_t*)&(((uint8_t*)_addr)[sizeof(T)]);

		if (crc_calc == crc_written) {
			core_util_critical_section_enter();
			memcpy(&data, (void*)_addr, sizeof(T));
			core_util_critical_section_exit();
			isDefault = false;
		}
		else {
			set_default();
		}

		_mutex.unlock();
	}

	/**
	 * Сохранить настройки в ПЗУ
	 * @param	set		Объект с настройками
	 * @retval	true	Настройки сохранились успешно
	 * @retval	false	Ошибка записи в ПЗУ
	 */
	bool save() {
		bool ret = true;
		uint32_t written = 0;
		T tmp;

		union Crc {
			uint32_t _u32;
			uint8_t _u8[4];
		}crc;

		core_util_critical_section_enter();
		memcpy(&tmp, &data, sizeof(T));
		core_util_critical_section_exit();

		crc._u32 = _crc.get(&tmp, sizeof(T));

		_mutex.lock();
		HAL_FLASH_Unlock();

		if (FLASH_WaitForLastOperation((uint32_t)HAL_FLASH_TIMEOUT_VALUE) != HAL_OK) {
			ret = false;
			goto exit;
		}

		FLASH_Erase_Sector(_page, VOLTAGE_RANGE_1);

		while (sizeof(T) > written) {
			if (HAL_FLASH_Program(TYPEPROGRAM_BYTE, _addr + written, ((uint8_t*)(&tmp))[written]) != HAL_OK) {
				ret = false;
				goto exit;
			}
			written++;
		}

		for (int q = 0; q < 4; q++) {
			if (HAL_FLASH_Program(TYPEPROGRAM_BYTE, _addr + written, crc._u8[q]) != HAL_OK) {
				ret = false;
				goto exit;
			}
			written++;
		}

		exit:

		HAL_FLASH_Lock();

		if (ret) {
			isDefault = false;
		}

		_mutex.unlock();

		return ret;
	}

	/**
	 * Записать данные фаулта во флэш
	 * @param str Строка с символом конца строки
	 * @return
	 */
	bool write_fault_data(const char * str) {
		bool ret = true;
		uint32_t len = 0;
		uint32_t written = 0;
		int addr = 0;
		const char *ptr = NULL;

		// Бэкап вместе с crc
		memcpy(&array, (void*)_addr, sizeof(array));

		HAL_FLASH_Unlock();

		if (FLASH_WaitForLastOperation((uint32_t)HAL_FLASH_TIMEOUT_VALUE) != HAL_OK) {
			ret = false;
			goto exit;
		}

		FLASH_Erase_Sector(_page, VOLTAGE_RANGE_1);

		// Восстанавливаем данные из бэкапа
		while (sizeof(array) > written) {
			if (HAL_FLASH_Program(TYPEPROGRAM_BYTE, _addr + written, array[written]) != HAL_OK) {
				ret = false;
				goto exit;
			}
			written++;
		}

		// Пишем контрольное слово "fault"
		written = 0;
		addr = _addr + sizeof(array);
		ptr = "fault";
		len = strlen(ptr);

		while (len > written) {
			if (HAL_FLASH_Program(TYPEPROGRAM_BYTE, addr + written, ptr[written]) != HAL_OK) {
				ret = false;
				goto exit;
			}
			written++;
		}

		// Пишем данные фаулта
		len = strlen(str) + 1;	// Вместе с символом '\0';
		addr = addr + written;
		written = 0;

		while (len > written) {
			if (HAL_FLASH_Program(TYPEPROGRAM_BYTE, addr + written, str[written]) != HAL_OK) {
				ret = false;
				goto exit;
			}
			written++;
		}

		exit:

		HAL_FLASH_Lock();

		return ret;
	}

	/**
	 * Прочитать данные файлта из флэш
	 * @return Указатель на облась во флэш
	 */
	const char * read_fault_data() {
		const char * pdata = (const char *)(_addr + sizeof(array));

		if ( memcmp(pdata, "fault", strlen("fault")) == 0 ){
			pdata += strlen("fault");

			// Данные есть. Теперь очищаем заголовок.
			clear_fault_data();
		}
		else {
			pdata = NULL;
		}

		return pdata;
	}

	/**
	 * Очистить данные фаулта
	 * @note Необходимо вызывать после чтения
	 * @note В качестве очистки символ конца строки
	 * @return
	 */
	bool clear_fault_data() {
		bool ret = true;
		uint32_t written = 0;

		// Бэкап вместе с crc
		memcpy(&array, (void*)_addr, sizeof(array));

		HAL_FLASH_Unlock();

		if (FLASH_WaitForLastOperation((uint32_t)HAL_FLASH_TIMEOUT_VALUE) != HAL_OK) {
			ret = false;
			goto exit;
		}

		FLASH_Erase_Sector(_page, VOLTAGE_RANGE_1);

		// Восстанавливаем данные из бэкапа
		while (sizeof(array) > written) {
			if (HAL_FLASH_Program(TYPEPROGRAM_BYTE, _addr + written, array[written]) != HAL_OK) {
				ret = false;
				goto exit;
			}
			written++;
		}

		// Очищаем данные
		if (HAL_FLASH_Program(TYPEPROGRAM_BYTE, _addr + sizeof(array), '\0') != HAL_OK) {
			ret = false;
			goto exit;
		}

		exit:

		HAL_FLASH_Lock();

		return ret;
	}

	/**
	 * Установить натсройки по умолчанию
	 */
	void set_default() {
		if (!_dflt) {
			PUTS("Settings::set_default() = NULL");
			while(1);
		}

		_dflt(&data);

		isDefault = true;
	}

	void resetSettings() {
		core_util_critical_section_enter();
		set_default();
		core_util_critical_section_exit();
		save();
	}

	bool changed() {
		return _changed;
	}

	void set_changed_flag(bool val) {
		if (val != _changed) {
			core_util_critical_section_enter();
			_changed = val;
			core_util_critical_section_exit();
		}
	}

	bool isDefault;

	union {
		T data;
		uint8_t array[sizeof(T) + 4];	// Настройки + crc
	};


private:
	void(*_dflt)(void *data);
	Mutex _mutex;
	CRC32 _crc;
	int _page;
	int _addr;
	bool _changed;
};

#endif /* SETTINGS_H_ */
