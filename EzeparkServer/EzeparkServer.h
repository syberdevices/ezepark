/*
 * EzeparkServer.h
 *
 *  Created on: 24 авг. 2016 г.
 *      Author: Denis
 */

#ifndef EZEPARKSERVER_H_
#define EZEPARKSERVER_H_

#include <mbed.h>
#include <rtos.h>
#include <EthernetInterface.h>
#include <HTTPClient/HTTPClient.h>
#include <common.h>
#include <DataBase/DataBase.h>

// ===========================================================

class FwHTTPText : public HTTPText {
public:
	uint32_t lengthOfFWfile;
	uint32_t _written;
	/** Create an HTTPText instance for output
	* @param str String to be transmitted
	*/
	FwHTTPText(char* str);
	/** Create an HTTPText instance for input
	* @param str Buffer to store the incoming string
	* @param size Size of the buffer
	*/
	FwHTTPText(char* str, size_t size);

protected:
	void writeReset();
	virtual int write(const char* buf, size_t len);
	virtual void setDataLen(size_t len);
};

// -----------------------------------------------------------

class LogHTTPText : public HTTPText {
public:
	LogHTTPText();

	void resetData();

protected:
	char _boundary[16*2 + 16];	// md5
	bool _fopened;
	bool _completed;
	unsigned int _logFileIndex;

	virtual int read(char* buf, size_t len, size_t* pReadLen);
	virtual int getDataType(char* type, size_t maxTypeLen);
	virtual size_t getDataLen();
	virtual bool getIsChunked();
};

//------------------------------------------------------------

class syncHTTPText : public HTTPText {
public:
	syncHTTPText();

protected:
	virtual int read(char* buf, size_t len, size_t* pReadLen);
	virtual bool getIsChunked();
	virtual int write(const char* buf, size_t len);
};

// ===========================================================

class EzeparkServer {
public:
	EzeparkServer(PinName reset);

	bool init(uint8_t *mac = NULL);

	bool connect(unsigned int timeout_ms = 15000);

	bool isConnected();

	void disconnect();

//	bool init(uint32_t ip, uint32_t gateway, uint32_t mask, uint32_t dns1, uint32_t dns2, uint8_t *mac = NULL);

	bool reset();

	/// --- API ---

	char* getTime();

	int check();

//	char* endSession(const char* ptx);

	int sync(DataBase::SyncMode sync);

	int getFirmware();

//	int getBLEfirmware();

	int uploadLogs();

	/// ---  ---

	bool isHaveFirmwareUpdate();

	void SetMode(NetMode mode);

	uint32_t getIPu32();
	char* getIPstr();
	void setIP(uint32_t ip);

	uint32_t getGatewayu32();
	char* getGatewayStr();
	void setGateway(uint32_t gateway);

	uint32_t getMasku32();
	char* getMaskStr();
	void setMask(uint32_t mask);

	uint32_t getDNS1u32();
	char* getDNS1Str();
	void setDNS1(uint32_t dns1);

	uint32_t getDNS2u32();
	char* getDNS2Str();
	void setDNS2(uint32_t dns2);

	uint8_t* getMAC();
	void setMAC(const uint8_t *mac);

	bool needReset();

	void reInit();

	bool isLink();
	bool connectedToServer();

	uint32_t token(uint32_t deviceid, uint32_t time);

	bool isCableConnected();

	/**
	 * Проверка PHY через режим Loopback
	 * @return	true/false
	 * @note	Метод выполняет проверку физики только один первый раз независимо от того, сколько раз его вызвать
	 */
	bool checkPHY();

	void disablePHY();

	void enPHY();

	void setTimeout(uint32_t val);

	uint32_t getTimeout();

	void isBusyNet(bool state);

	bool isBusy();

	char fileName[30];

private:
	DigitalOut _reset;
	Mutex _mutex;
	Mutex _mutexForcheck;
	Mutex _settMutex;

	EthernetInterface _eth;
	HTTPClient _http;
	HTTPClient _httplog;
	FwHTTPText _firmhttpin;
	HTTPText   _httpin;

	bool _needReset;

	bool _isLink;
	bool _isLinkWithoutError;
	bool _isCableConnected;
	bool _isPHYChecked;
	bool _isConnected;

	NetMode _mode;
	uint32_t _ip;
	uint32_t _gateway;
	uint32_t _mask;
	uint32_t _dns1;
	uint32_t _dns2;
	uint8_t _mac[6];
	uint32_t _timeout;

	const char *_timeurl;
	const char *_checkurl;
	const char *_syncurl;
	const char *_endurl;
	const char *_firmwareUrl;
	const char *_logsUrl;
	char _url[128];
	char _rxBuf[10];

	bool _isBusyLog;
	bool _isBusyNet;

//	char _txbuf[1024];

	void setLink(bool stat);
	void setLinkWithoutError(bool stat);
	bool checkCable();
};

extern EzeparkServer EzServer;


#endif /* EZEPARKSERVER_H_ */
