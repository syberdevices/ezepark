/*
 * EzeparkServer.cpp
 *
 *  Created on: 24 авг. 2016 г.
 *      Author: Denis
 */

#include "EzeparkServer.h"
#include <MyTime/MyTime.h>
#include <Settings/Settings.h>
#include <Watchdog/Watchdog.h>
#include <DataBase/DataBase.h>
#include <crc32/crc32.h>
#include <common.h>

#include <lwip/dns.h>
#include <SdCard/SdCard.h>
#include <cstring>
#include <trace/trace.h>

#define USE_PRINT_DBG	0

#if !USE_PRINT_DBG
#undef PRINTF
#undef PUTS
#undef PUTC

#define PRINTF(...)
#define PUTS(str)
#define PUTC(c)
#endif

extern uint8_t *p_DFU_Data;
extern Settings<Sett_t> settings;
extern SdCard sdcard;
extern Watchdog wdg;

extern "C" {
extern uint8_t PHYstatus;
int reInitPhy();
}

enum PHY_STATUSES {
	PHY_CHECK = 0x1,
	CABLE_CHECK = 0x2,
	PHY_DISABLE = 0x4,
	PHY_EN = 0x8
};

// ==========================================================

FwHTTPText::FwHTTPText(char* str) : HTTPText(str) {

}

FwHTTPText::FwHTTPText(char* str, size_t size) : HTTPText(str, size) {
	lengthOfFWfile = 0;
}

/*virtual*/void FwHTTPText::writeReset() {
	_written = 0;
	lengthOfFWfile = 0;
}

/*virtual*/int FwHTTPText::write(const char* buf, size_t len) {
	wdg.kick(TASK_NET);
	if (lengthOfFWfile > DFU_FIRMWARE_MAX_SIZE - 4) {
		return false;
	}
	if (_written + len > lengthOfFWfile) {
		return false;
	}

	memcpy(p_DFU_Data + 4 + _written, buf, len);
	_written += len;
	return true;
}

/*virtual*/ void FwHTTPText::setDataLen(size_t len) { //From Content-Length header, or if the transfer is chunked, next chunk length
	lengthOfFWfile = len;
	if (lengthOfFWfile <= DFU_FIRMWARE_MAX_SIZE - 4) {
		memcpy(p_DFU_Data, &len, sizeof(uint32_t));
	}
}

// -----------------------------------------------------------

LogHTTPText::LogHTTPText() : HTTPText(NULL) {
	strcpy(_boundary, "----WebKitFormBoundary7MA4YWxkTrZu0gW");
	_fopened = false;
	_completed = false;
	_logFileIndex = 0;
}

void LogHTTPText::resetData() {
	sdcard.closeLogFile();
	_fopened = false;
	_completed = false;
	_logFileIndex = 0;
}

/*virtual*/ int LogHTTPText::read(char* buf, size_t len, size_t* pReadLen) {
	int res = 0;
	char *pbuf = buf;
	*pReadLen = 0;

	wdg.kick(TASK_NET);

	if (!_completed) {
		repeat:

		if (!_fopened) {
			const char *fname = sdcard.openNewestLogFile((SdCard::LogType_t)_logFileIndex);
			if (fname) {
				_fopened = true;

				res = snprintf(pbuf, len, "\r\n--%s\r\n", _boundary);
				*pReadLen += res;

				res = snprintf(pbuf += res, len -= res, "Content-Disposition: form-data; name=\"file[%u]\"; filename=\"%s\"\r\n", _logFileIndex, fname);
				*pReadLen += res;

				res = snprintf(pbuf += res, len -= res, "%s\r\n", "Content-Type: application/octet-stream");
				*pReadLen += res;

				res = snprintf(pbuf += res, len -= res, "\r\n");
				*pReadLen += res;

				pbuf += res;
				len -= res;
			}
			else {	// Файлы закончились или отсутствуют или не удалось открыть именно этот (_logFileIndex)
				if (_logFileIndex < (int)SdCard::LOG_NUM - 1) {
					_logFileIndex++;
					goto repeat;
				}

				res = snprintf(pbuf, len, "\r\n--%s--\r\n", _boundary);
				*pReadLen += res;

				res = snprintf(pbuf += res, len -= res, "\r\n");
				*pReadLen += res;

				_completed = true;

				goto exit;
			}
		}

		*pReadLen += sdcard.readLogFile(pbuf, len);

		if (*pReadLen == 0) {	// Закончили читать файл
			sdcard.closeLogFile();
			_fopened = false;
			_logFileIndex++;

			goto repeat;
		}
	}

	exit:

	return 0;
}

/*virtual*/ int LogHTTPText::getDataType(char* type, size_t maxTypeLen) {
	snprintf(type, maxTypeLen, "multipart/form-data; boundary=%s", _boundary);

	return HTTP_OK;
}

/*virtual*/ size_t LogHTTPText::getDataLen() { //From Content-Length header, or if the transfer is chunked, next chunk length
	return 0;
}

/*virtual*/ bool LogHTTPText::getIsChunked() {
	return true;
}

//------------------------------------------------------------

syncHTTPText::syncHTTPText() : HTTPText(NULL) {

}

/*virtual*/ int syncHTTPText::read(char* buf, size_t len, size_t* pReadLen) {
	return db.readData(buf, len, pReadLen);
}

/*virtual*/ bool syncHTTPText::getIsChunked() {
	return true;
}

/*virtual*/int syncHTTPText::write(const char* buf, size_t len) {
	wdg.kick(TASK_NET);

	return db.updateBase(buf, len);
}

// ===========================================================


EzeparkServer::EzeparkServer(PinName reset):
			_reset(reset, 0), _firmhttpin(NULL, 0), _httpin(_rxBuf, 10), _needReset(false), _isLink(false), _isLinkWithoutError(false), _isConnected(false), _isBusyLog(false), _isBusyNet(false) {
	_timeurl = "http://service.dev.ezepark.com/api/v2/controller/time";
	_checkurl = "http://service.dev.ezepark.com/api/v2/controller/check";
	_syncurl = "http://service.dev.ezepark.com/api/v2/controller/sync";
	_endurl = "http://service.dev.ezepark.com/api/v2/controller/end-session";
	_firmwareUrl = "http://service.dev.ezepark.com/firmware/";
	_logsUrl = "http://service.dev.ezepark.com/api/v2/controller/upload?id=";
//	fileName = "filename.bin";

	_isCableConnected = false;

	_ip = 0;
	_gateway = 0;
	_mask = 0;
	_dns1 = 0;
	_dns2 = 0;
	memset(_mac,0,6);
	_timeout = 15000;

#if USE_PRINT_DBG
	_http.dumpReqHeader(true);
	_http.dumpResHeader(true);
#else
	_http.dumpReqHeader(false);
	_http.dumpResHeader(false);
#endif
}

bool EzeparkServer::init(uint8_t *mac/* = NULL*/) {
	if (mac != NULL) {
		///\todo Сделать настройку макадреса
	}

	ip_addr_t addr;

	if (_mode == NET_MODE_STATIC) {
		char s_ip[16];
		char s_mask[16];
		char s_gateway[16];

		addr.addr = _ip;
		memcpy(s_ip, inet_ntoa(addr), 16);
		addr.addr = _mask;
		memcpy(s_mask, inet_ntoa(addr), 16);
		addr.addr = _gateway;
		memcpy(s_gateway, inet_ntoa(addr), 16);

		_eth.init(s_ip, s_mask, s_gateway);
	}
	else {
		_eth.init();
	}

	connect();

	if (_mode == NET_MODE_STATIC) {
		core_util_critical_section_enter();
		addr.addr = _dns1 = settings.data.dns1;
		core_util_critical_section_exit();
		dns_setserver(0, &addr);

		core_util_critical_section_enter();
		addr.addr = _dns2 = settings.data.dns2;
		core_util_critical_section_exit();
		dns_setserver(1, &addr);
	}

	return true;
}

bool EzeparkServer::connect(unsigned int timeout_ms /*= 15000*/) {

	core_util_critical_section_enter();
	_isConnected = false;
	core_util_critical_section_exit();

    setLink(false);

	_mutex.lock();

	_eth.connect(10000);

	ip_addr_t val;
	char *ptr;

	ptr = _eth.getIPAddress();
	inet_aton(ptr, &val);
	_ip = val.addr;

	if (_ip == 0) {
		_eth.disconnect();
	}
	else {
		ptr = _eth.getGateway();
		inet_aton(ptr, &val);
		_gateway = val.addr;

		ptr = _eth.getNetworkMask();
		inet_aton(ptr, &val);
		_mask = val.addr;

		_dns1 = dns_getserver(0).addr;
		_dns2 = dns_getserver(1).addr;
	}

	_mutex.unlock();

	core_util_critical_section_enter();
	_isConnected = true;
	core_util_critical_section_exit();

	return true;
}

bool EzeparkServer::isConnected() {
	bool isCon;

	core_util_critical_section_enter();
	isCon = _isConnected;
	core_util_critical_section_exit();

	return isCon;
}

void EzeparkServer::disconnect() {
	 _eth.disconnect();
}

bool EzeparkServer::reset() {
	_mutex.lock();

	_reset = 0;
	ETH_HandleTypeDef ether;
	ether.Instance = ETH;
	HAL_ETH_MspDeInit(&ether);

	core_util_critical_section_enter();
	_mode = (NetMode)settings.data.net_mode;
	_ip = settings.data.ip;
	_gateway = settings.data.gateway;
	_mask = settings.data.mask;
	_dns1 = settings.data.dns1;
	_dns2 = settings.data.dns2;
	memcpy(_mac, settings.data.mac, sizeof(_mac));
	_timeout = settings.data.con_timeout * 1000;
	core_util_critical_section_exit();

	wait_ms(100);
	_reset = 1;
	wait_ms(100);

	bool ret = init();

	if (ret == true) {
		_needReset = false;
	}

	_mutex.unlock();

	return ret;
}

void EzeparkServer::reInit() {
	_mutex.lock();

	_reset = 0;
	wait_ms(200);
	_reset = 1;
	wait_ms(200);

	reInitPhy();

	_mutex.unlock();
}

bool EzeparkServer::isLink() {
	core_util_critical_section_enter();
	bool link = _isLink;
	core_util_critical_section_exit();
	return link;
}

bool EzeparkServer::connectedToServer() {
	core_util_critical_section_enter();
	bool link = _isLinkWithoutError;
	core_util_critical_section_exit();
	return link;
}

void EzeparkServer::SetMode(NetMode mode) {
	if (_mode != mode) {
		_mode = mode;
		core_util_critical_section_enter();
		settings.data.net_mode = (uint8_t)_mode;
		core_util_critical_section_exit();

		settings.save();
		sdcard.printSettingsLog();

		_needReset = true;
	}
}

uint32_t EzeparkServer::getIPu32() {
	ip_addr_t val;
	char *ptr;

	ptr = _eth.getIPAddress();
	inet_aton(ptr, &val);
	_ip = val.addr;
	core_util_critical_section_enter();
	uint32_t ip = _ip;
	core_util_critical_section_exit();
	return ip;
}

char* EzeparkServer::getIPstr() {
	return _eth.getIPAddress();
}

void EzeparkServer::setIP(uint32_t ip) {
	if (_mode == NET_MODE_STATIC && _ip != ip) {
		_ip = ip;
		core_util_critical_section_enter();
		settings.data.ip = _ip;
		core_util_critical_section_exit();

		settings.save();
		sdcard.printSettingsLog();

		_needReset = true;
	}
}

uint32_t EzeparkServer::getGatewayu32() {
	ip_addr_t val;
	char *ptr;

	ptr = _eth.getGateway();
	inet_aton(ptr, &val);
	_gateway = val.addr;

	core_util_critical_section_enter();
	uint32_t gateway = _gateway;
	core_util_critical_section_exit();
	return gateway;
}

char* EzeparkServer::getGatewayStr() {
	return _eth.getGateway();
}

void EzeparkServer::setGateway(uint32_t gateway) {
	if (_mode == NET_MODE_STATIC && _gateway != gateway) {
		_gateway = gateway;
		core_util_critical_section_enter();
		settings.data.gateway = _gateway;
		core_util_critical_section_exit();

		settings.save();
		sdcard.printSettingsLog();

		_needReset = true;
	}
}

uint32_t EzeparkServer::getMasku32() {
	ip_addr_t val;
	char *ptr;

	ptr = _eth.getNetworkMask();
	inet_aton(ptr, &val);
	_mask = val.addr;

	core_util_critical_section_enter();
	uint32_t mask = _mask;
	core_util_critical_section_exit();
	return mask;
}

char* EzeparkServer::getMaskStr() {
	return _eth.getNetworkMask();
}

void EzeparkServer::setMask(uint32_t mask) {
	if (_mode == NET_MODE_STATIC && _mask != mask) {
		_mask = mask;
		core_util_critical_section_enter();
		settings.data.mask = _mask;
		core_util_critical_section_exit();

		settings.save();
		sdcard.printSettingsLog();

		_needReset = true;
	}
}

uint32_t EzeparkServer::getDNS1u32() {
	_dns1 = dns_getserver(0).addr;
	core_util_critical_section_enter();
	uint32_t dns1 = _dns1;
	core_util_critical_section_exit();
	return dns1;
}

char* EzeparkServer::getDNS1Str() {
	ip_addr_t dns1 = dns_getserver(0);
	return inet_ntoa(dns1);
}

void EzeparkServer::setDNS1(uint32_t dns1) {
	if (_mode == NET_MODE_STATIC && _dns1 != dns1) {
		_dns1 = dns1;
		core_util_critical_section_enter();
		settings.data.dns1 = _dns1;
		core_util_critical_section_exit();

		settings.save();
		sdcard.printSettingsLog();

		_needReset = true;
	}
}

uint32_t EzeparkServer::getDNS2u32() {
	_dns2 = dns_getserver(1).addr;
	core_util_critical_section_enter();
	uint32_t dns2 = _dns2;
	core_util_critical_section_exit();
	return dns2;
}

char* EzeparkServer::getDNS2Str() {
	ip_addr_t dns2 = dns_getserver(1);
	return inet_ntoa(dns2);
}

void EzeparkServer::setDNS2(uint32_t dns2) {
	if (_mode == NET_MODE_STATIC && _dns2 != dns2) {
		_dns2 = dns2;
		core_util_critical_section_enter();
		settings.data.dns2 = _dns2;
		core_util_critical_section_exit();

		settings.save();
		sdcard.printSettingsLog();

		_needReset = true;
	}
}

uint8_t* EzeparkServer::getMAC() {
	return (uint8_t*)_eth.getMACAddress();
//	return _mac;
}

void EzeparkServer::setMAC(const uint8_t *mac) {

}

bool EzeparkServer::needReset() {
	_mutex.lock();
	bool need = _needReset;
	_mutex.unlock();
	return need;
}

bool EzeparkServer::isHaveFirmwareUpdate() {
	if ( strcmp(db.getReqFirmwareVer(), db.getCurrFirmwareVer()) == 0 ||
			strcmp(db.getReqFirmwareVer(), "none") == 0 ) {
		return false;
	}
	else {
		return true;
	}
}

/// --- API ---

//char* EzeparkServer::getTime() {
//	HTTPResult httpret = HTTP_NO_RESULT;
//
//	_mutex.lock();
//
//	memset(_rxbuf, '\0', sizeof(_rxbuf));
//
//	if (isCableConnected()) {
//		httpret = _http.get(_timeurl, &_httpin, _timeout);
//
//		locDB.printfLog(LocalDataBase::LOG_SERVER_RSP, "time;%u;%u; ;%s", _http.m_httpResponseCode, (unsigned int)httpret, _rxbuf);
//	}
//
//	if (httpret != HTTP_PARSE && httpret != HTTP_CONN && httpret != HTTP_PRTCL) {
//		PUTS("\n");
//		PUTS(_rxbuf);
//		PUTS("\n");
//		setLink(true);
//	}
//	else {
//		PUTS("\nTIME FAIL\n");
//		setLink(false);
//	}
//
//	char *pret = NULL;
//	if (httpret == HTTP_OK) {
//		pret = _rxbuf;
//		setLinkWithoutError(true);
//	}
//	else {
//		setLinkWithoutError(false);
//	}
//
//	_mutex.unlock();
//
//	return pret;
//}

int EzeparkServer::check() {
	HTTPResult httpret = HTTP_NO_RESULT;

	core_util_critical_section_enter();
	uint32_t deviceid = settings.data.devID;
	core_util_critical_section_exit();

	if (deviceid == 0) {
		return httpret;
	}

	_mutex.lock();

	uint32_t tm = mtime.get();

	char _txbuf[80];

	sprintf(_txbuf,
			"{"
				"\"" J_CODE "\":\"%u\","
				"\"" J_TIME "\":%u,"
				"\"" J_TOKEN "\":\"%u\""
			"}",
			(unsigned int)deviceid, (unsigned int)tm, (unsigned int)token(deviceid, tm));

	HTTPText httpout(_txbuf);

	if (isCableConnected()){
		sdcard.printfLog(SdCard::LOG_COMMON, "Server Check%s", "...");

		httpret = _http.post(_checkurl, httpout, &_httpin, _timeout);

		if (httpret == HTTP_OK) {
			sdcard.printfLog(SdCard::LOG_COMMON, "Server Check%s", "OK");
		}
		else {
			sdcard.printfLog(SdCard::LOG_COMMON, "Server Check%s", "FAIL");
		}

//		locDB.printfLog(LocalDataBase::LOG_SERVER_RSP, "check;%u;%u;%s;%s", _http.m_httpResponseCode, (unsigned int)httpret, _txbuf, bufForBase);
	}

	if (httpret != HTTP_PARSE && httpret != HTTP_CONN && httpret != HTTP_PRTCL) {
		PUTS("\n");
		PUTS(_rxbuf);
		PUTS("\n");
		setLink(true);
	}
	else {
		PUTS("\nCHECK FAIL\n");
		setLink(false);
	}

	int ret;
	if (httpret == HTTP_OK /*&& strcmp(bufForBase, "{}") == 0*/) {
		setLinkWithoutError(true);
		ret = 0;
	}
	else {
		setLinkWithoutError(false);
		ret = -1;
	}

	_mutex.unlock();

	return ret;
}

//char* EzeparkServer::endSession(const char* ptx) {
//	_mutex.lock();
//
//	strcpy(_txbuf, ptx);
//	HTTPText httpout(_txbuf);
//
//	HTTPResult httpret = _http.post(_endurl, httpout, &_httpin, 15000);
//	if (httpret != HTTP_PARSE && httpret != HTTP_CONN && httpret != HTTP_PRTCL) {
//		PUTS("\n");
//		PUTS(_rxbuf);
//		PUTS("\n");
//	    setLink(true);
//	}
//	else {
//		PUTS("\nEND FAIL\n");
//	    setLink(false);
//	}
//
//	char *pret = NULL;
//	if (httpret == HTTP_OK) {
//		setLinkWithoutError(true);
//		pret = _rxbuf;
//	}
//	else {
//		setLinkWithoutError(false);
//	}
//
//	char buf[128];
//	time_t tim = mtime.get();
//	sprintf(buf, "%u;end-session;%u;%u", (unsigned int)tim, _http.m_httpResponseCode, (unsigned int)httpret);
//	locDB.log(LocalDataBase::LOG_SERVER_RSP, buf);
//
//	_mutex.unlock();
//	return pret;
//}

int EzeparkServer::sync(DataBase::SyncMode sync) {
	int ret = 0;
	HTTPResult httpret = HTTP_NO_RESULT;

	_mutex.lock();
	
	syncHTTPText http;
//	syncHTTPText httpin;

	if (isCableConnected()){

		if (sync == DataBase::SyncEvents) {
			sdcard.printfLog(SdCard::LOG_COMMON, "[DB:REQUEST]:%s", "Events");
		}
		else {
			sdcard.printfLog(SdCard::LOG_COMMON, "[DB:REQUEST]:%s", "All");
		}

//		ret = locDB.openDbFile();
//		if (ret != -1) {				// проверка не нужна, т.к. на сервер будет отправляться состояние карты, т.е. синки будут всегда
			httpret = _http.post(_syncurl, http, &http, _timeout);

//			locDB.printfLog(LocalDataBase::LOG_SERVER_RSP, "sync;%u;%u;%s;%s", _http.m_httpResponseCode, (unsigned int)httpret, _txbuf, bufForBase);
//		}
//		else {
//			LOG("Open db file filed");
//		}

		if (httpret == HTTP_OK) {
			sdcard.printfLog(SdCard::LOG_COMMON, "[DB:RESPONSE]:%s", "Ok");
		}
		else {
			sdcard.printfLog(SdCard::LOG_COMMON, "[DB:RESPONSE]:%s", "Fail");
		}
	}

//	if (ret != -1) {
//		ret = locDB.closeDbFile();
//		if (ret == -1) {
//			LOG("Close db file filed");
//		}
//	}


	if (ret == 0 && httpret != HTTP_PARSE && httpret != HTTP_CONN && httpret != HTTP_PRTCL) {
		PUTS("\nDATA BASE RECEIVE SUCCESS!\n");
	    setLink(true);
	}
	else {
		PUTS("\nSYNC FAIL\n");
	    setLink(false);
	}

	if (httpret == HTTP_OK) {
		ret = 0;
		setLinkWithoutError(true);
	}
	else {
		ret = -1;
		setLinkWithoutError(false);
	}

	_mutex.unlock();

	return ret;
}

int EzeparkServer::getFirmware() {
	int ret = -1;
	HTTPResult httpret = HTTP_NO_RESULT;

	_mutex.lock();

	strcpy(_url, _firmwareUrl);
	strcat(_url, fileName);

	_firmhttpin.lengthOfFWfile = 0;
	if (isCableConnected()){
		LOG("Fw download start...");
		httpret = _http.get(_url, &_firmhttpin, _timeout);
		LOG("Fw download finish");

		uint32_t szfile = *(uint32_t*)p_DFU_Data;

		if (httpret == HTTP_OK ) {
			sdcard.printfLog(SdCard::LOG_COMMON, "FW size %f", (float)szfile/1024);

			CRC32 crcObj;
			uint32_t crc = crcObj.get(p_DFU_Data + 4, szfile - 4);
//			memcpy(p_DFU_Data + 4 + szfile, &crc, sizeof(uint32_t));	// Добавляем контрольную сумму
			if (memcmp(&crc, p_DFU_Data + 4 + szfile - 4, 4) == 0) { //+4 - не смотрим на размер, -4 - не смотрим на crc в конце файла
				ret = 0;
				PUTS("\nFIRMWARE RECEIVE SUCCESS!\n");
			}
		}

		if (ret != 0) {
			LOG("Fw update failed");
		}

		sdcard.printfLog(SdCard::LOG_SERVER_RSP, "firmwareUpdate;%u;%u", _http.m_httpResponseCode, (unsigned int)httpret);
	}

	_mutex.unlock();

	return ret;
}

//int EzeparkServer::getBLEfirmware() {
//	int ret = -1;
//	HTTPResult httpret = HTTP_NO_RESULT;
//
//	_mutex.lock();
//
//	strcpy(_url, _firmwareUrl);
//	strcat(_url, "ble");
//	strcat(_url, ".bin");
//
//	_firmhttpin.lengthOfFWfile = 0;
//	if (isCableConnected()){
//		LOG("Fw BLE download start...");
//		httpret = _http.get(_url, &_firmhttpin, _timeout);
//		LOG("Fw BLE download finish");
//
//		uint32_t szfile = *(uint32_t*)p_DFU_Data;
//
//		if (httpret == HTTP_OK ) {
//			sdcard.printfLog(SdCard::LOG_COMMON, "FW BLE size %f", (float)szfile/1024);
//			ret = 0;
//			PUTS("\nFIRMWARE RECEIVE SUCCESS!\n");
//		}
//
//		if (ret != 0) {
//			LOG("Fw BLE update failed");
//		}
//
//		sdcard.printfLog(SdCard::LOG_SERVER_RSP, "BLEfirmwareUpdate;%u;%u", _http.m_httpResponseCode, (unsigned int)httpret);
//	}
//
//	_mutex.unlock();
//
//	return ret;
//}

int EzeparkServer::uploadLogs() {
	_isBusyLog = true;
	int ret = -1;
	HTTPResult httpret = HTTP_NO_RESULT;

//	_mutex.lock();

	LogHTTPText httpout;
	char inbuff[64];
	HTTPText httpin(inbuff, 64);
	char url[80];
	core_util_critical_section_enter();
	unsigned int id = settings.data.devID;
	core_util_critical_section_exit();

	if(id == 0) {
		return -1;
	}

	sprintf(url, "%s%u", _logsUrl, id);

//	const char *ptx =	"------WebKitFormBoundary7MA4YWxkTrZu0gW\n"
//						"Content-Disposition: form-data; name=\"file[0]\"; filename=\"log.txt\"\n"
//						"Content-Type: application/octet-stream\n"
//						"\r\n"
//						"One!!!\n"
//						"------WebKitFormBoundary7MA4YWxkTrZu0gW\n"
//						"Content-Disposition: form-data; name=\"file[1]\"; filename=\"log1.txt\"\n"
//						"Content-Type: application/octet-stream\n"
//						"\r\n"
//						"Two!!!\n"
//						"------WebKitFormBoundary7MA4YWxkTrZu0gW\n"
//						"Content-Disposition: form-data; name=\"file[2]\"; filename=\"log2.txt\"\n"
//						"Content-Type: application/octet-stream\n"
//						"\r\n"
//						"Three!!!\n"
//						"------WebKitFormBoundary7MA4YWxkTrZu0gW--\n"
//						"\r\n";
//	strcpy(_txbuf, ptx);
//	HTTPText httpout(_txbuf);

	if (isCableConnected()){
		LOG("Log upload start...");
		httpret = _httplog.post(url, httpout, &httpin, _timeout);

		// Надо ОБЯЗАТЕЛЬНО вызывать после запроса
		httpout.resetData();

		if (httpret == HTTP_OK) {
			LOG("Log upload success");
			ret = 0;
		}
		else {
			LOG("Log upload failed");
		}

		sdcard.printfLog(SdCard::LOG_SERVER_RSP, "logUpload;%u;%u", _http.m_httpResponseCode, (unsigned int)httpret);
	}

//	_mutex.unlock();
	_isBusyLog = false;

	return ret;
}

/// --- END API ---

uint32_t EzeparkServer::token(uint32_t deviceid, uint32_t time) {
	return deviceid % 1000 * 769 + time % 1000 * 456417;
}

bool EzeparkServer::checkPHY(){
	bool ret = false;
	uint8_t tmtPHYstatus = PHYstatus;

	ret = tmtPHYstatus & PHY_CHECK ? true : false;

	return ret;
}

bool EzeparkServer::isCableConnected(){
	static bool prestate = false;

	core_util_critical_section_enter();
	bool ret = PHYstatus & CABLE_CHECK ? true : false;
	core_util_critical_section_exit();

	if (prestate != ret) {
		if (ret == true) {
			sdcard.printfLog(SdCard::LOG_COMMON, "[PHY]:Cable %s", "connected");
		}
		else {
			sdcard.printfLog(SdCard::LOG_COMMON, "[PHY]:Cable %s", "disconnected");
			setLinkWithoutError(false);
		}
		prestate = ret;
	}

	return ret;
}

void EzeparkServer::disablePHY() {
	core_util_critical_section_enter();
	PHYstatus |= PHY_DISABLE;
	core_util_critical_section_exit();
}

void EzeparkServer::setTimeout(uint32_t val) {
	uint32_t expected = _timeout;
	while (core_util_atomic_cas_u32(&_timeout, &expected, val) == false);
}

uint32_t EzeparkServer::getTimeout() {
	uint32_t expected = 0;
	core_util_atomic_cas_u32(&_timeout, &expected, 0);
	return expected;
}

void EzeparkServer::isBusyNet(bool state) {
	_isBusyNet = state;
}

bool EzeparkServer::isBusy() {
	return (_isBusyLog || _isBusyNet);
}
//======================================================================

void EzeparkServer::setLink(bool stat) {
	core_util_critical_section_enter();
	_isLink = stat;
	core_util_critical_section_exit();
}

void EzeparkServer::setLinkWithoutError(bool stat) {
	core_util_critical_section_enter();
	_isLinkWithoutError = stat;
	core_util_critical_section_exit();
}


EzeparkServer EzServer(PF_11);

