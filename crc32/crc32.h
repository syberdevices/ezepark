/******************************************************************************
	���� ���������� ��������� CRC32 �� ����������� ��������� 0xEDB88320
	�������� �����: ������������ ����� �������������
	E-mail: paveldvlip@mail.ru
	2005 ���
******************************************************************************/

#ifndef CRC32_H_
#define CRC32_H_

#include "mbed.h"

class CRC32 {
public:
	CRC32();
	uint32_t process(const void* pData, uint32_t nLen);
	void set_current_process_crc(uint32_t curr_crc);	// На случай, если crc надо посчитать не сначала
	uint32_t complete();
	uint32_t get(const void* pData, uint32_t nLen);

	static bool inited;

private:
//	uint32_t table[256];
	uint32_t _crc;
};

#endif // CRC32_H_
