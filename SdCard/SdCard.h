/*
 * LocalDataBase.h
 *
 *  Created on: 25 авг. 2016 г.
 *      Author: Denis
 */

#ifndef LOCALDATABASE_H_
#define LOCALDATABASE_H_

#include <DataBase/DataBase.h>
#include <common.h>

#include <mbed.h>
#include <rtos.h>

class SdCard {
public:

	SdCard();

	typedef enum LogType {
		LOG_COMMON,
		LOG_SERVER_RSP,
		LOG_BLE_DISCOVER,
		LOG_STACK,

		LOG_NUM,
	}LogType_t;

	int init();

	bool checkSDcard();
	
	void SdPowerOn();

	void SdPowerOff();

	int crcFWfile();

	int openFWfile();

	int closeFWfile();

	int writeFWfile(const char* buf, size_t len);

	int deleteFWFile();

	bool isBusy();

	int formatDisk();

	uint32_t sizeOfFWfile();

	void safelyRemove();

	time_t lastTime();
	
	void timeUpdated();

	uint32_t cardError();

	int loadConfig(Config_t *pconfig);

// ---------------------------------

	int printfLog(LogType type, const char* format, ...);

	const char* openNewestLogFile(LogType_t type);

	int closeLogFile();

	int readLogFile(char* buf, size_t maxlen);

	bool isNeedLogTransmit();

	void resetLogTransmitFlag();

	int printSettingsLog();

	int printConfigLog(Config_t *config);

// ---------------------------------

private:
	typedef struct {
		const char *name;
		const char *head;
	}Log_t;

	Log_t logdat[LOG_NUM];

	Mutex _mutex;

	char _path[256];

	FILE *_pFullDb;
	FILE *_pFW;
	FILE *_pLog;
	bool _isStreamErr;

	uint32_t _cardError;

	bool _isBusy;

	uint32_t _timeUpdated;

	char _logpath[128];

	char *_plogpath;		/// Указатель на изменяемую часть пути лога

	DigitalOut _sdOn;

	uint32_t _beacons;

// ---------------------------------

	int removePath(char *path);

	int privatePrintfLog(LogType type, const char* format, ...);

	int vprintfLog(LogType type, struct tm *ts, const char* format, std::va_list arg);

	int createLogPath(LogType type, struct tm* tims);

	int getCurrentLocalTime(struct tm* ts);

// ---------------------------------

};

extern SdCard sdcard;


#define LOG(data)		sdcard.printfLog(SdCard::LOG_COMMON, (data))

#endif /* LOCALDATABASE_H_ */
