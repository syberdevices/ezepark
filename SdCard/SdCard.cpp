/*
 * LocalDataBase.cpp
 *
 *  Created on: 25 авг. 2016 г.
 *      Author: Denis
 */

#include <rt_TypeDef.h>
#undef NULL

#include <SdCard/SdCard.h>
#include <SDFileSystem/SDFileSystem.h>
#include <MyTime/MyTime.h>
#include <Settings/Settings.h>
#include <common.h>
#include <Watchdog/Watchdog.h>

#include <cfloat>
#include <ctype.h>

extern Settings<Sett_t> settings;
extern Watchdog wdg;

extern SDFileSystem *p_sd;

#define SD_DIR						"/sd"
#define BIN_DIR						"/sd/bin"
#define DATABASE_DIR				"/sd/database"
#define BEACONS_DIR					"/sd/database/beacons"
#define UUIDS_DIR					"/sd/database/uuids"
#define SESSIONS_DIR				"/sd/sessions"
#define EVENTS_DIR					"/sd/events"
#define TMP_DIR						"/sd/tmp"
#define TMP_DATABASE_DIR			"/sd/tmp/database"
#define TMP_BEACONS_DIR				"/sd/tmp/database/beacons"
#define TMP_UUIDS_DIR				"/sd/tmp/database/uuids"
#define TMP_SESSIONS_DIR			"/sd/tmp/sessions"
#define FULL_DB_FILE				"/sd/db.json"
#define FILE_TYPE					".json"
#define	FIRMWARE					"/sd/bin/fw.bin"

#define LOG_DIR						"/sd/log"
#define LOG_COMMON_FILE				"common.log"
#define LOG_STACK_FILE				"stack.log"
#define LOG_SERVER_RSP_FILE			"server.csv"
#define LOG_BLE_DISCOVER_FILE		"blediscover.csv"

#define LOG_SERVER_RSP_HEAD			"TIME;REQUEST;RESPONSE CODE;HTTP ERROR;REQUEST DATA;RESPONSE DATA\r\n"
#define LOG_BLE_DISCOVER_HEAD		"TIME;UUID;RSSI\r\n"

#define LOGGING_DAYS_NUM			10		//!< Количество логируемых дней

#define TEST_FILE               	"/sd/TEST_FILE"

extern bool locdbInited;

SdCard::SdCard() :
		_pFullDb(NULL), _pFW(NULL), _pLog(NULL), _isStreamErr(false), _cardError(0), _isBusy(false),
		_timeUpdated(0), _sdOn(PD_3, 1) {
	logdat[LOG_COMMON].name = LOG_COMMON_FILE;
	logdat[LOG_COMMON].head = NULL;
	logdat[LOG_STACK].name = LOG_STACK_FILE;
	logdat[LOG_STACK].head = NULL;
	logdat[LOG_SERVER_RSP].name = LOG_SERVER_RSP_FILE;
	logdat[LOG_SERVER_RSP].head = LOG_SERVER_RSP_HEAD;
	logdat[LOG_BLE_DISCOVER].name = LOG_BLE_DISCOVER_FILE;
	logdat[LOG_BLE_DISCOVER].head = LOG_BLE_DISCOVER_HEAD;

	strcpy(_logpath, LOG_DIR);
	_plogpath = _logpath + strlen(_logpath) + 1;
}

int SdCard::init() {
	int ret = 0;
	locdbInited = true;

	_mutex.lock();
	_isBusy = true;

	DIR* fp = opendir(BIN_DIR);
	if (fp == NULL) {
		if (mkdir(BIN_DIR, 0) != 0) {
			ret = -1;
			goto exit;
		}
	}
	else {
		closedir(fp);
	}

	// ------- LOG -----------

	fp = opendir(LOG_DIR);
	if (fp == NULL) {
		if (mkdir(LOG_DIR, 0) != 0) {
			ret = -1;
			goto exit;
		}
	}
	else {
		closedir(fp);
	}

	exit:

	if (ret != 0) {
		if (_cardError == 0) {
			core_util_atomic_incr_u32(&_cardError, 1);
		}
	}
	else if (_cardError != 0){
		core_util_atomic_decr_u32(&_cardError, 1);
	}

	_isBusy = false;
	_mutex.unlock();

	if(ret != 0) {
		locdbInited = false;
	}

	return ret;
}

int SdCard::formatDisk(){
	_mutex.lock();
	_isBusy = true;

	int ret = p_sd->format();

	if (ret != 0) {
		if (_cardError == 0) {
			core_util_atomic_incr_u32(&_cardError, 1);
		}
	}
	else if (_cardError != 0){
		core_util_atomic_decr_u32(&_cardError, 1);
	}

	_isBusy = false;
    _mutex.unlock();

    return ret;
}

int SdCard::openFWfile() {
	int ret = 0;

	_mutex.lock();
	_isBusy = true;

	_pFW = fopen(FIRMWARE, "w");
	if (_pFW == NULL) {
		ret = -1;
	}

	if (ret != 0) {
		if (_cardError == 0) {
			core_util_atomic_incr_u32(&_cardError, 1);
		}
	}
	else if (_cardError != 0){
		core_util_atomic_decr_u32(&_cardError, 1);
	}

	_isBusy = false;
	_mutex.unlock();

	return ret;
}

int SdCard::writeFWfile(const char* buf, size_t len) {
	size_t written = 0;

	_mutex.lock();
	_isBusy = true;

	if (_pFW) {
		written = (int)fwrite(buf, 1, len, _pFW);
	}

	int ret;
	if (written == len) {
		ret = 0;
	}
	else {
		ret = -1;
		_pFW = NULL;	// Если ошибка, то следующей записи уже не будет
	}

	if (ret != 0) {
		if (_cardError == 0) {
			core_util_atomic_incr_u32(&_cardError, 1);
		}
	}
	else if (_cardError != 0){
		core_util_atomic_decr_u32(&_cardError, 1);
	}

	_isBusy = false;
	_mutex.unlock();

	return ret;
}

int SdCard::closeFWfile() {
	int ret = -1;

	_mutex.lock();
	_isBusy = true;
	
	if (_pFW) {
		ret = fclose(_pFW);
		_pFW = NULL;
	}
	
	if (ret != 0) {
		if (_cardError == 0) {
			core_util_atomic_incr_u32(&_cardError, 1);
		}
	}
	else if (_cardError != 0){
		core_util_atomic_decr_u32(&_cardError, 1);
	}

	_isBusy = false;
	_mutex.unlock();

	return ret;
}

int SdCard::deleteFWFile() {
	_mutex.lock();
	_isBusy = true;

	int ret = remove(FIRMWARE);

	if (ret != 0) {
		if (_cardError == 0) {
			core_util_atomic_incr_u32(&_cardError, 1);
		}
	}
	else if (_cardError != 0){
		core_util_atomic_decr_u32(&_cardError, 1);
	}

	_isBusy = false;
	_mutex.unlock();

	return ret;
}

bool SdCard::isBusy() {
	return _isBusy;
}

bool SdCard::checkSDcard() {
	_mutex.lock();
	_isBusy = true;

	char buf[] = "some string";
	int len = sizeof(buf);
	char buf_read[len] = {0};
	int ret = -1;

	FILE *fp = fopen(TEST_FILE, "w");
	if (fp != NULL) {
		int cnt = fwrite(buf, len, 1, fp);
		if (cnt == 1) {
			ret = 0;
		}
		else {
			ret = -1;
			goto exit;
		}

		ret = fclose(fp);
		fp = NULL;
		if (ret == -1) {
			goto exit;
		}
	}
	else {
		ret = -1;
		goto exit;
	}

	fp = fopen(TEST_FILE, "r");
	if (fp != NULL) {
		fread(buf_read, len, 1, fp);
		if (strcmp(buf_read, buf) == 0) {
			ret = 0;
		}
		else {
			ret = -1;
			goto exit;
		}

		ret = fclose(fp);
		fp = NULL;
		if (ret == -1) {
			goto exit;
		}
	}
	else {
		ret = -1;
	}

	exit:

	if (fp) {	// Если указатель на файл есть, то точно была ошибка. Просто пытаемся закрыть и удалить артефакты
		fclose(fp);
		fp = NULL;
		remove(TEST_FILE);
	}
	else {
		int res = remove(TEST_FILE);
		if (ret == 0) {
			ret = res;
		}
	}

	if (ret != 0) {
		if (_cardError == 0) {
			core_util_atomic_incr_u32(&_cardError, 1);
		}
	}
	else if (_cardError != 0){
		core_util_atomic_decr_u32(&_cardError, 1);
	}

	_isBusy = false;
	_mutex.unlock();

	return ret == 0 ? true : false;
}

void SdCard::SdPowerOn() {
	_sdOn = 0;
}

void SdCard::SdPowerOff() {
	_sdOn = 1;
}

int SdCard::crcFWfile() {
	int ret = 0;
	uint8_t data[20];
	int len = 20;
	CRC32 crc;
	_mutex.lock();

	FILE *fp = fopen(FIRMWARE, "r");
	if (fp != NULL) {
		while(1){
			int count = fread(data, 1, len, fp);
			crc.process(data, count);
			if (count < len) {
				break;
			}
		}
		ret = fclose(fp);
	}
	else {
		ret = -1;
	}

	uint32_t _crc = crc.complete();
	fp = fopen(FIRMWARE, "a");
	if (fp != NULL) {
		fwrite((char *)&_crc, 1, sizeof(_crc), fp);
		ret = fclose(fp);
	}
	else {
		ret = -1;
	}

	if (ret != 0) {
		if (_cardError == 0) {
			core_util_atomic_incr_u32(&_cardError, 1);
		}
	}
	else if (_cardError != 0){
		core_util_atomic_decr_u32(&_cardError, 1);
	}

	_mutex.unlock();
	return ret;
}

uint32_t SdCard::sizeOfFWfile() {
	int size = 0;
	_mutex.lock();

	FILE *fp = fopen(FIRMWARE, "r");
	if (fp != NULL) {
		fseek(fp, 0, SEEK_END);
		size = ftell(fp);
		fseek(fp, 0, SEEK_SET);
		fclose(fp);
	}

	_mutex.unlock();
	return size;
}

void SdCard::safelyRemove() {
	_mutex.lock();
}

uint32_t SdCard::cardError() {
	return _cardError;
}

int SdCard::loadConfig(Config_t *pconfig) {
	int ret = -1;

	_mutex.lock();
	_isBusy = true;

	FILE *fp = fopen("/sd/config", "r");
	if (fp) {
		fseek(fp, 0, SEEK_END);
		int size = ftell(fp);
		fseek(fp, 0, SEEK_SET);

		char *pbuf = new char[size + 1];
		if (pbuf) {
			if (fread(pbuf, size, 1, fp) == 1) {
				pbuf[size] = '\0';
				Config_t config;
				char *ptr = pbuf;
				int tmp;

				ptr = strstr(ptr, "beeper");
				if (ptr) {
					ptr += strlen("beeper") + 2;
					if (memcmp(ptr, "true", strlen("true")) == 0) {
						config.beeper = true;
					}
					else if (memcmp(ptr, "false", strlen("false")) == 0) {
						config.beeper = false;
					}
					else { goto exit; }
				}
				else { goto exit; }

				ptr = strstr(ptr, "advertise");
				if (ptr) {
					ptr += strlen("advertise") + 2;
					if (memcmp(ptr, "true", strlen("true")) == 0) {
						config.advertise = true;
					}
					else if (memcmp(ptr, "false", strlen("false")) == 0) {
						config.advertise = false;
					}
					else { goto exit; }
				}
				else { goto exit; }

				ptr = strstr(ptr, "scan");
				if (ptr) {
					ptr += strlen("scan") + 2;
					if (memcmp(ptr, "true", strlen("true")) == 0) {
						config.scan = true;
					}
					else if (memcmp(ptr, "false", strlen("false")) == 0) {
						config.scan = false;
					}
					else { goto exit; }
				}
				else { goto exit; }

				ptr = strstr(ptr, "adv_interval");
				if (ptr) {
					ptr += strlen("adv_interval") + 2;
					if (EOF == sscanf(ptr, "%d", &tmp)) { goto exit; }
					config.adv_interval = tmp;
				}
				else { goto exit; }

				ptr = strstr(ptr, "auth_timeout");
				if (ptr) {
					ptr += strlen("auth_timeout") + 2;
					if (EOF == sscanf(ptr, "%d", &tmp)) { goto exit; }
					config.auth_timeout = tmp;
				}
				else { goto exit; }

				ptr = strstr(ptr, "att_mtu");
				if (ptr) {
					ptr += strlen("att_mtu") + 2;
					if (EOF == sscanf(ptr, "%d", &tmp)) { goto exit; }
					config.att_mtu = tmp;
				}
				else { goto exit; }

				ptr = strstr(ptr, "min_conn_interval");
				if (ptr) {
					ptr += strlen("min_conn_interval") + 2;
					if (EOF == sscanf(ptr, "%f", &config.min_conn_interval)) { goto exit; }
				}
				else { goto exit; }

				ptr = strstr(ptr, "max_conn_interval");
				if (ptr) {
					ptr += strlen("max_conn_interval") + 2;
					if (EOF == sscanf(ptr, "%f", &config.max_conn_interval)) { goto exit; }
				}
				else { goto exit; }

				ptr = strstr(ptr, "slave_latency");
				if (ptr) {
					ptr += strlen("slave_latency") + 2;
					if (EOF == sscanf(ptr, "%d", &tmp)) { goto exit; }
					config.slave_latency = tmp;
				}
				else { goto exit; }

				ptr = strstr(ptr, "supervision_timeout");
				if (ptr) {
					ptr += strlen("supervision_timeout") + 2;
					if (EOF == sscanf(ptr, "%f", &config.supervision_timeout)) { goto exit; }
				}
				else { goto exit; }

				if (config.att_mtu >= 23 && config.att_mtu <= 247 &&
						config.min_conn_interval >= 7.5f && config.min_conn_interval <= 4000.0f &&
						config.max_conn_interval >= 7.5f && config.max_conn_interval <= 4000.0f &&
						config.slave_latency >= 0 && config.slave_latency <= 499 &&
						config.supervision_timeout >= 100.0f && config.supervision_timeout <= 32000.0f &&
						config.auth_timeout >= 0 && config.auth_timeout <= 255 &&
						config.adv_interval >= 20 && config.adv_interval <= 10240)
				{
					memcpy(pconfig, &config, sizeof(config));
					ret = 0;
				}
			}

			exit:
			delete pbuf;
		}

		fclose(fp);
	}

	_isBusy = false;
	_mutex.unlock();

	return ret;
}

// ------------------------------------------------config------------------------------

int SdCard::printfLog(LogType type, const char* format, ...) {
	int ret = 0;

	core_util_critical_section_enter();
	uint8_t log_mode = settings.data.logs_mode;
	core_util_critical_section_exit();

	if (log_mode == LOGS_MODE_DISABLED) {
		goto exit;
	}

	struct tm ts;
	getCurrentLocalTime(&ts);

	_mutex.lock();
	_isBusy = true;

    std::va_list arg;
    va_start(arg, format);
	ret = vprintfLog(type, &ts, format, arg);
    va_end(arg);

	_isBusy = false;
	_mutex.unlock();

	exit:

    return ret;
}

const char* SdCard::openNewestLogFile(LogType_t type) {
	const char *retname = NULL;
	char buf[] = "2016-01-01";
	int res;

	_mutex.lock();
	_isBusy = true;

	strcpy(_path, LOG_DIR);

	DIR *dp = opendir(_path);
	if (dp) {
		struct dirent *entry;

		while ((entry = readdir(dp)) != NULL) {
			// Проверяем формат имени объектов
			int y, m, d;
			res = sscanf(entry->d_name, "%d-%d-%d", &y, &m, &d);
			if (res == 3) {
				// Это тапка с логами
				// Сравниваем имена папок. Т.к. имена - даты, то так канает
				if (strcmp(entry->d_name, buf) > 0) {	// Нашли более новое имя
					strcpy(buf, entry->d_name);
				}
			}
		}

		closedir(dp);
		dp = NULL;

		strcat(_path, "/");
		strcat(_path, buf);
		strcat(_path, "/");

		switch(type) {
		case LOG_COMMON: {
			strcat(_path, LOG_COMMON_FILE);
			retname = LOG_COMMON_FILE;
			break;
		}
		case LOG_SERVER_RSP: {
			strcat(_path, LOG_SERVER_RSP_FILE);
			retname = LOG_SERVER_RSP_FILE;
			break;
		}
		case LOG_BLE_DISCOVER: {
			strcat(_path, LOG_BLE_DISCOVER_FILE);
			retname = LOG_BLE_DISCOVER_FILE;
			break;
		}
		case LOG_STACK: {
			strcat(_path, LOG_STACK_FILE);
			retname = LOG_STACK_FILE;
			break;
		}
		default: {

		}
		}

		_pLog = fopen(_path, "r");
		if (_pLog == NULL) {
			retname = NULL;
		}
	}

	_isBusy = false;
	_mutex.unlock();

	return retname;
}

int SdCard::closeLogFile() {
	int ret = -1;

	_mutex.lock();
	_isBusy = true;

	if (_pLog) {
		ret = fclose(_pLog);
		_pLog = NULL;
	}

	_isBusy = false;
	_mutex.unlock();

	return ret;
}

int SdCard::readLogFile(char* buf, size_t maxlen) {
	int ret = 0;

	_mutex.lock();
	_isBusy = true;

	if (_pLog) {
		ret = fread(buf, 1, maxlen, _pLog);
	}

	_isBusy = false;
	_mutex.unlock();

	return ret;
}

int SdCard::printSettingsLog() {
	int ret = 0;

	core_util_critical_section_enter();
	uint8_t log_mode = settings.data.logs_mode;
	core_util_critical_section_exit();

	if (log_mode == LOGS_MODE_DISABLED) {
		goto exit;
	}

	Sett_t tmp;
	char buf[512];

	core_util_critical_section_enter();
	memcpy(&tmp, &settings.data, sizeof(tmp));
	core_util_critical_section_exit();

	sprintf(buf, "\t%s: %u\n", "devID", (unsigned int)tmp.devID);
	sprintf(buf + strlen(buf), "\t%s: %s\n", "dev_type", tmp.dev_type == DEVICE_TYPE_OUT ? "EXIT" : tmp.dev_type == DEVICE_TYPE_IN ? "ENTER" : "OFF");
	sprintf(buf + strlen(buf), "\t%s: %u\n", "dev_number", tmp.dev_number);
	sprintf(buf + strlen(buf), "\t%s: %s\n", "net_mode", tmp.net_mode == NET_MODE_STATIC ? "Static" : "DHCP");
	sprintf(buf + strlen(buf), "\t%s: %u.%u.%u.%u\n", "ip", ((uint8_t*)&tmp.ip)[0], ((uint8_t*)&tmp.ip)[1], ((uint8_t*)&tmp.ip)[2], ((uint8_t*)&tmp.ip)[3]);
	sprintf(buf + strlen(buf), "\t%s: %u.%u.%u.%u\n", "gateway", ((uint8_t*)&tmp.gateway)[0], ((uint8_t*)&tmp.gateway)[1], ((uint8_t*)&tmp.gateway)[2], ((uint8_t*)&tmp.gateway)[3]);
	sprintf(buf + strlen(buf), "\t%s: %u.%u.%u.%u\n", "mask", ((uint8_t*)&tmp.mask)[0], ((uint8_t*)&tmp.mask)[1], ((uint8_t*)&tmp.mask)[2], ((uint8_t*)&tmp.mask)[3]);
	sprintf(buf + strlen(buf), "\t%s: %02x:%02x:%02x:%02x:%02x:%02x\n", "mac", tmp.mac[0], tmp.mac[1], tmp.mac[2], tmp.mac[3], tmp.mac[4], tmp.mac[5]);
	sprintf(buf + strlen(buf), "\t%s: %u.%u.%u.%u\n", "dns1", ((uint8_t*)&tmp.dns1)[0], ((uint8_t*)&tmp.dns1)[1], ((uint8_t*)&tmp.dns1)[2], ((uint8_t*)&tmp.dns1)[3]);
	sprintf(buf + strlen(buf), "\t%s: %u.%u.%u.%u\n", "dns2", ((uint8_t*)&tmp.dns2)[0], ((uint8_t*)&tmp.dns2)[1], ((uint8_t*)&tmp.dns2)[2], ((uint8_t*)&tmp.dns2)[3]);
	sprintf(buf + strlen(buf), "\t%s: %u\n", "con_timeout", tmp.con_timeout);
	sprintf(buf + strlen(buf), "\t%s: %u\n", "tx_pwr", tmp.tx_pwr);
	sprintf(buf + strlen(buf), "\t%s: %d\n", "beac_trh", tmp.beac_trh);
	sprintf(buf + strlen(buf), "\t%s %s\n", "loging is", tmp.logs_mode == LOGS_MODE_ENABLED ? "enabled" : "disabled");

	_mutex.lock();
	_isBusy = true;

	ret = privatePrintfLog(SdCard::LOG_COMMON, "Settings:\n%s", buf);

	_isBusy = false;
	_mutex.unlock();

	exit:
	return ret;
}

int SdCard::printConfigLog(Config_t *config) {
	int ret = 0;
	char buf[512];

	sprintf(buf, "\t%s: %s\n", "beeper", config->beeper == true ? "true" : "false");
	sprintf(buf + strlen(buf), "\t%s: %s\n", "advertise", config->advertise == true ? "true" : "false");
	sprintf(buf + strlen(buf), "\t%s: %s\n", "scan", config->scan == true ? "true" : "false");
	sprintf(buf + strlen(buf), "\t%s: %u\n", "adv_interval", (unsigned int)config->adv_interval);
	sprintf(buf + strlen(buf), "\t%s: %u\n", "att_mtu", (unsigned int)config->att_mtu);
	sprintf(buf + strlen(buf), "\t%s: %0.2f\n", "min_conn_interval", config->min_conn_interval);
	sprintf(buf + strlen(buf), "\t%s: %0.2f\n", "max_conn_interval", config->max_conn_interval);
	sprintf(buf + strlen(buf), "\t%s: %u\n", "slave_latency", (unsigned int)config->slave_latency);
	sprintf(buf + strlen(buf), "\t%s: %0.2f\n", "supervision_timeout", config->supervision_timeout);

	_mutex.lock();
	_isBusy = true;

	ret = privatePrintfLog(SdCard::LOG_COMMON, "Config:\n%s", buf);

	_isBusy = false;
	_mutex.unlock();

	return ret;
}

// ------------------------------------------------------------------------------

time_t SdCard::lastTime() {
	time_t timeForRTC = 1451606400;	// 01 Jan 2016 00:00:00 GMT
	char buf[5];
	struct tm ts;
	uint32_t tim = 0;
	uint32_t size = 0;
	
	if (openNewestLogFile(LOG_COMMON) != NULL) {
		fseek(_pLog, 0, SEEK_END);
		size = ftell(_pLog);
		if (size <= 1) {
			closeLogFile();
			goto exit;
		}
		char symbol;
		do {
			fseek(_pLog, -2, SEEK_CUR);
			symbol = fgetc(_pLog);
			if (symbol == -1) {
				closeLogFile();
				goto exit;
			}
			size = ftell(_pLog);
			if (size <= 1) {
				fseek(_pLog, 0, SEEK_SET);
				break;
			}
		} while (symbol != '\n');

		//дни
		fseek(_pLog, 1, SEEK_CUR);
		fread(buf, 1, 2, _pLog);
		tim = atoi(buf);
		ts.tm_mday = tim;
		buf[2] = '\0';

		//месяцы
		fseek(_pLog, 1, SEEK_CUR);
		fread(buf, 1, 2, _pLog);
		tim = atoi(buf);
		ts.tm_mon = tim;
		buf[2] = '\0';

		//года
		fseek(_pLog, 1, SEEK_CUR);
		fread(buf, 1, 4, _pLog);
		tim = atoi(buf);
		ts.tm_year = tim;
		buf[2] = '\0';

		//часы
		fseek(_pLog, 1, SEEK_CUR);
		fread(buf, 1, 2, _pLog);
		tim = atoi(buf);
		ts.tm_hour = tim;
		buf[2] = '\0';

		//минуты
		fseek(_pLog, 1, SEEK_CUR);
		fread(buf, 1, 2, _pLog);
		tim = atoi(buf);
		ts.tm_min = tim;
		buf[2] = '\0';

		//секунды
		fseek(_pLog, 1, SEEK_CUR);
		fread(buf, 1, 2, _pLog);
		tim = atoi(buf);
		ts.tm_sec = tim;
		buf[2] = '\0';

		timeForRTC = mtime.convert2UnixTime(ts);

		closeLogFile();
	}

	exit:
	return timeForRTC;
}

void SdCard::timeUpdated() {
	if (!_timeUpdated) {
		core_util_atomic_incr_u32(&_timeUpdated, 1);
	}
}

// ==============================================================================

int SdCard::removePath(char *path) {
	int ret = 0;
	char *tmppath = (char*)malloc(128);

	if (tmppath) {
		strcpy(tmppath, path);

		DIR* fp = opendir(tmppath);
		if (fp != NULL) {
			strcat(tmppath, "/");
			char *ptr = tmppath + strlen(tmppath);
			struct dirent *entry;

			while ((entry = readdir(fp)) != NULL) {
				strcpy(ptr, entry->d_name);

				DIR* dir = opendir(tmppath);
				if (dir == NULL) {	// Если не удалось открыть, то это файл. Удаляем
					ret = remove(tmppath);
					if (ret == -1) {
						break;
					}
				}
				else {
					ret = closedir(dir);
					if (ret == 0) {
						dir = NULL;
						ret = removePath(tmppath);
					}
					if (ret == -1) {
						break;
					}
				}
			}

			int res = closedir(fp);
			fp = NULL;

			if (ret == 0 && res == 0) {
				*(ptr - 1) = '\0';
				if (strcmp(tmppath, SD_DIR) != 0) {
					ret = remove(tmppath);
				}
			}
			else {
				ret = -1;
			}
		}
		else {	// Если не удалось открыть, то это файл. Удаляем
			ret = remove(tmppath);
		}

		free(tmppath);
	}
	else {
		ret = -1;
	}

	return ret;
}

int SdCard::privatePrintfLog(LogType type, const char* format, ...) {
	struct tm ts;
	getCurrentLocalTime(&ts);

    std::va_list arg;
    va_start(arg, format);
	int ret = vprintfLog(type, &ts, format, arg);
    va_end(arg);

    return ret;
}

int SdCard::vprintfLog(LogType type, struct tm *ts, const char* format, std::va_list arg) {
	int ret = createLogPath(type, ts);

	if (ret == 0) {
		FILE *fp = fopen(_logpath, "a+");
		if (fp != NULL) {
			fprintf(fp, "[%02d-%02d-%04d %02d:%02d:%02d]", ts->tm_mday, ts->tm_mon, ts->tm_year, ts->tm_hour, ts->tm_min, ts->tm_sec);

			if (type == LOG_SERVER_RSP || type == LOG_BLE_DISCOVER) {
				fputc(';', fp);
			}
			else {
				fputc(' ', fp);
			}

			if (type != LOG_STACK) {
				if (!_timeUpdated) fprintf(fp, "[!WRONG TIME!] ");

				vfprintf(fp, format, arg);
				fputc('\n', fp);
			}
			else {
				static const char *tasks[] = {"kernel", "main", "ble", "net", "tcpip", "recv", "phy", "log"};
				extern void *os_active_TCB[];

				fprintf(fp,  " STACK...\n");

				for (int q = 0; q < 8; q++) {
					P_TCB tcb = (P_TCB)os_active_TCB[q];

					unsigned int stack_size = tcb->priv_stack;
					unsigned int free_stack = tcb->tsk_stack - (uint32_t)tcb->stack;
					unsigned int used_stack = (uint32_t)tcb->stack + tcb->priv_stack - tcb->tsk_stack;
					unsigned int high_mark = 0;
					while (tcb->stack[high_mark] == 0xE25A2EA5)
						high_mark++;
					unsigned int max_stack = tcb->priv_stack - (high_mark * 4);

					fprintf(fp, "\t\t[%s]:\tsize %u,\tfree %u,\tused %u,\tmax %u\n", tasks[q], stack_size, free_stack, used_stack, max_stack);
				}
			}

			fclose(fp);
		}
		else {
			ret = -1;
		}
	}

	if (ret == 0) {
		if (_cardError != 0){
			core_util_atomic_decr_u32(&_cardError, 1);
		}
	}
	else {
		if (_cardError == 0) {
			core_util_atomic_incr_u32(&_cardError, 1);
		}
	}

	return ret;
}

int SdCard::createLogPath(LogType type, struct tm* ts) {
	int ret = -1;
	int res;
	DIR *dp = NULL;
	int cnt = 0;
	char buf[11];
	buf[0] = 0xFF;	// Самое большое значение, чтобы первый раз правильно отработал strcpy()

	if (type != LOG_COMMON) goto exit;

	*(_plogpath - 1) = '\0';

	dp = opendir(_logpath);
	if (dp != NULL) {
		struct dirent *entry;

		*(_plogpath - 1) = '/';

		while (1) {
			cnt = 0;

			while ((entry = readdir(dp)) != NULL) {
				// Проверяем формат имени объектов
				int y, m, d;
				res = sscanf(entry->d_name, "%d-%d-%d", &y, &m, &d);
				if (res != 3) {
					// Какой-то левый объект. Удаляем
					strcpy(_plogpath, entry->d_name);
					ret = removePath(_logpath);
					if (ret == -1) {
						closedir(dp);
						goto exit;
					}
				}
				else {
					// Это тапка с логами
					// Сравниваем имена папок. Т.к. имена - даты, то так канает
					if (strcmp(entry->d_name, buf) < 0) {	// Нашли более старое имя
						strcpy(buf, entry->d_name);
					}
					cnt++;
				}
			}

			if (cnt > LOGGING_DAYS_NUM) {	// Удаляем самую старую папку, если количесвто папок больше LOGGING_DAYS_NUM
				strcpy(_plogpath, buf);
				ret = removePath(_logpath);
				if (ret == -1) {
					closedir(dp);
					goto exit;
				}

				if (cnt == LOGGING_DAYS_NUM + 1) {	// Если после удаления папки папок остается LOGGING_DAYS_NUM, выходим
					break;
				}
			}
			else {
				break;
			}
		}

		closedir(dp);
		dp = NULL;
	}
	else {
		goto exit;
	}

	ret = 0;

	*(_plogpath - 1) = '/';
	sprintf(_plogpath, "%04d-%02d-%02d", ts->tm_year, ts->tm_mon, ts->tm_mday);

	dp = opendir(_logpath);
	if (dp == NULL) {	// Если папки нет, создаем её
		if (mkdir(_logpath, 0) != 0) {
			goto exit;
			ret = -1;
		}
	}
	else {
		closedir(dp);
	}

	strcat(_plogpath, "/");
	strcat(_plogpath, logdat[type].name);

	exit:

	return ret;
}

int SdCard::getCurrentLocalTime(struct tm* ts) {
	time_t tim = mtime.get();

	return mtime.convert(ts, tim);
}

// ------------------------------------------------------------------------------

SdCard sdcard;

extern "C" {
int print_log_ble_c(char *str) {
	return sdcard.printfLog(SdCard::LOG_COMMON, "[BLE%s", str);
}
}

