/*
 * MyTime.cpp
 *
 *  Created on: 24 авг. 2016 г.
 *      Author: Denis
 */

#include "MyTime.h"


/// Смещение времени относительно Гринвича в часах
#define GMT_OFFSET_HOURS							(0)
/// Смещение времени относительно Гринвича в секундах
#define GMT_OFFSET_SECONDS							(GMT_OFFSET_HOURS * 60 * 60)


MyTime::MyTime() {
//	set_time(0);
}

time_t MyTime::get() {
	_mutex.lock();
	time_t t = time(NULL);
	_mutex.unlock();

	return t;
}

void MyTime::set(time_t t) {
	_mutex.lock();
	set_time(t);
	_mutex.unlock();
}

int MyTime::convert(struct tm* ts, time_t t) {
	int ret = -1;

	t += GMT_OFFSET_SECONDS;			// Сдвиг на местное время

	_mutex.lock();

	struct tm* pts = localtime(&t);
	if (pts != NULL) {
		memcpy(ts, pts, sizeof(tm));
		ts->tm_year += 1900;
		ts->tm_mon += 1;
		ret = 0;
	}

	_mutex.unlock();

	return ret;
}

time_t MyTime::convert2UnixTime( struct tm ts ) {
	_mutex.lock();

	ts.tm_year -=1900;
	ts.tm_mon -=1;
	time_t unixTime = mktime(&ts) - GMT_OFFSET_SECONDS;

	_mutex.unlock();

	return (unixTime);
}


MyTime mtime;
