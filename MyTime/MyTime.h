/*
 * MyTime.h
 *
 *  Created on: 24 авг. 2016 г.
 *      Author: Denis
 */

#ifndef MYTIME_H_
#define MYTIME_H_

#include <mbed.h>
#include <rtos.h>

class MyTime {
public:
	MyTime();

	time_t get();

	void set(time_t t);

	int convert(struct tm* ts, time_t t);

	time_t convert2UnixTime( struct tm ptrtime );

private:
	Mutex _mutex;
};


extern MyTime mtime;

#endif /* MYTIME_H_ */
