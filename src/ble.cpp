/*
 * ble.cpp
 *
 *  Created on: 26 мая 2017 г.
 *      Author: Chuyec
 */

#include "mbed.h"
#include "rtos.h"
#include <SdCard/SdCard.h>

#include "common.h"
#include "Watchdog/Watchdog.h"
#include "MyTime/MyTime.h"
#include "Settings/Settings.h"
#include "EzeparkServer/EzeparkServer.h"
#include "trace/trace.h"
#include "nrf_dfu/Dfu.h"

#include "sdk_config.h"
#include "ble.h"

#include "ble_dis.h"
#include "ble_bcs.h"
#include "ble_uis.h"
#include "ble_ncs.h"
#include "ble_fus.h"
#include "ble_sss.h"
#include "ble_dos.h"
#include "ble_pps.h"

#include "ble_hci.h"
#include "nrf_log.h"
#include "nrf_log_ctrl.h"
#include "nrf_log_default_backends.h"
#include "nrf_ble_gatt.h"
#include "app_error.h"
#include "ble_gap.h"
#include "ble_conn_state.h"
#include "ble_conn_params.h"
#include "ble_db_discovery.h"
#include "ble_advertising.h"
#include "nrf_sdh.h"
#include "nrf_sdh_ble.h"
#include "nrf_sdh_soc.h"
#include "ser_app_hal.h"
#include "app_timer.h"

extern Watchdog wdg;
extern EzeparkServer EzServer;
extern Thread *p_net_thread;
extern osThreadId mainThreadID;
extern Mail<int8_t, 1> TemperatureMailbox;
extern uint8_t *p_DFU_Data;

extern void rebootMCU();
extern bool cardDetected();

const char fwpath[] = "/sd/bin/fw.bin";

//extern DigitalOut ledNet;
//extern DigitalOut ledBle;
//extern DigitalOut ledHealth;
//extern DigitalOut ledSD;
//extern void ledsState(LedType_t led, LedState_t state);

extern Settings<Sett_t> settings;
extern Config_t config;

enum Command {
	CMD_NONE,
	CMD_OPEN 					= 0x01,
	CMD_CLOSE					= 0x02,
	CMD_DISCONNECT				= 0x03,

	CMD_REBOOT 					= 0x10,
	CMD_SERVICE_MODE_ENTER		= 0x11,
	CMD_SERVICE_MODE_EXIT		= 0x12,
	CMD_SHUTDOWN				= 0x13,
	CMD_SD_CLEANUP				= 0x14,
	CMD_SET_DEFAULT				= 0x15,
	CMD_SAVE_SETTINGS			= 0x16,

	CMD_INTERNAL_DFU_OK			= 0xA0,
};

enum ComandResponse {
	CMD_RSP_NONE,

	CMD_RSP_SUCCESS				= 0x01,
	CMD_RSP_NO_BALANCE_ENTER	= 0x02,
	CMD_RSP_NO_BALANCE_EXIT		= 0x03,
	CMD_RSP_TECH_ISSUE			= 0x04,
	CMD_RSP_NOT_UPDATED			= 0x05,
	CMD_RSP_NO_SESSION			= 0x06,
	CMD_RSP_NO_USER				= 0x07,
	CMD_RSP_NO_UUID				= 0x08,
	CMD_RSP_GATE_BUSY			= 0x09,
	CMD_RSP_GATE_OFF			= 0x0A,
	CMD_RSP_NO_PLACES			= 0x0B,
	CMD_SESSION_IS_OPENED		= 0x0C,

	CMD_RSP_SDC_CLEANUP_ERROR	= 0x20,
	CMD_RSP_NO_PRIVILEGES		= 0x21,
	CMD_RSP_SETTINGS_SAVE_OK	= 0x22,
	CMD_RSP_SETTINGS_SAVE_FAIL	= 0x23,
};

enum SelftestType_e {
	CARD_DETECTED		= 0x01,
	CARD_HEALTH_OK		= 0x02,
	PHY_OK				= 0x04,
	CABLE_CONNECTED		= 0x08,
	NET_OK				= 0x10,
	SERVER_CONNECTED	= 0x20,
	SERVER_DATA_OK		= 0x40,
	GATE_FEEDBACK_OK	= 0x80
};

enum DFUStatus_e {
	DFU_NONE,
	DFU_RX_READY = 1,
	DFU_INTEGRITY_FAIL,
	DFU_INTERNAL_ERROR,
	DFU_OK
};

#define TIMEOUT_OF_OPEN					35*1000
#define TIMEOUT_REANIMATE_GATE			30*1000
#if TIMEOUT_REANIMATE_GATE >= TIMEOUT_OF_OPEN
#error "TIMEOUT_REANIMATE_GATE >= TIMEOUT_OF_OPEN"
#endif

#define DFU_START_PACKET_COUNTER		0x0000	//!< Счетчик в стартовом пакете
#define DFU_FINISH_PACKET_COUNTER		0xFFFF	//!< Счетчик в заключительном пакете, содержащем CRC
#define DFU_MAX_DATA_LEN				18		//!< Максимальное количество данных прошивки, передаваемое в пакете

bool BleAdvertisingOn = false;
bool BleConnected = false;

Mutex BLE_Mutex;
Mutex FW_Mutex;

const static uint8_t ServiceAppUuid[16] = {0xd7, 0x9d, 0xa8, 0x1d, 0x7c, 0xda, 0x49, 0x35, 0x93, 0x11, 0x20, 0xbe, 0x9d, 0x38, 0x61, 0x11}; // "d79da81d-7cda-4935-9311-20be9d386111"
const static uint32_t ServiceAppUserID = 1;

#define NAME_MAX_LEN		31 - 18

static char device_name[NAME_MAX_LEN + 1] = "Ez";	// NAME_MAX_LEN + '\0'
static uint8_t response;
static uint32_t user_id;
static uint8_t smph_uuid[16];
static uint8_t dev_type;
static uint32_t dev_id;
static uint8_t dev_number = 0;
static uint8_t fw_version[3] = {
#include <version/version.txt>
};
static uint8_t tx_pwr_index;
static int8_t tx_pwr_dbm;
static int8_t beac_trh;
static int8_t temperature = -127;

// Массив мощностей сигала
/// @note Индексация начинается с 1
static int8_t tx_pwr_val[TX_POWER_VALUES_NUMBER] = {-40, -20, -16, -12, -8, -4, 0, 3, 4};
//
static bool openedByEzeparkUser = false;
static bool openedByEzeparkBeacon = false;
static time_t evtm;
static bool authorised = false;
static bool isServiceModeOn = false;
static bool bleNormalModeRequested = false;
static bool bleServiceModeRequested = false;
static Timer openGateTimer;
static time_t timeSinceNotConected;
static time_t timeSinceServiceModeEnter;

static uint8_t beacon_mac[6];
static uint8_t selftest_status = 0;
static Sett_t localSett;
static uint16_t fus_packet_cnt = 0;
static CRC32 crc;
static bool dos_selftest_notification_subscribed = false;

/// Состояния шлагбаума
typedef enum {
	GATE_BEGIN,		//!< Стартовое состояние в начале программы
	GATE_OPENED,	//!< Зарегестрировано событие открытия шлагбаума
	GATE_IS_OPEN,	//!< Шлагбаум находится в состоянии "Открыт"
	GATE_NORMAL,	//!< Шлагбаум работает в нормальном режиме, ожидая команд
	GATE_ISSUE,		//!< Шлагбаум не закрылся после открытия по истечению таймаута
}gateState_t;
static gateState_t gateState = GATE_BEGIN;

typedef enum {
	REQUEST_NONE,
	PHONE_REUEST,
	BEACON_REQUEST
}RequestDevType_t;

#define APP_BLE_CONN_CFG_TAG               1                                       /**< A tag identifying the SoftDevice BLE configuration. */
#define APP_BLE_OBSERVER_PRIO              1                                       /**< Application's BLE observer priority. You shouldn't need to modify this value. */													/**< A tag that refers to the BLE stack configuration we set with @ref sd_ble_cfg_set. Default tag is @ref BLE_CONN_CFG_TAG_DEFAULT. */
#define APP_FEATURE_NOT_SUPPORTED          BLE_GATT_STATUS_ATTERR_APP_BEGIN + 2    /**< Reply when unsupported features are requested. *///

#define MANUFACTURER_NAME                "Ezepark"				                    /**< Manufacturer. Will be passed to Device Information Service. */
#define APP_ADV_INTERVAL                 32                                         /**< The advertising interval (in units of 0.625 ms. This value corresponds to 187.5 ms). */
#define APP_ADV_TIMEOUT_IN_SECONDS       0                                          /**< The advertising timeout in units of seconds. */

//#define MIN_CONN_INTERVAL                MSEC_TO_UNITS(40, UNIT_1_25_MS)           /**< Minimum acceptable connection interval (0.04 seconds). */
//#define MAX_CONN_INTERVAL                MSEC_TO_UNITS(650, UNIT_1_25_MS)           /**< Maximum acceptable connection interval (0.65 second). */
//
//#define SLAVE_LATENCY                    0                                          /**< Slave latency. */
//#define CONN_SUP_TIMEOUT                 MSEC_TO_UNITS(4000, UNIT_10_MS)            /**< Connection supervisory timeout (4 seconds). */

#define FIRST_CONN_PARAMS_UPDATE_DELAY		2000                      /**< Time from initiating event (connect or start of notification) to first time sd_ble_gap_conn_param_update is called (5 seconds). */
#define NEXT_CONN_PARAMS_UPDATE_DELAY		5000                     /**< Time between each call to sd_ble_gap_conn_param_update after the first call (30 seconds). */
#define MAX_CONN_PARAMS_UPDATE_COUNT     	3                                          /**< Number of attempts before giving up the connection parameter negotiation. */

#define SCAN_INTERVAL                   0x00A0                                      /**< Determines scan interval in units of 0.625 millisecond. */
#define SCAN_WINDOW                     0x0050                                      /**< Determines scan window in units of 0.625 millisecond. */
#define SCAN_TIMEOUT                    0

static uint16_t  m_conn_handle = BLE_CONN_HANDLE_INVALID;	/**< Handle of the current connection. */

BLE_BCS_DEF(m_bcs);
BLE_DOS_DEF(m_dos);
BLE_UIS_DEF(m_uis);
BLE_NCS_DEF(m_ncs);
BLE_FUS_DEF(m_fus);
BLE_SSS_DEF(m_sss);
BLE_PPS_DEF(m_pps);

NRF_BLE_GATT_DEF(m_gatt);                                           /**< GATT module instance. */
BLE_ADVERTISING_DEF(m_advertising);                                 /**< Advertising module instance. */
//BLE_DB_DISCOVERY_DEF(m_db_discovery);                               /**< Database discovery module instances. */

//
static void advertising_start();
static void advertising_stop(void);
static void advertising_init(void);
static void init_from_settings(void);


class Gate {
public:
	Gate(PinName ctrl, PinName fb) : _ctrl(ctrl, 0), _fb_real(fb), _fb_dummy(false), cnt_100ms(0) {
	}

	bool open () {
		bool ret = true;

		if (!isOpen()) {
			_ctrl = 1;

			if (settings.data.enable_gate_feedback) {
				// Ожидаем открытия шлагбаума 30*100 мс
				cnt_100ms = 0;
				while (!isOpen()) {
					if (++cnt_100ms > 30) {
						ret = false;
						break;
					}
					Thread::wait(100);
				}
			}
			else {
				Thread::wait(500);
				_fb_dummy = true;
			}

			if (ret) {
				fb_dummy_timer.reset();
				fb_dummy_timer.start();
			}
		}

		release();

		return ret;
	}

	bool openWithoutFeedback () {
		_ctrl = 1;

		Thread::wait(1000);

		release();

		return true;
	}

	bool close() {
		return false;

		bool ret = true;

		if (isOpen()) {
			_ctrl = 1;

			// Ожидаем закрытия шлагбаума 30*100 мс
			cnt_100ms = 0;
			while (isOpen()) {
				if (++cnt_100ms > 30) {
					ret = false;
					break;
				}
				Thread::wait(100);
			}
		}

		release();

		return ret;
	}

	bool isOpen() {
		bool fb;

		if (settings.data.enable_gate_feedback) {
			fb = _fb_dummy = (bool)_fb_real;
		}
		else {
			if (_fb_dummy) {
				if (fb_dummy_timer.read_ms() > fb_dummy_time_ms) {
					_fb_dummy = false;
				}
			}

			fb = _fb_dummy;
		}

		if (!_fb_dummy) {
			fb_dummy_timer.stop();
		}

		return fb;
	}

private:
	DigitalOut _ctrl;
	DigitalIn _fb_real;

	bool _fb_dummy;
	Timer fb_dummy_timer;
	const int fb_dummy_time_ms = 10000;

	uint8_t cnt_100ms;

	void release() {
		_ctrl = 0;
	}
};
static Gate gate(PF_9, PF_8);

typedef struct __attribute__((packed)) {
	uint8_t beacon_mac[6];
}BeaconMAC_t;

Mail<BeaconMAC_t, 1> BeaconMailbox;

typedef struct __attribute__((packed)) {
	uint8_t uuid[16];
	uint32_t userid;
}UUID_useid_t;

Mail<UUID_useid_t, 1> uuidMailbox;

Mail<uint8_t, 4> CmdMailbox;

#define MAX_BEACON_NUM				50
#define BEACON_RSSI_TIMEOUT			10
#define BEACON_BILLING_TIMEOUT		15
#define BEACON_AVERAGE_FACTOR		1
#define BEACON_RESET_RSSI			-110

typedef struct {
	time_t lastActivity;	// Время последней видимости бикона
	time_t eventTime;		// Время, когда по бикону было событие
//	uint8_t uuid[17];
	uint8_t mac[7];
	int8_t rssi;
}Beacon_t;

static Beacon_t beacons[MAX_BEACON_NUM];

static Beacon_t* getOlderBeacon() {
	time_t tm = -1 & ~(1 << 31);
	Beacon_t* beac = NULL;

	for (uint8_t q = 0; q < MAX_BEACON_NUM; q++) {
		if (beacons[q].lastActivity < tm) {
			tm = beacons[q].lastActivity;
			beac = &beacons[q];
		}
	}

	return beac;
}

static Beacon_t* findActiveBeacon(uint8_t* mac) {
	for (uint8_t q = 0; q < MAX_BEACON_NUM; q++) {
		if (memcmp(mac, beacons[q].mac, 6) == 0) {
			return &beacons[q];
		}
	}

	return NULL;
}

static int8_t getAverageRssi(uint8_t* mac, int8_t rssi) {
	time_t tm = mtime.get();
	Beacon_t* beac = findActiveBeacon(mac);

	if (beac != NULL) {	// Бикон уже заметили ранее
		if (tm - beac->lastActivity > BEACON_RSSI_TIMEOUT) { // Если давно не виделся в поле зрения, сбрасываем rssi
			beac->rssi = BEACON_RESET_RSSI;
		}
	}
	else { // Это новый бикон. Удаляем самый старый и ставим вместо него новый
		beac = getOlderBeacon();
		memcpy(beac->mac, mac, 6);
		beac->mac[6] = '\0';
		beac->rssi = BEACON_RESET_RSSI;
		beac->eventTime = 0;
	}

	beac->lastActivity = tm;
	beac->rssi = (beac->rssi * (BEACON_AVERAGE_FACTOR - 1) + rssi) / BEACON_AVERAGE_FACTOR;

	return beac->rssi;
}

static void resetRssi(uint8_t* mac) {
	Beacon_t* beac = findActiveBeacon(mac);
	if (beac) {
		beac->rssi = BEACON_RESET_RSSI;
	}
}

/// ======================================
/// Central
/// ======================================

/**@brief Parameters used when scanning.
 */
static ble_gap_scan_params_t const m_scan_params = {
		active : 1,
		use_whitelist : 0,
		adv_dir_report : 0,
		interval : SCAN_INTERVAL,
		window : SCAN_WINDOW,
		timeout : SCAN_TIMEOUT
};

/**@brief Function for handling database discovery events.
 *
 * @details This function is callback function to handle events from the database discovery module.
 *          Depending on the UUIDs that are discovered, this function should forward the events
 *          to their respective services.
 *
 * @param[in] p_event  Pointer to the database discovery event.
 */
static void db_disc_handler(ble_db_discovery_evt_t * p_evt) {

}

/**@brief Function for handling BLE Stack events concerning central applications.
 *
 * @details This function keeps the connection handles of central applications up-to-date. It
 * parses scanning reports, initiating a connection attempt to peripherals when a target UUID
 * is found, and manages connection parameter update requests. Additionally, it updates the status
 * of LEDs used to report central applications activity.
 *
 * @note        Since this function updates connection handles, @ref BLE_GAP_EVT_DISCONNECTED events
 *              should be dispatched to the target application before invoking this function.
 *
 * @param[in]   p_ble_evt   Bluetooth stack event.
 */
static void on_ble_central_evt(const ble_evt_t * const p_ble_evt) {
    uint8_t * pmac = (uint8_t*)p_ble_evt->evt.gap_evt.params.adv_report.peer_addr.addr;
    int8_t rssi = p_ble_evt->evt.gap_evt.params.adv_report.rssi;

    if (p_ble_evt->header.evt_id == BLE_GAP_EVT_ADV_REPORT) {
    	int8_t avg_rssi = getAverageRssi(pmac, rssi);
		if (avg_rssi >= beac_trh) {
			Beacon_t* beac = findActiveBeacon(pmac);

			if (mtime.get() - beac->eventTime > BEACON_BILLING_TIMEOUT) {
				beac->eventTime = mtime.get();

				BeaconMAC_t *mail = BeaconMailbox.alloc();
				if (mail) {
					mail->beacon_mac[0] = pmac[5];
					mail->beacon_mac[1] = pmac[4];
					mail->beacon_mac[2] = pmac[3];
					mail->beacon_mac[3] = pmac[2];
					mail->beacon_mac[4] = pmac[1];
					mail->beacon_mac[5] = pmac[0];
					BeaconMailbox.put(mail);
				}
			}
			resetRssi(pmac);
		}
    }
}

/**
 * @brief Database discovery initialization.
 */
static void db_discovery_init(void)
{
	if (!bleServiceModeRequested) {
		ret_code_t err_code = ble_db_discovery_init(db_disc_handler);
		if (err_code != NRF_SUCCESS) {
			NRF_LOG_ERROR("ble_db_discovery_init(), code 0x%x", err_code);
			APP_ERROR_CHECK(err_code);
		}
	}
}

/**@brief Function for initiating scanning.
 */
static void scan_start(void)
{
	if (config.scan == true) {
		if (!bleServiceModeRequested) {
			ret_code_t err_code;

			(void) sd_ble_gap_scan_stop();

			err_code = sd_ble_gap_scan_start(&m_scan_params);
			// It is okay to ignore this error since we are stopping the scan anyway.
			if (err_code != NRF_ERROR_INVALID_STATE && err_code != NRF_SUCCESS) {
				NRF_LOG_ERROR("sd_ble_gap_scan_start(), code 0x%x", err_code);
				APP_ERROR_CHECK(err_code);
			}
		}
	}
}

static void scan_stop(void)
{
	if (config.scan == true) {
		ret_code_t err_code;

		err_code = sd_ble_gap_scan_stop();
		if (err_code != NRF_SUCCESS) {
			NRF_LOG_ERROR("sd_ble_gap_scan_stop(), code 0x%x", err_code);
			APP_ERROR_CHECK(err_code);
		}
	}
}

/// ======================================
/// Peripheral
/// ======================================

static void setName() {
	snprintf(device_name, NAME_MAX_LEN, "Ez%u%c", (unsigned int)dev_id, dev_type == DEVICE_TYPE_IN ? 'N' : (dev_type == DEVICE_TYPE_OUT ? 'X' : 'F'));

	ble_gap_conn_sec_mode_t sec_mode;
	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);

	uint32_t err_code = sd_ble_gap_device_name_set(&sec_mode, (const uint8_t *)device_name, strlen(device_name));
	if (err_code != NRF_SUCCESS) {
		NRF_LOG_ERROR("sd_ble_gap_device_name_set(), code 0x%x", err_code);
		APP_ERROR_CHECK(err_code);
	}

	advertising_init();
}

static void ble_disconnect() {
	if (m_conn_handle != BLE_CONN_HANDLE_INVALID) {
		uint32_t err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
		if (err_code != NRF_SUCCESS) {
			NRF_LOG_ERROR("sd_ble_gap_disconnect(), code 0x%x", err_code);
			APP_ERROR_CHECK(err_code);
		}
	}
}

static uint32_t ble_cmd_write_response_chr(void * data, uint16_t len) {
    ble_gatts_hvx_params_t params;

    memset(&params, 0, sizeof(params));
    params.type = BLE_GATT_HVX_NOTIFICATION;
    params.handle = m_bcs.response_handles.value_handle;
    params.p_data = (const uint8_t*)data;
    params.p_len = &len;

    uint32_t err_code = sd_ble_gatts_hvx(m_conn_handle, &params);
	if (err_code != NRF_SUCCESS) {
		NRF_LOG_ERROR("sd_ble_gatts_hvx(), code 0x%x", err_code);
		APP_ERROR_CHECK(err_code);
	}

	return err_code;
}

static uint32_t ble_cmd_write_response(uint8_t response_code) {
	return ble_cmd_write_response_chr(&response_code, 1);
}

static uint32_t ble_cmd_write_response(uint8_t response_code, time_t tm) {
    uint16_t len;

    struct __packed Response {
    	uint8_t code;
    	uint32_t dev_id;
    	uint32_t tm;
    } rsp = { .code = response_code };

    bool net_ok = (EzServer.checkPHY() && EzServer.isCableConnected() && EzServer.isLink() && EzServer.connectedToServer());

    if (rsp.code == CMD_RSP_SUCCESS && !net_ok && !isServiceModeOn) {	// Расширенный ответ не делаем в сервисное приложение
		core_util_critical_section_enter();
		rsp.dev_id = settings.data.devID;
		core_util_critical_section_exit();
    	rsp.tm = tm;

    	len = sizeof(rsp);
    }
    else {
    	len = 1;
    }

	return ble_cmd_write_response_chr(&rsp, len);
}

static void bcs_write_handler (ble_bcs_t * p_bcs, ble_gatts_evt_write_t const * p_evt_write) {
	if (p_evt_write->handle == p_bcs->command_handles.value_handle) {
		uint8_t const *pval = &p_evt_write->data[0];
		uint32_t err_code;
		uint8_t rsp = CMD_RSP_NONE;

		if (!isServiceModeOn) {
			if (p_evt_write->len == COMMAND_UUID_CHAR_LEN) {
				// Запись в эту характеристику равна команде на открытие из UserApp или авторизации из ServiceApp
				if (memcmp(pval, ServiceAppUuid, 16) == 0 && *((uint32_t*)(&pval[16])) == ServiceAppUserID) {
					bleServiceModeRequested = true;
					scan_stop();
					rsp = CMD_RSP_SUCCESS;
					err_code = ble_bcs_status_update(p_bcs, rsp);
					if (err_code != NRF_SUCCESS) {
						NRF_LOG_ERROR("ble_bcs_status_update(), code 0x%x", err_code);
						APP_ERROR_CHECK(err_code);
					}
					///\todo Тут нужно ждать, пока придет ивент об успешной нотификации
					ble_disconnect();
				}
				else {
					UUID_useid_t *mail = uuidMailbox.alloc();
					if (mail) {
						memcpy(mail->uuid, pval, 16);
						mail->userid = *((uint32_t*)(&pval[16]));
						uuidMailbox.put(mail);
					}
				}

				authorised = true;
			}
			else {
				rsp = CMD_RSP_NO_PRIVILEGES;
				err_code = ble_bcs_status_update(p_bcs, rsp);
				if (err_code != NRF_SUCCESS) {
					NRF_LOG_ERROR("ble_bcs_status_update(), code 0x%x", err_code);
					APP_ERROR_CHECK(err_code);
				}
				ble_disconnect();
			}
		}
		else {
			// Запись в эту характеристику подтверждает подключение сервисного приложения
			if (p_evt_write->len == COMMAND_UUID_CHAR_LEN && memcmp(pval, ServiceAppUuid, 16) == 0 && *((uint32_t*)(&pval[16])) == ServiceAppUserID) {
				authorised = true;
				rsp = CMD_RSP_SUCCESS;
				err_code = ble_bcs_status_update(p_bcs, rsp);
				if (err_code != NRF_SUCCESS) {
					NRF_LOG_ERROR("ble_bcs_status_update(), code 0x%x", err_code);
					APP_ERROR_CHECK(err_code);
				}
			}
			else if (authorised && p_evt_write->len == 1) {
				uint8_t *mail = CmdMailbox.alloc();
				if (mail) {
					*mail = *pval;
					CmdMailbox.put(mail);
				}
				else {
					rsp = CMD_RSP_NO_PRIVILEGES;
					err_code = ble_bcs_status_update(p_bcs, rsp);
					if (err_code != NRF_SUCCESS) {
						NRF_LOG_ERROR("ble_bcs_status_update(), code 0x%x", err_code);
						APP_ERROR_CHECK(err_code);
					}
				}
			}
		}
	}
}

static void uis_write_handler (ble_uis_t * p_uis, ble_gatts_evt_write_t const * p_evt_write) {
	if (authorised) {
		if (p_evt_write->handle == p_uis->devid_handles.value_handle) {
			if (p_evt_write->len == DEVID_UUID_CHAR_LEN) {
				memcpy(&localSett.devID, &p_evt_write->data[0], sizeof(uint32_t));
			}
		}
		else if (p_evt_write->handle == p_uis->devtype_handles.value_handle) {
			// Read only
		}
		else if (p_evt_write->handle == p_uis->devnumber_handles.value_handle) {
			if (p_evt_write->len == DEVNUMBER_UUID_CHAR_LEN) {
				localSett.dev_number = p_evt_write->data[0];
			}
		}
	}
}

static void ncs_write_handler (ble_ncs_t * p_ncs, ble_gatts_evt_write_t const * p_evt_write) {
	if (authorised) {
		if (p_evt_write->handle == p_ncs->mode_handles.value_handle) {
			if (p_evt_write->len == NET_MODE_UUID_CHAR_LEN) {
				if (p_evt_write->data[0] < NET_MODE_NUM) {
					localSett.net_mode = p_evt_write->data[0];
				}
			}
		}
		else if (p_evt_write->handle == p_ncs->ipaddr_handles.value_handle) {
			if (p_evt_write->len == IPADDR_UUID_CHAR_LEN) {
				if (localSett.net_mode == NET_MODE_STATIC) {
					memcpy(&localSett.ip, &p_evt_write->data[0], sizeof(uint32_t));
				}
			}
		}
		else if (p_evt_write->handle == p_ncs->snmask_handles.value_handle) {
			if (p_evt_write->len == SNMASK_UUID_CHAR_LEN) {
				if (localSett.net_mode == NET_MODE_STATIC) {
					memcpy(&localSett.mask, &p_evt_write->data[0], sizeof(uint32_t));
				}
			}
		}
		else if (p_evt_write->handle == p_ncs->gateway_handles.value_handle) {
			if (p_evt_write->len == GATEWAY_UUID_CHAR_LEN) {
				if (localSett.net_mode == NET_MODE_STATIC) {
					memcpy(&localSett.gateway, &p_evt_write->data[0], sizeof(uint32_t));
				}
			}
		}
		else if (p_evt_write->handle == p_ncs->dns1_handles.value_handle) {
			if (p_evt_write->len == DNS1_UUID_CHAR_LEN) {
				if (localSett.net_mode == NET_MODE_STATIC) {
					memcpy(&localSett.dns1, &p_evt_write->data[0], sizeof(uint32_t));
				}
			}
		}
		else if (p_evt_write->handle == p_ncs->dns2_handles.value_handle) {
			if (p_evt_write->len == DNS2_UUID_CHAR_LEN) {
				if (localSett.net_mode == NET_MODE_STATIC) {
					memcpy(&localSett.dns2, &p_evt_write->data[0], sizeof(uint32_t));
				}
			}
		}
		else if (p_evt_write->handle == p_ncs->mac_handles.value_handle) {
			if (p_evt_write->len == MAC_UUID_CHAR_LEN) {
				if ((p_evt_write->data[0] & 0x03) == 0x02) {
					memcpy(&localSett.mac, &p_evt_write->data[0], MAC_UUID_CHAR_LEN);
				}
			}
		}
		else if (p_evt_write->handle == p_ncs->timeout_handles.value_handle) {
			if (p_evt_write->len == TIMEOUT_UUID_CHAR_LEN) {
				localSett.con_timeout = p_evt_write->data[0];
			}
		}
	}
}

static void fus_write_handler (ble_fus_t * p_fus, ble_gatts_evt_write_t const * p_evt_write) {
		typedef enum {
			DFU_STATE_NONE,
			DFU_STATE_START,
			DFU_STATE_PROCESS,
			DFU_STATE_FINISH
		} dfu_state_t;
		static dfu_state_t dfu_state = DFU_STATE_NONE;
		static uint32_t fwSize = 0;
		static uint32_t writtenBytes = 0;

	if (authorised) {
		if (p_evt_write->handle == p_fus->fwdata_handles.value_handle) {
			uint8_t rsp = DFU_NONE;
			uint16_t packet_cnt;
			memcpy(&packet_cnt, &p_evt_write->data[0], sizeof(uint16_t));
			uint8_t const *pdata = &p_evt_write->data[2];

			///\todo Сделать лок мьютекса FW_Mutex.trylock()

			if (fus_packet_cnt == 0) {
				dfu_state = DFU_STATE_NONE;
			}

			if (dfu_state == DFU_STATE_NONE && packet_cnt == DFU_START_PACKET_COUNTER) {
				writtenBytes = 0;
				dfu_state = DFU_STATE_START;
			}

			switch (dfu_state) {
			case DFU_STATE_START:
				fwSize = *((uint32_t*)pdata);
				if (fwSize <= DFU_FIRMWARE_MAX_SIZE - 8) {	// -8, потому что в начало кладём размер, а в конец - crc
					memcpy(p_DFU_Data, &fwSize, sizeof(uint32_t));
					rsp = DFU_RX_READY;
					dfu_state = DFU_STATE_PROCESS;
				}
				else {
					rsp = DFU_INTEGRITY_FAIL;
					dfu_state = DFU_STATE_NONE;
				}
				break;

			case DFU_STATE_PROCESS:
				if (fus_packet_cnt == packet_cnt) {
					int32_t len = fwSize - writtenBytes;
					if (len > 0) {
						len = len < DFU_MAX_DATA_LEN ? len : DFU_MAX_DATA_LEN;
						memcpy(&p_DFU_Data[writtenBytes + 4], pdata, len);
						writtenBytes += len;

						if (writtenBytes == fwSize) {
							dfu_state = DFU_STATE_FINISH;
						}
					}
					else {
						rsp = DFU_INTEGRITY_FAIL;
						dfu_state = DFU_STATE_NONE;
					}
				}
				else {
					rsp = DFU_INTEGRITY_FAIL;
					dfu_state = DFU_STATE_NONE;
				}
				break;

			case DFU_STATE_FINISH:
				if (packet_cnt == DFU_FINISH_PACKET_COUNTER) {
					uint32_t crc_in = *((uint32_t*)pdata);
					uint32_t crc_loc = crc.get(p_DFU_Data + 4, writtenBytes);

					if (crc_in == crc_loc) {
						memcpy(p_DFU_Data + 4 + writtenBytes, &crc_loc, sizeof(uint32_t));
						uint8_t *mail = CmdMailbox.alloc();
						if (mail) {
							*mail = CMD_INTERNAL_DFU_OK;
							CmdMailbox.put(mail);
						}
						rsp = DFU_OK;
					}
					else {
						rsp = DFU_INTEGRITY_FAIL;
					}
					dfu_state = DFU_STATE_NONE;
				}
				else {
					rsp = DFU_INTEGRITY_FAIL;
					dfu_state = DFU_STATE_NONE;
				}
				break;

			default:
				rsp = DFU_INTEGRITY_FAIL;
				dfu_state = DFU_STATE_NONE;
				break;
			}

			if (rsp != DFU_NONE && rsp != DFU_OK) {	///\todo Убрать DFU_OK, если делать в бутлодере прошивку из SRAM
				uint32_t err_code = ble_fus_status_update(p_fus, rsp);
				if (err_code != NRF_SUCCESS) {
					NRF_LOG_ERROR("ble_fus_status_update(), code 0x%x", err_code);
					APP_ERROR_CHECK(err_code);
				}
			}

			if (rsp == DFU_NONE || rsp == DFU_RX_READY) {
				fus_packet_cnt++;
			}
			else {
				fus_packet_cnt = 0;
			}
		}
	}
}

static void sss_write_handler (ble_sss_t * p_sss, ble_gatts_evt_write_t const * p_evt_write) {
	if (authorised) {
		if (p_evt_write->handle == p_sss->txpower_handles.value_handle) {
			if (p_evt_write->len == TXPOWER_UUID_CHAR_LEN) {
				uint8_t val = p_evt_write->data[0];
				if (val && val <= TX_POWER_VALUES_NUMBER) {
					localSett.tx_pwr = val;
				}
			}
		}
		else if (p_evt_write->handle == p_sss->beaconthr_handles.value_handle) {
			if (p_evt_write->len == BEACONTHR_UUID_CHAR_LEN) {
				int8_t val = (int8_t)p_evt_write->data[0];
				if (val > -110 && val < 0) {
					localSett.beac_trh = val;
				}
			}
		}
	}
}

static void dos_write_handler (ble_dos_t * p_dos, ble_gatts_evt_write_t const * p_evt_write) {
	if (p_evt_write->handle == p_dos->selftest_handles.cccd_handle) {	// При проверке нотификации не смотрим на авторизацию, потому что подписка может прийти до авторизации
		if (p_evt_write->len == 2) {
			uint8_t val = p_evt_write->data[0];
			if (val == 1) {
				dos_selftest_notification_subscribed = true;
			}
			else {
				dos_selftest_notification_subscribed = false;
			}
		}
	}

	if (authorised) {
		if (p_evt_write->handle == p_dos->selftest_handles.value_handle) {
			// Read only
		}
		else if (p_evt_write->handle == p_dos->mode_handles.value_handle) {
			// Read only
		}
		else if (p_evt_write->handle == p_dos->logging_handles.value_handle) {
			if (p_evt_write->len == LOGGING_UUID_CHAR_LEN) {
				uint8_t val = p_evt_write->data[0];
				if (val < LOGS_MODE_NUM) {
					localSett.logs_mode = val;
				}
			}
		}
	}
}

static void pps_write_handler (ble_pps_t * p_pps, ble_gatts_evt_write_t const * p_evt_write) {
	if (authorised) {
		if (p_evt_write->handle == p_pps->temperature_handles.value_handle) {
			// Read only
		}
	}
}

/**@brief Function for checking if a bluetooth stack event is an advertising timeout.
 *
 * @param[in] p_ble_evt Bluetooth stack event.
 */
static bool ble_evt_is_advertising_timeout(ble_evt_t const * p_ble_evt) {
	  return ( (p_ble_evt->header.evt_id == BLE_GAP_EVT_TIMEOUT) && (p_ble_evt->evt.gap_evt.params.timeout.src == BLE_GAP_TIMEOUT_SRC_ADVERTISING) );
}

//static void evt_log(ble_evt_t * p_ble_evt) {
//	NRF_LOG_INFO("Received event, code 0x%x", p_ble_evt->header.evt_id);
//	ble_gap_conn_params_t conn_params;
//
//	switch (p_ble_evt->header.evt_id) {
//	case BLE_GAP_EVT_CONNECTED:
//		NRF_LOG_INFO("\n");
//		NRF_LOG_INFO("BLE_GAP_EVT_CONNECTED");
//		break;
//	case BLE_GAP_EVT_DISCONNECTED:
//		NRF_LOG_INFO("BLE_GAP_EVT_DISCONNECTED");
//		break;
//	case BLE_GAP_EVT_CONN_PARAM_UPDATE:
//		NRF_LOG_INFO("BLE_GAP_EVT_CONN_PARAM_UPDATE");
//		break;
//	case BLE_GAP_EVT_DATA_LENGTH_UPDATE_REQUEST:
//		NRF_LOG_INFO("BLE_GAP_EVT_DATA_LENGTH_UPDATE_REQUEST");
//		break;
//	case BLE_GAP_EVT_DATA_LENGTH_UPDATE:
//		NRF_LOG_INFO("BLE_GAP_EVT_DATA_LENGTH_UPDATE");
//		break;
//	case BLE_GATTC_EVT_EXCHANGE_MTU_RSP:
//		NRF_LOG_INFO("BLE_GATTC_EVT_EXCHANGE_MTU_RSP");
//		break;
//	case BLE_GATTS_EVT_WRITE:
//		NRF_LOG_INFO("BLE_GATTS_EVT_WRITE");
//		break;
//	case BLE_GATTS_EVT_EXCHANGE_MTU_REQUEST:
//		NRF_LOG_INFO("BLE_GATTS_EVT_EXCHANGE_MTU_REQUEST");
//		break;
//	case BLE_GATTS_EVT_HVN_TX_COMPLETE:
//		NRF_LOG_INFO("BLE_GATTS_EVT_HVN_TX_COMPLETE");
//		break;
//	default:
//		break;
//	}
//
//	if (p_ble_evt->header.evt_id == BLE_GAP_EVT_CONNECTED) {
//		conn_params = p_ble_evt->evt.gap_evt.params.connected.conn_params;
//		NRF_LOG_INFO("Conn params: conn_int %d, slv_lat %d, sup_timeout %d", 1.25f*conn_params.max_conn_interval, conn_params.slave_latency, 10.0f*conn_params.conn_sup_timeout);
//	}
//	else if (p_ble_evt->header.evt_id == BLE_GAP_EVT_CONN_PARAM_UPDATE) {
//		conn_params = p_ble_evt->evt.gap_evt.params.conn_param_update.conn_params;
//		NRF_LOG_INFO("Conn params: conn_int %d, slv_lat %d, sup_timeout %d", 1.25f*conn_params.max_conn_interval, conn_params.slave_latency, 10.0f*conn_params.conn_sup_timeout);
//	}
//}

/**@brief   Function for handling BLE events from peripheral applications.
 * @details Updates the status LEDs used to report the activity of the peripheral applications.
 *
 * @param[in]   p_ble_evt   Bluetooth stack event.
 */
static void on_ble_peripheral_evt(ble_evt_t const * p_ble_evt)
{
    ret_code_t err_code;

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
            NRF_LOG_INFO("Connected.");
            m_conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
			BleAdvertisingOn = false;
			BleConnected = true;
            break;

        case BLE_GAP_EVT_DISCONNECTED:
			uint8_t reason;
			reason = p_ble_evt->evt.gap_evt.params.disconnected.reason;
			NRF_LOG_INFO("Disconnected, reason %d", reason);
			switch(reason) {
			case BLE_HCI_STATUS_CODE_INVALID_BTLE_COMMAND_PARAMETERS:
				NRF_LOG_INFO("BLE_HCI_STATUS_CODE_INVALID_BTLE_COMMAND_PARAMETERS");
				break;
			case BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION:
				NRF_LOG_INFO("BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION");
				break;
			case BLE_HCI_REMOTE_DEV_TERMINATION_DUE_TO_LOW_RESOURCES:
				NRF_LOG_INFO("BLE_HCI_REMOTE_DEV_TERMINATION_DUE_TO_LOW_RESOURCES");
				break;
			case BLE_HCI_REMOTE_DEV_TERMINATION_DUE_TO_POWER_OFF:
				NRF_LOG_INFO("BLE_HCI_REMOTE_DEV_TERMINATION_DUE_TO_POWER_OFF");
				break;
			case BLE_HCI_LOCAL_HOST_TERMINATED_CONNECTION:
				NRF_LOG_INFO("BLE_HCI_LOCAL_HOST_TERMINATED_CONNECTION");
				break;
			case BLE_HCI_STATUS_CODE_LMP_RESPONSE_TIMEOUT:
				NRF_LOG_INFO("BLE_HCI_STATUS_CODE_LMP_RESPONSE_TIMEOUT");
				break;
			case BLE_HCI_INSTANT_PASSED:
				NRF_LOG_INFO("BLE_HCI_INSTANT_PASSED");
				break;
			case BLE_HCI_CONNECTION_TIMEOUT:
				NRF_LOG_INFO("BLE_HCI_CONNECTION_TIMEOUT");
				break;
			case BLE_HCI_DIFFERENT_TRANSACTION_COLLISION:
				NRF_LOG_INFO("BLE_HCI_DIFFERENT_TRANSACTION_COLLISION");
				break;
			}

			m_conn_handle = BLE_CONN_HANDLE_INVALID;
			authorised = false;
			BleConnected = false;
			dos_selftest_notification_subscribed = false;
			if (isServiceModeOn) {
				isServiceModeOn = false;
				bleNormalModeRequested = true;
			}

//			// Если шлагбаум закрыт после дисконнекта, то начинаем адвертайзить тут
//			if (!gate.isOpen()) {
//				advertising_start();	///\todo
//			}
            break;

#if defined(S132)
        case BLE_GAP_EVT_PHY_UPDATE_REQUEST:
        {
            NRF_LOG_DEBUG("PHY update request.");
            ble_gap_phys_t const phys = {BLE_GAP_PHY_AUTO, BLE_GAP_PHY_AUTO};

            err_code = sd_ble_gap_phy_update(p_ble_evt->evt.gap_evt.conn_handle, &phys);
			if (err_code != NRF_SUCCESS) {
				NRF_LOG_ERROR("sd_ble_gap_phy_update(), code 0x%x", err_code);
				APP_ERROR_CHECK(err_code);
			}
        } break;
#endif

        case BLE_GATTC_EVT_TIMEOUT:
            // Disconnect on GATT Client timeout event.
            NRF_LOG_DEBUG("GATT Client Timeout.");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gattc_evt.conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
			if (err_code != NRF_SUCCESS) {
				NRF_LOG_ERROR("sd_ble_gap_disconnect(), code 0x%x", err_code);
				APP_ERROR_CHECK(err_code);
			}
            break;

        case BLE_GATTS_EVT_TIMEOUT:
            // Disconnect on GATT Server timeout event.
            NRF_LOG_DEBUG("GATT Server Timeout.");
            err_code = sd_ble_gap_disconnect(p_ble_evt->evt.gatts_evt.conn_handle, BLE_HCI_REMOTE_USER_TERMINATED_CONNECTION);
			if (err_code != NRF_SUCCESS) {
				NRF_LOG_ERROR("sd_ble_gap_disconnect(), code 0x%x", err_code);
				APP_ERROR_CHECK(err_code);
			}
            break;

        case BLE_EVT_USER_MEM_REQUEST:
            err_code = sd_ble_user_mem_reply(m_conn_handle, NULL);
			if (err_code != NRF_SUCCESS) {
				NRF_LOG_ERROR("sd_ble_user_mem_reply(), code 0x%x", err_code);
				APP_ERROR_CHECK(err_code);
			}
            break;

        case BLE_GATTS_EVT_RW_AUTHORIZE_REQUEST:
        {
            ble_gatts_evt_rw_authorize_request_t  req;
            ble_gatts_rw_authorize_reply_params_t auth_reply;

            req = p_ble_evt->evt.gatts_evt.params.authorize_request;

            if (req.type != BLE_GATTS_AUTHORIZE_TYPE_INVALID)
            {
                if ((req.request.write.op == BLE_GATTS_OP_PREP_WRITE_REQ)     ||
                    (req.request.write.op == BLE_GATTS_OP_EXEC_WRITE_REQ_NOW) ||
                    (req.request.write.op == BLE_GATTS_OP_EXEC_WRITE_REQ_CANCEL))
                {
                    if (req.type == BLE_GATTS_AUTHORIZE_TYPE_WRITE)
                    {
                        auth_reply.type = BLE_GATTS_AUTHORIZE_TYPE_WRITE;
                    }
                    else
                    {
                        auth_reply.type = BLE_GATTS_AUTHORIZE_TYPE_READ;
                    }
                    auth_reply.params.write.gatt_status = APP_FEATURE_NOT_SUPPORTED;
                    err_code = sd_ble_gatts_rw_authorize_reply(p_ble_evt->evt.gatts_evt.conn_handle, &auth_reply);
    				if (err_code != NRF_SUCCESS) {
    					NRF_LOG_ERROR("sd_ble_gatts_rw_authorize_reply(), code 0x%x", err_code);
    					APP_ERROR_CHECK(err_code);
    				}
                }
            }
        } break;


        default:
            // No implementation needed.
            break;
    }
}

static void tx_pwr_exchange(ble_evt_t const * p_ble_evt) {
	uint32_t err_code = NRF_SUCCESS;
	int8_t _tx_pwr = 100;

	if (p_ble_evt->header.evt_id == BLE_GAP_EVT_CONNECTED) {
		uint8_t idx = tx_pwr_index >= TX_POWER_VALUES_NUMBER - 1 ? TX_POWER_VALUES_NUMBER : tx_pwr_index + 2;
		_tx_pwr = tx_pwr_val[idx - 1];
		err_code = sd_ble_gap_tx_power_set(_tx_pwr);
	}
	else if (p_ble_evt->header.evt_id == BLE_GAP_EVT_DISCONNECTED) {
		_tx_pwr = tx_pwr_val[tx_pwr_index - 1];
		err_code = sd_ble_gap_tx_power_set(_tx_pwr);
	}

	if (_tx_pwr != 100) {	// Если !=100, то пробовали менять мощность
		if (err_code == NRF_SUCCESS) {
			NRF_LOG_INFO("tx_power %d", _tx_pwr);
		}
		else {
			NRF_LOG_ERROR("sd_ble_gap_tx_power_set(), code 0x%x", err_code);
			APP_ERROR_CHECK(err_code);
		}
	}
}

/**@brief Function for handling BLE events.
 *
 * @param[in]   p_ble_evt   Bluetooth stack event.
 * @param[in]   p_context   Unused.
 */
static void ble_evt_handler(ble_evt_t const * p_ble_evt, void * p_context)
{
	tx_pwr_exchange(p_ble_evt);

    uint16_t conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
    uint16_t role        = ble_conn_state_role(conn_handle);

    // Based on the role this device plays in the connection, dispatch to the right handler.
    if (role == BLE_GAP_ROLE_PERIPH || ble_evt_is_advertising_timeout(p_ble_evt)) {
        on_ble_peripheral_evt(p_ble_evt);
    }
    else if ((role == BLE_GAP_ROLE_CENTRAL) || (p_ble_evt->header.evt_id == BLE_GAP_EVT_ADV_REPORT)) {
        on_ble_central_evt(p_ble_evt);
    }
}

/**@brief Function for initializing the BLE stack.
 *
 * @details Initializes the SoftDevice and the BLE event interrupt.
 */
static void ble_stack_init(void)
{
	ret_code_t err_code;

	err_code = nrf_sdh_enable_request();
	if (err_code != NRF_SUCCESS) {
		NRF_LOG_ERROR("nrf_sdh_enable_request(), code 0x%x", err_code);
		APP_ERROR_CHECK(err_code);
	}

	// Configure the BLE stack using the default settings.
	// Fetch the start address of the application RAM.
	uint32_t ram_start = 0;
	err_code = nrf_sdh_ble_default_cfg_set(APP_BLE_CONN_CFG_TAG, &ram_start);
	if (err_code != NRF_SUCCESS) {
		NRF_LOG_ERROR("nrf_sdh_ble_default_cfg_set(), code 0x%x", err_code);
		APP_ERROR_CHECK(err_code);
	}

	// Enable BLE stack.
	err_code = nrf_sdh_ble_enable(&ram_start);
	if (err_code != NRF_SUCCESS) {
		NRF_LOG_ERROR("nrf_sdh_ble_enable(), code 0x%x", err_code);
		APP_ERROR_CHECK(err_code);
	}

	// Register a handler for BLE events.
	NRF_SDH_BLE_OBSERVER(m_ble_observer, APP_BLE_OBSERVER_PRIO, ble_evt_handler, NULL);
}

/**@brief Function for the GAP initialization.
 *
 * @details This function sets up all the necessary GAP (Generic Access Profile) parameters of the
 *          device including the device name, appearance, and the preferred connection parameters.
 */
static void gap_params_init(void)
{
    ret_code_t              err_code;
    ble_gap_conn_params_t   gap_conn_params;
    ble_gap_conn_sec_mode_t sec_mode;

    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sec_mode);

    err_code = sd_ble_gap_device_name_set(&sec_mode, (const uint8_t *)device_name, strlen(device_name));
	if (err_code != NRF_SUCCESS) {
		NRF_LOG_ERROR("sd_ble_gap_device_name_set(), code 0x%x", err_code);
		APP_ERROR_CHECK(err_code);
	}

    err_code = sd_ble_gap_appearance_set(BLE_APPEARANCE_UNKNOWN);
	if (err_code != NRF_SUCCESS) {
		NRF_LOG_ERROR("sd_ble_gap_appearance_set(), code 0x%x", err_code);
		APP_ERROR_CHECK(err_code);
	}

    memset(&gap_conn_params, 0, sizeof(gap_conn_params));

    gap_conn_params.min_conn_interval = MSEC_TO_UNITS(config.min_conn_interval, UNIT_1_25_MS);
    gap_conn_params.max_conn_interval = MSEC_TO_UNITS(config.max_conn_interval, UNIT_1_25_MS);
    gap_conn_params.slave_latency     = config.slave_latency;
    gap_conn_params.conn_sup_timeout  = MSEC_TO_UNITS(config.supervision_timeout, UNIT_10_MS);

    err_code = sd_ble_gap_ppcp_set(&gap_conn_params);
	if (err_code != NRF_SUCCESS) {
		NRF_LOG_ERROR("sd_ble_gap_ppcp_set(), code 0x%x", err_code);
		APP_ERROR_CHECK(err_code);
	}
}

void ble_opt_set(void) {
	ble_opt_t opt;
	uint32_t err_code;

	memset(&opt, 0x00, sizeof(ble_opt_t));
	opt.gap_opt.compat_mode_1.enable = 1;
	err_code = sd_ble_opt_set(BLE_GAP_OPT_COMPAT_MODE_1, &opt);
	if (err_code != NRF_SUCCESS) {
		NRF_LOG_ERROR("sd_ble_opt_set() BLE_GAP_OPT_COMPAT_MODE_1, code 0x%x", err_code);
		APP_ERROR_CHECK(err_code);
	}

	memset(&opt, 0x00, sizeof(ble_opt_t));
	opt.common_opt.conn_evt_ext.enable = 1;
	err_code = sd_ble_opt_set(BLE_COMMON_OPT_CONN_EVT_EXT, &opt);
	if (err_code != NRF_SUCCESS) {
		NRF_LOG_ERROR("sd_ble_opt_set() BLE_COMMON_OPT_CONN_EVT_EXT, code 0x%x", err_code);
		APP_ERROR_CHECK(err_code);
	}
}

/**@brief GATT module event handler.
 */
static void gatt_evt_handler(nrf_ble_gatt_t * p_gatt, nrf_ble_gatt_evt_t const * p_evt)
{
	if (p_evt->evt_id == NRF_BLE_GATT_EVT_ATT_MTU_UPDATED) {
		NRF_LOG_INFO("GATT ATT MTU on connection 0x%x changed to %d.\r\n", p_evt->conn_handle, p_evt->params.att_mtu_effective);
	}
	else if (p_evt->evt_id == NRF_BLE_GATT_EVT_DATA_LENGTH_UPDATED) {
		NRF_LOG_INFO("GATT Data Length on connection 0x%x changed to %d.", p_evt->conn_handle, p_evt->params.data_length);
	}

//	ble_hrs_on_gatt_evt(&m_hrs, p_evt);
}

/**@brief Function for initializing the GATT module.
 */
static void gatt_init(void) {
    ret_code_t err_code = nrf_ble_gatt_init(&m_gatt, gatt_evt_handler);
	if (err_code != NRF_SUCCESS) {
		NRF_LOG_ERROR("nrf_ble_gatt_init(), code 0x%x", err_code);
		APP_ERROR_CHECK(err_code);
	}
}

/**@brief Function for handling advertising events.
 *
 * @details This function will be called for advertising events which are passed to the application.
 *
 * @param[in] ble_adv_evt  Advertising event.
 */
static void on_adv_evt(ble_adv_evt_t ble_adv_evt)
{
    switch (ble_adv_evt)
    {
        case BLE_ADV_EVT_FAST:
            NRF_LOG_INFO("Fast advertising.");
            break;

        case BLE_ADV_EVT_IDLE:
            break;

        default:
            break;
    }
}

/**@brief Function for initializing the Advertising functionality.
 */
static void advertising_init(void)
{
    ret_code_t err_code;
    ble_advertising_init_t init;
    ble_adv_modes_config_t adv_modes_config;
//    static ble_uuid_t adv_uuids[] = {{BCS_UUID_SERVICE, m_bcs.uuid_type}};
    static ble_uuid_t adv_uuids[] = {{0x657A, BLE_UUID_TYPE_BLE}};
    static ble_advdata_conn_int_t conn_inetrval = {
		min_conn_interval : (uint16_t)MSEC_TO_UNITS(config.min_conn_interval, UNIT_1_25_MS),
		max_conn_interval : (uint16_t)MSEC_TO_UNITS(config.max_conn_interval, UNIT_1_25_MS)
    };

    //-------------------

    memset(&init, 0, sizeof(init));

    init.advdata.include_appearance      = false;
    init.advdata.flags                   = BLE_GAP_ADV_FLAGS_LE_ONLY_GENERAL_DISC_MODE;
    init.advdata.uuids_complete.uuid_cnt = sizeof(adv_uuids) / sizeof(adv_uuids[0]);
    init.advdata.uuids_complete.p_uuids  = adv_uuids;
    init.advdata.p_tx_power_level        = &tx_pwr_dbm;
    init.advdata.p_slave_conn_int        = &conn_inetrval;
    init.advdata.name_type               = BLE_ADVDATA_FULL_NAME;

    init.config.ble_adv_fast_enabled  = true;
    init.config.ble_adv_fast_interval = MSEC_TO_UNITS(config.adv_interval, UNIT_0_625_MS);;
    init.config.ble_adv_fast_timeout  = APP_ADV_TIMEOUT_IN_SECONDS;

    init.evt_handler = on_adv_evt;

    err_code = ble_advertising_init(&m_advertising, &init);
	if (err_code != NRF_SUCCESS) {
		NRF_LOG_ERROR("ble_advertising_init(), code 0x%x", err_code);
		APP_ERROR_CHECK(err_code);
	}

    //-------------------

    memset(&adv_modes_config, 0, sizeof(adv_modes_config));

    adv_modes_config.ble_adv_on_disconnect_disabled = true;
    adv_modes_config.ble_adv_fast_enabled           = true;
    adv_modes_config.ble_adv_fast_interval          = APP_ADV_INTERVAL;
    adv_modes_config.ble_adv_fast_timeout           = APP_ADV_TIMEOUT_IN_SECONDS;

    ble_advertising_modes_config_set(&m_advertising, &adv_modes_config);

    //-------------------

    ble_advertising_conn_cfg_tag_set(&m_advertising, APP_BLE_CONN_CFG_TAG);
}

static void chars_init(void) {
#define SETVAL(pval, ch_len, service, handles)		do{\
														if ((pval) != NULL) {\
															memcpy(&tmpval, (pval), (ch_len));\
														}\
														gatts_value.len = (ch_len);\
														err_code = sd_ble_gatts_value_set(service.conn_handle, service.handles.value_handle, &gatts_value);\
														if (err_code != NRF_SUCCESS) {\
															NRF_LOG_ERROR("sd_ble_gatts_value_set(), code 0x%x", err_code);\
															APP_ERROR_CHECK(err_code);\
														}\
													}while(0)
	uint32_t err_code;
	union val_u {
		uint32_t u32;
		uint8_t u8[RESPONSE_UUID_CHAR_LEN_CLIENT_APP];	// Максимальная длина
	}tmpval;

	ble_gatts_value_t gatts_value;
	// Initialize value struct.
	memset(&gatts_value, 0, sizeof(gatts_value));
	gatts_value.offset = 0;
	gatts_value.p_value = &tmpval.u8[0];

	memset(&tmpval.u8[0], 0, sizeof(tmpval));
	SETVAL(&tmpval, bleServiceModeRequested ? RESPONSE_UUID_CHAR_LEN_SERVICE_APP : RESPONSE_UUID_CHAR_LEN_CLIENT_APP, m_bcs, response_handles);

	if (bleServiceModeRequested) {
		core_util_critical_section_enter();
		memcpy(&localSett, &settings.data, sizeof(Sett_t));
		core_util_critical_section_exit();

		SETVAL(&localSett.devID, DEVID_UUID_CHAR_LEN, m_uis, devid_handles);

		SETVAL(&localSett.dev_type, DEVTYPE_UUID_CHAR_LEN, m_uis, devtype_handles);

		SETVAL(&localSett.dev_number, DEVNUMBER_UUID_CHAR_LEN, m_uis, devnumber_handles);

		SETVAL(&localSett.net_mode, NET_MODE_UUID_CHAR_LEN, m_ncs, mode_handles);

		if (localSett.net_mode == NET_MODE_DHCP) {
			tmpval.u32 = EzServer.getIPu32();
			SETVAL(&tmpval, IPADDR_UUID_CHAR_LEN, m_ncs, ipaddr_handles);

			tmpval.u32 = EzServer.getMasku32();
			SETVAL(&tmpval, SNMASK_UUID_CHAR_LEN, m_ncs, snmask_handles);

			tmpval.u32 = EzServer.getGatewayu32();
			SETVAL(&tmpval, GATEWAY_UUID_CHAR_LEN, m_ncs, gateway_handles);

			tmpval.u32 = EzServer.getDNS1u32();
			SETVAL(&tmpval, DNS1_UUID_CHAR_LEN, m_ncs, dns1_handles);

			tmpval.u32 = EzServer.getDNS2u32();
			SETVAL(&tmpval, DNS2_UUID_CHAR_LEN, m_ncs, dns2_handles);
		}
		else {
			SETVAL(&localSett.ip, IPADDR_UUID_CHAR_LEN, m_ncs, ipaddr_handles);

			SETVAL(&localSett.mask, SNMASK_UUID_CHAR_LEN, m_ncs, snmask_handles);

			SETVAL(&localSett.gateway, GATEWAY_UUID_CHAR_LEN, m_ncs, gateway_handles);

			SETVAL(&localSett.dns1, DNS1_UUID_CHAR_LEN, m_ncs, dns1_handles);

			SETVAL(&localSett.dns2, DNS2_UUID_CHAR_LEN, m_ncs, dns2_handles);
		}

		SETVAL(&localSett.mac, MAC_UUID_CHAR_LEN, m_ncs, mac_handles);

		SETVAL(&localSett.con_timeout, TIMEOUT_UUID_CHAR_LEN, m_ncs, timeout_handles);

		tmpval.u8[0] = 0;
		SETVAL(&tmpval, FWSTATUS_UUID_CHAR_LEN, m_fus, fwstatus_handles);

		SETVAL(fw_version, FWVER_UUID_CHAR_LEN, m_fus, fwver_handles);

		SETVAL(&localSett.tx_pwr, TXPOWER_UUID_CHAR_LEN, m_sss, txpower_handles);

		SETVAL(&localSett.beac_trh, BEACONTHR_UUID_CHAR_LEN, m_sss, beaconthr_handles);

		SETVAL(&selftest_status, SELFTEST_UUID_CHAR_LEN, m_dos, selftest_handles);

		tmpval.u8[0] = SERVICE_MODE;
		SETVAL(&tmpval, MODE_UUID_CHAR_LEN, m_dos, mode_handles);

		SETVAL(&localSett.logs_mode, LOGGING_UUID_CHAR_LEN, m_dos, logging_handles);

		SETVAL(&temperature, TEMP_UUID_CHAR_LEN, m_pps, temperature_handles);
	}

#undef SETVAL
}

/**@brief Function for initializing services that will be used by the application.
 *
 * @details Initialize the Heart Rate, Battery and Device Information services.
 */
static void services_init(void)
{
	ret_code_t     err_code;
	ble_dis_init_t dis_init;
	ble_bcs_init_t bcs_init;
	ble_uis_init_t uis_init;
	ble_ncs_init_t ncs_init;
	ble_fus_init_t fus_init;
	ble_sss_init_t sss_init;
	ble_dos_init_t dos_init;
	ble_pps_init_t pps_init;

	// Initialize Device Information Service.
	memset(&dis_init, 0, sizeof(dis_init));
	ble_srv_ascii_to_utf8(&dis_init.manufact_name_str, (char *)MANUFACTURER_NAME);
	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&dis_init.dis_attr_md.read_perm);
	BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(&dis_init.dis_attr_md.write_perm);
	err_code = ble_dis_init(&dis_init);
	if (err_code != NRF_SUCCESS) {
		NRF_LOG_ERROR("ble_dis_init(), code 0x%x", err_code);
		APP_ERROR_CHECK(err_code);
	}

	// Initialize barrier control service.
	memset(&bcs_init, 0, sizeof(bcs_init));
	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&bcs_init.bcs_char_attr_md.cccd_write_perm);
	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&bcs_init.bcs_char_attr_md.read_perm);
	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&bcs_init.bcs_char_attr_md.write_perm);
	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&bcs_init.bcs_report_read_perm);
	bcs_init.bcs_write_handler = bcs_write_handler;
	bcs_init.support_notification = true;
	err_code = ble_bcs_init(&m_bcs, &bcs_init, bleServiceModeRequested ? RESPONSE_UUID_CHAR_LEN_SERVICE_APP : RESPONSE_UUID_CHAR_LEN_CLIENT_APP);
	if (err_code != NRF_SUCCESS) {
		NRF_LOG_ERROR("ble_bcs_init(), code 0x%x", err_code);
		APP_ERROR_CHECK(err_code);
	}

	if (bleServiceModeRequested) {
		// Initialize user identification service.
		memset(&uis_init, 0, sizeof(uis_init));
		BLE_GAP_CONN_SEC_MODE_SET_OPEN(&uis_init.uis_char_attr_md.cccd_write_perm);
		BLE_GAP_CONN_SEC_MODE_SET_OPEN(&uis_init.uis_char_attr_md.read_perm);
		BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(&uis_init.uis_char_attr_md.write_perm);
		BLE_GAP_CONN_SEC_MODE_SET_OPEN(&uis_init.uis_report_read_perm);
		uis_init.uis_write_handler = uis_write_handler;
		err_code = ble_uis_init(&m_uis, &uis_init);
		if (err_code != NRF_SUCCESS) {
			NRF_LOG_ERROR("ble_uis_init(), code 0x%x", err_code);
			APP_ERROR_CHECK(err_code);
		}

		// Initialize network service service.
		memset(&ncs_init, 0, sizeof(ncs_init));
		BLE_GAP_CONN_SEC_MODE_SET_OPEN(&ncs_init.ncs_char_attr_md.cccd_write_perm);
		BLE_GAP_CONN_SEC_MODE_SET_OPEN(&ncs_init.ncs_char_attr_md.read_perm);
		BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(&ncs_init.ncs_char_attr_md.write_perm);
		BLE_GAP_CONN_SEC_MODE_SET_OPEN(&ncs_init.ncs_report_read_perm);
		ncs_init.ncs_write_handler = ncs_write_handler;
		err_code = ble_ncs_init(&m_ncs, &ncs_init);
		if (err_code != NRF_SUCCESS) {
			NRF_LOG_ERROR("ble_ncs_init(), code 0x%x", err_code);
			APP_ERROR_CHECK(err_code);
		}

		// Initialize Firmware Update Service.
		memset(&fus_init, 0, sizeof(fus_init));
		BLE_GAP_CONN_SEC_MODE_SET_OPEN(&fus_init.fus_char_attr_md.cccd_write_perm);
		BLE_GAP_CONN_SEC_MODE_SET_OPEN(&fus_init.fus_char_attr_md.read_perm);
		BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(&fus_init.fus_char_attr_md.write_perm);
		BLE_GAP_CONN_SEC_MODE_SET_OPEN(&fus_init.fus_report_read_perm);
		fus_init.fus_write_handler = fus_write_handler;
		fus_init.support_notification = true;
		err_code = ble_fus_init(&m_fus, &fus_init);
		if (err_code != NRF_SUCCESS) {
			NRF_LOG_ERROR("ble_fus_init(), code 0x%x", err_code);
			APP_ERROR_CHECK(err_code);
		}

		// Initialize signal strength service.
		memset(&sss_init, 0, sizeof(sss_init));
		BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sss_init.sss_char_attr_md.cccd_write_perm);
		BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sss_init.sss_char_attr_md.read_perm);
		BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(&sss_init.sss_char_attr_md.write_perm);
		BLE_GAP_CONN_SEC_MODE_SET_OPEN(&sss_init.sss_report_read_perm);
		sss_init.sss_write_handler = sss_write_handler;
		err_code = ble_sss_init(&m_sss, &sss_init);
		if (err_code != NRF_SUCCESS) {
			NRF_LOG_ERROR("ble_sss_init(), code 0x%x", err_code);
			APP_ERROR_CHECK(err_code);
		}

		// Initialize Device Operation Service.
		memset(&dos_init, 0, sizeof(dos_init));
		BLE_GAP_CONN_SEC_MODE_SET_OPEN(&dos_init.dos_char_attr_md.cccd_write_perm);
		BLE_GAP_CONN_SEC_MODE_SET_OPEN(&dos_init.dos_char_attr_md.read_perm);
		BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(&dos_init.dos_char_attr_md.write_perm);
		BLE_GAP_CONN_SEC_MODE_SET_OPEN(&dos_init.dos_report_read_perm);
		dos_init.dos_write_handler = dos_write_handler;
		dos_init.support_notification = true;
		err_code = ble_dos_init(&m_dos, &dos_init);
		if (err_code != NRF_SUCCESS) {
			NRF_LOG_ERROR("ble_dos_init(), code 0x%x", err_code);
			APP_ERROR_CHECK(err_code);
		}

		// Initialize physical parameters service.
		memset(&pps_init, 0, sizeof(pps_init));
		BLE_GAP_CONN_SEC_MODE_SET_OPEN(&pps_init.pps_char_attr_md.cccd_write_perm);
		BLE_GAP_CONN_SEC_MODE_SET_OPEN(&pps_init.pps_char_attr_md.read_perm);
		BLE_GAP_CONN_SEC_MODE_SET_NO_ACCESS(&pps_init.pps_char_attr_md.write_perm);
		BLE_GAP_CONN_SEC_MODE_SET_OPEN(&pps_init.pps_report_read_perm);
		pps_init.pps_write_handler = pps_write_handler;
		pps_init.support_notification = true;
		err_code = ble_pps_init(&m_pps, &pps_init);
		if (err_code != NRF_SUCCESS) {
			NRF_LOG_ERROR("ble_pps_init(), code 0x%x", err_code);
			APP_ERROR_CHECK(err_code);
		}
	}

	chars_init();
}

/**@brief Function for handling the Connection Parameters Module.
 *
 * @details This function will be called for all events in the Connection Parameters Module which
 *          are passed to the application.
 *          @note All this function does is to disconnect. This could have been done by simply
 *                setting the disconnect_on_fail config parameter, but instead we use the event
 *                handler mechanism to demonstrate its use.
 *
 * @param[in] p_evt  Event received from the Connection Parameters Module.
 */
static void on_conn_params_evt(ble_conn_params_evt_t * p_evt)
{
	ret_code_t err_code;

	switch (p_evt->evt_type) {
	case BLE_CONN_PARAMS_EVT_FAILED:
		NRF_LOG_INFO("BLE_CONN_PARAMS_EVT_FAILED");
		err_code = sd_ble_gap_disconnect(m_conn_handle, BLE_HCI_CONN_INTERVAL_UNACCEPTABLE);
		if (err_code != NRF_SUCCESS) {
			NRF_LOG_ERROR("sd_ble_gap_disconnect(), code 0x%x", err_code);
			APP_ERROR_CHECK(err_code);
		}
	break;

	case BLE_CONN_PARAMS_EVT_SUCCEEDED:
		NRF_LOG_INFO("BLE_CONN_PARAMS_EVT_SUCCEEDED");
	break;
	}
}

/**@brief Function for handling a Connection Parameters error.
 *
 * @param[in] nrf_error  Error code containing information about what went wrong.
 */
static void conn_params_error_handler(uint32_t nrf_error)
{
	NRF_LOG_ERROR("conn_params_error_handler(), code 0x%x", nrf_error);
	APP_ERROR_HANDLER(nrf_error);
}

/**@brief Function for initializing the Connection Parameters module.
 */
static void conn_params_init(void)
{
	ret_code_t             err_code;
	ble_conn_params_init_t cp_init;

	ble_gap_conn_params_t  conn_params;
	memset(&conn_params, 0, sizeof(conn_params));
	conn_params.min_conn_interval = MSEC_TO_UNITS(config.min_conn_interval, UNIT_1_25_MS);
	conn_params.max_conn_interval = MSEC_TO_UNITS(config.max_conn_interval, UNIT_1_25_MS);
	conn_params.conn_sup_timeout = MSEC_TO_UNITS(config.supervision_timeout, UNIT_10_MS);
	conn_params.slave_latency = config.slave_latency;

	memset(&cp_init, 0, sizeof(cp_init));
	cp_init.p_conn_params                  = &conn_params;
	cp_init.first_conn_params_update_delay = FIRST_CONN_PARAMS_UPDATE_DELAY;
	cp_init.next_conn_params_update_delay  = NEXT_CONN_PARAMS_UPDATE_DELAY;
	cp_init.max_conn_params_update_count   = MAX_CONN_PARAMS_UPDATE_COUNT;
//	cp_init.start_on_notify_cccd_handle    = m_bcs.response_handles.cccd_handle;
	cp_init.disconnect_on_fail             = false;
	cp_init.evt_handler                    = on_conn_params_evt;
	cp_init.error_handler                  = conn_params_error_handler;

	err_code = ble_conn_params_init(&cp_init);
	if (err_code != NRF_SUCCESS) {
		NRF_LOG_ERROR("ble_conn_params_init(), code 0x%x", err_code);
		APP_ERROR_CHECK(err_code);
	}
}

/**@brief Function for starting advertising.
 */
static void advertising_start()
{
	if (config.advertise == true) {
        ret_code_t err_code;

        err_code = ble_advertising_start(&m_advertising, BLE_ADV_MODE_FAST);
		if (err_code == NRF_SUCCESS) {
			BleAdvertisingOn = true;
		}
		else {
			NRF_LOG_ERROR("sd_ble_gap_adv_start(), code 0x%x", err_code);
			APP_ERROR_CHECK(err_code);
		}

	}
}

static void advertising_stop(void)
{
	if (BleAdvertisingOn) {
		uint32_t err_code = sd_ble_gap_adv_stop();
		if (err_code == NRF_SUCCESS) {
			BleAdvertisingOn = false;
		}
		else {
			NRF_LOG_ERROR("sd_ble_gap_adv_stop(), code 0x%x", err_code);
			APP_ERROR_CHECK(err_code);
		}

	}
}

/**@brief Function for initializing the nrf log module.
 */
static void nrf_log_init(void)
{
    ret_code_t err_code = NRF_LOG_INIT(NULL);
	if (err_code != NRF_SUCCESS) {
		NRF_LOG_ERROR("NRF_LOG_INIT(), code 0x%x", err_code);
		APP_ERROR_CHECK(err_code);
	}

    NRF_LOG_DEFAULT_BACKENDS_INIT();
}

static void BLE_Init(void) {
	uint32_t err_code = nrf_sdh_disable_request();
	(void)err_code;

	ser_app_hal_nrf_reset_pin_clear();

	ble_stack_init();
	gap_params_init();
	ble_opt_set();
	gatt_init();
	services_init();
	advertising_init();
	conn_params_init();
	db_discovery_init();
//	peer_manager_init();

	// Перед началом адвертайзинга применяем настройки
	init_from_settings();

//	scan_start();
//	advertising_start();
}

static void BLE_Off(void) {
	advertising_stop();
	scan_stop();

	Thread::wait(200);

	uint32_t err_code = nrf_sdh_disable_request();
	if (err_code != NRF_SUCCESS) {
		NRF_LOG_ERROR("nrf_sdh_disable_request(), code 0x%x", err_code);
		APP_ERROR_CHECK(err_code);
	}

	ser_app_hal_nrf_reset_pin_clear();

	BleConnected = false;
	BleAdvertisingOn = false;
	isServiceModeOn = false;
	m_conn_handle = BLE_CONN_HANDLE_INVALID;
}

/// Необходима, чтобы сбрасывать статические переменные в начальные значения, если задача была перезапущена другой задачей
static void init_static_vars() {
	nrf_sdh_disable_request();
	ser_app_hal_nrf_reset_pin_clear();

	BleConnected = false;
	BleAdvertisingOn = false;
	isServiceModeOn = false;
	m_conn_handle = BLE_CONN_HANDLE_INVALID;
	bleNormalModeRequested = false;
	gateState = GATE_BEGIN;
	openedByEzeparkUser = false;
	openedByEzeparkBeacon = false;
	authorised = false;
}

static void gate_open_process(void) {
	RequestDevType_t request_type = REQUEST_NONE;

	osEvent evt = uuidMailbox.get(0);
	if (evt.status == osEventMail) {
		UUID_useid_t *mail = (UUID_useid_t*)evt.value.p;
		memcpy(smph_uuid, mail->uuid, 16);
		user_id = mail->userid;
		uuidMailbox.free(mail);

		request_type = PHONE_REUEST;
	}
	else {
		evt = BeaconMailbox.get(0);
		if (evt.status == osEventMail) {
			BeaconMAC_t *mail = (BeaconMAC_t*)evt.value.p;
			memcpy(beacon_mac, mail->beacon_mac, 6);
			BeaconMailbox.free(mail);

			request_type = BEACON_REQUEST;
		}
	}

	if (request_type != REQUEST_NONE) {
		char buff[128];
		strcpy(buff, "[BLE:RSP]:");

		if (db.isUpdated()) {
			DataBase::BillingStatus_t billing = DataBase::GATE_BUSY;

			if (!gate.isOpen()) {
				wdg.kick(TASK_ALL);

				if (dev_type == DEVICE_TYPE_IN) {
					DataBase::BillingStatus_t sessionStatus = DataBase::NO_SESSION;

					// Используем мьютекс только при обращении к базе,
					// а locDB.writeEvent() делаем без мьютекса, потому что туда пишутся ивенты, которые не затираются при обновлении базы
					BLE_Mutex.lock();
					// одно открытие шлагбаума на одну сессию
					if (request_type == PHONE_REUEST) {
						billing = db.checkBilling(smph_uuid, user_id, &sessionStatus);
					}
					else {
						billing = db.checkBillingBeacon(beacon_mac, &sessionStatus);
					}
					BLE_Mutex.unlock();

					wdg.kick(TASK_ALL);

					switch (billing) {
					case DataBase::BALANCE_OK:
					case DataBase::CORPORATE_USER:
					case DataBase::RESERVED_USER:
						if (gate.open()) {
							strcat(buff, "CMD_RSP_SUCCESS");
							response = CMD_RSP_SUCCESS;
							evtm = mtime.get();
							if (request_type == PHONE_REUEST) {
								openedByEzeparkUser = true;
							}
							else {
								openedByEzeparkBeacon = true;
							}

							if (sessionStatus == DataBase::NO_SESSION) {
								if (db.allowReservedSpaces()) {
									if (billing == DataBase::CORPORATE_USER) {
										db.decrementOfCorporate();
									}
									else if (billing == DataBase::RESERVED_USER) {
										db.decrementOfReserved();
									}
									else {
										db.decrementOfGeneral();
									}
								}
								else {
									db.decrementOfGeneral();
								}
							}
						}
						else {
							response = CMD_RSP_TECH_ISSUE;
							strcat(buff, "CMD_RSP_TECH_ISSUE");
						}
						break;
					case DataBase::NO_BALANCE:
						response = CMD_RSP_NO_BALANCE_ENTER;
						strcat(buff, "CMD_RSP_NO_BALANCE_ENTER");
						break;
					case DataBase::NO_USER:
						response = CMD_RSP_NO_USER;
						strcat(buff, "CMD_RSP_NO_USER");
						break;
					case DataBase::GATE_BUSY:
						response = CMD_RSP_GATE_BUSY;
						strcat(buff, "CMD_RSP_GATE_BUSY");
						break;
					case DataBase::NO_PLACES:
						response = CMD_RSP_NO_PLACES;
						strcat(buff, "CMD_RSP_NO_PLACES");
						break;
					case DataBase::NO_UUID:
						response = CMD_RSP_NO_UUID;
						strcat(buff, "CMD_RSP_NO_UUID");
						break;
					case DataBase::SESSION_IS_OPENED:
						response = CMD_SESSION_IS_OPENED;
						strcat(buff, "CMD_SESSION_IS_OPENED");
						break;
					default:
						// Пусть будет так...
						response = CMD_RSP_NO_USER;
						strcat(buff, "CMD_RSP_NO_USER");
						break;
					}
				}
				else if (dev_type == DEVICE_TYPE_OUT) {
					BLE_Mutex.lock();
					if (request_type == PHONE_REUEST) {
						billing = db.checkBillingExit(smph_uuid, user_id);
					}
					else {
						billing = db.checkBillingExitBeacon(beacon_mac);
					}
					BLE_Mutex.unlock();

					wdg.kick(TASK_ALL);

					switch (billing) {
					case DataBase::BALANCE_OK:
					case DataBase::CORPORATE_USER:
					case DataBase::RESERVED_USER:
						if (gate.open()) {
							strcat(buff, "CMD_RSP_SUCCESS");
							response = CMD_RSP_SUCCESS;
							evtm = mtime.get();
							if (request_type == PHONE_REUEST) {
								openedByEzeparkUser = true;
							}
							else {
								openedByEzeparkBeacon = true;
							}
						}
						else {
							response = CMD_RSP_TECH_ISSUE;
							strcat(buff, "CMD_RSP_TECH_ISSUE");
						}
						break;
					case DataBase::NO_BALANCE:
						response = CMD_RSP_NO_BALANCE_EXIT;
						strcat(buff, "CMD_RSP_NO_BALANCE_EXIT");
						break;
					case DataBase::GATE_BUSY:
						response = CMD_RSP_GATE_BUSY;
						strcat(buff, "CMD_RSP_GATE_BUSY");
						break;
					case DataBase::NO_SESSION:
						response = CMD_RSP_NO_SESSION;
						strcat(buff, "CMD_RSP_NO_SESSION");
						break;
					case DataBase::NO_USER:
						response = CMD_RSP_NO_USER;
						LOG("[BLE:RSP]:CMD_RSP_NO_USER");
						break;
					default:
						// Пусть будет так...
						response = CMD_RSP_NO_USER;
						strcat(buff, "CMD_RSP_NO_USER");
						break;
					}
				}
				else {
					strcat(buff, "CMD_RSP_GATE_OFF");
					response = CMD_RSP_GATE_OFF;
				}
			}
			else {
				response = CMD_RSP_GATE_BUSY;
				strcat(buff, "CMD_RSP_GATE_BUSY");
			}

			if (response == CMD_RSP_SUCCESS) {
				BLE_Mutex.lock();
				if (request_type == PHONE_REUEST) {
					db.writeEvent(smph_uuid, evtm, false);
					PRINTF("Event: %02x%02x%02x%02x-%02x%02x-..., %u\n", smph_uuid[0], smph_uuid[1], smph_uuid[2], smph_uuid[3], smph_uuid[4], smph_uuid[5], (unsigned int)evtm);
				}
				else {
					db.writeEvent(beacon_mac, evtm, true);
					PRINTF("Event: %02x:%02x:%02x:%02x:%02x:%02x, %u\n", beacon_mac[0], beacon_mac[1], beacon_mac[2], beacon_mac[3], beacon_mac[4], beacon_mac[5], (unsigned int)evtm);
				}
				BLE_Mutex.unlock();
			}
		}
		else {
			response = CMD_RSP_NOT_UPDATED;
			strcat(buff, "CMD_RSP_NOT_UPDATED");
		}

		if (request_type == PHONE_REUEST) {
			ble_cmd_write_response(response, evtm);
			Thread::wait(1000);
			ble_disconnect();

			strcat(buff, ", UUID: ");
			size_t len = strlen(buff);
			for (uint8_t q = 0; q < 16; q++) {
				sprintf(buff + len, "%02x", smph_uuid[q]);
				len += 2;
			}
		}
		else {
			strcat(buff, ", MAC: ");
			size_t len = strlen(buff);
			for (uint8_t q = 0; q < 6; q++) {
				sprintf(buff + len, "%02x", beacon_mac[q]);
				len += 2;
			}
		}

		LOG(buff);
	}
}

uint16_t dist = 0;

extern Mail<uint16_t, 1>mail_dist;

static void getDist() {
	osEvent evt = mail_dist.get(0);
	if (evt.status == osEventMail) {
		uint16_t *pDist = (uint16_t*)evt.value.p;
		if (pDist) {
			dist = *pDist;
		}
		mail_dist.free(pDist);
	}
}

static bool isHaveCar() {
	if (settings.data.enable_dist_sensor) {
		return (dist <= settings.data.dist_sensor_threshold);
	}
	else {
		return true;
	}
}

static void gate_state_process(void) {
	switch(gateState) {
	case GATE_BEGIN: {
		if (gate.isOpen()) {
			openGateTimer.start();
			gateState = GATE_IS_OPEN;
			PRINTF("GATE_BEGIN -> GATE_IS_OPEN\n");
		}
		else {
			gateState = GATE_NORMAL;
			PRINTF("GATE_BEGIN -> GATE_NORMAL\n");
		}
		break;
	}

	case GATE_OPENED: {
		if (!openedByEzeparkUser && !openedByEzeparkBeacon && !isServiceModeOn && dev_type != DEVICE_TYPE_OFF) {
			evtm = mtime.get();
			PRINTF("Event: \"\", %u\n", (unsigned int)evtm);
			db.writeEvent(NULL, evtm, false);

			if (dev_type == DEVICE_TYPE_IN) {
				db.decrementOfGeneral();
			}
		}

		if (!isServiceModeOn) {	// Если через шлагбаум проехали, то переводим BLE в сброс
			while(BleConnected) {
				Thread::wait(10);
			}
			BLE_Off();
		}

		openGateTimer.start();
		gateState = GATE_IS_OPEN;
		openedByEzeparkUser = false;
		openedByEzeparkBeacon = false;
		LOG("|-- Gate Opened");
		PRINTF("GATE_OPENED -> GATE_IS_OPEN\n");
		break;
	}

	case GATE_IS_OPEN: {
		static bool tryOpen = true;

		if (openGateTimer.read_ms() >= TIMEOUT_REANIMATE_GATE && tryOpen) {
			gate.openWithoutFeedback();
			tryOpen = false;
		}

		if (!gate.isOpen()) {
			gateState = GATE_NORMAL;
			PRINTF("GATE_IS_OPEN -> GATE_NORMAL\n");
		}
		else if (openGateTimer.read_ms() > TIMEOUT_OF_OPEN) {
			gateState = GATE_ISSUE;
			PRINTF("GATE_IS_OPEN -> GATE_ISSUE\n");
		}

		if (gateState != GATE_IS_OPEN) {
			openGateTimer.stop();
			openGateTimer.reset();
			tryOpen = true;

			if (!isServiceModeOn) {	// Если открыто из сервисного приложения, то не инициаллизируем BLE снова, потому что он и не выключался
				BLE_Init();
			}
		}

		break;
	}

	case GATE_NORMAL: {
		if (gate.isOpen()) {
			gateState = GATE_OPENED;
			PRINTF("GATE_NORMAL -> GATE_OPENED\n");
		}

		if (!bleServiceModeRequested && !bleNormalModeRequested && m_conn_handle == BLE_CONN_HANDLE_INVALID) {
			if(isHaveCar() && !BleAdvertisingOn) {
				advertising_start();
				scan_start();
			}
			else if (!isHaveCar() && BleAdvertisingOn) {
				advertising_stop();
				scan_stop();
			}
		}

		break;
	}

	case GATE_ISSUE: {	// Тут висит, если шлагбаум не закрылся в течение TIMEOUT_OF_OPEN
		if (!gate.isOpen()) {
			gateState = GATE_NORMAL;
			PRINTF("GATE_ISSUE -> GATE_NORMAL\n");
		}

		if (!BleAdvertisingOn && m_conn_handle == BLE_CONN_HANDLE_INVALID) {
			// Адвертайзинг на тот случай, если была попытка подключения с респонсом GATE_BUSY
			advertising_start();
			scan_start();
		}
		break;
	}
	}
}

static void settings_process(void) {
	if (settings.changed()) {
		core_util_critical_section_enter();
		dev_type = settings.data.dev_type;
		tx_pwr_index = settings.data.tx_pwr;
		dev_number = settings.data.dev_number;
		dev_id = settings.data.devID;
		beac_trh = settings.data.beac_trh;
		core_util_critical_section_exit();

		tx_pwr_dbm = tx_pwr_val[tx_pwr_index - 1];
		uint32_t err_code = sd_ble_gap_tx_power_set(tx_pwr_dbm);
		if (err_code != NRF_SUCCESS) {
			NRF_LOG_ERROR("sd_ble_gap_tx_power_set(), code 0x%x", err_code);
			APP_ERROR_CHECK(err_code);
		}

		setName();

		settings.set_changed_flag(false);
	}
}

static void service_cmd_process(void) {
	osEvent evt = CmdMailbox.get(0);
	if (evt.status == osEventMail) {
		uint8_t *mail = (uint8_t*)evt.value.p;
		uint8_t cmd = *mail;
		CmdMailbox.free(mail);

		switch(cmd) {
		case CMD_OPEN:
			LOG("[BLE:CMD]:CMD_OPEN");
			// Команда открытия из сервисного
			selftest_status &= ~GATE_FEEDBACK_OK;

			if (!gate.isOpen()) {
				if (gate.open()) {
					response = CMD_RSP_SUCCESS;
					selftest_status |= GATE_FEEDBACK_OK;
				}
				else {
					response = CMD_RSP_TECH_ISSUE;
				}
			}
			else {
				response = CMD_RSP_GATE_BUSY;
			}

			ble_cmd_write_response(response);
			break;

		case CMD_CLOSE:
			LOG("[BLE:CMD]:CMD_CLOSE");
			response = CMD_RSP_NO_PRIVILEGES;
			ble_cmd_write_response(response);
			break;

		case CMD_DISCONNECT:
			LOG("[BLE:CMD]:CMD_DISCONNECT");
			response = CMD_RSP_SUCCESS;
			ble_cmd_write_response(response);
			ble_disconnect();
			break;

		case CMD_REBOOT:
			LOG("[BLE:CMD]:CMD_REBOOT");
			response = CMD_RSP_SUCCESS;
			ble_cmd_write_response(response);
			rebootMCU();
			break;

		case CMD_SERVICE_MODE_ENTER:
			LOG("[BLE:CMD]:CMD_SERVICE_MODE_ENTER");
			response = CMD_RSP_NO_PRIVILEGES;
			ble_cmd_write_response(response);
			break;

		case CMD_SERVICE_MODE_EXIT:
			LOG("[BLE:CMD]:CMD_SERVICE_MODE_EXIT");
			response = CMD_RSP_NO_PRIVILEGES;
			ble_cmd_write_response(response);
			break;

		case CMD_SHUTDOWN:
			LOG("[BLE:CMD]:CMD_SHUTDOWN");
			osSignalSet(mainThreadID, SHUTDOWN_MODE_ON);
			response = CMD_RSP_SUCCESS;
			ble_cmd_write_response(response);
			break;

		case CMD_SD_CLEANUP:
			LOG("[BLE:CMD]:CMD_SD_CLEANUP");
			p_net_thread->signal_set(SD_CLEANUP);
			Thread::signal_wait(NET_READY_FOR_SD_CLEANUP);

			int ret;
			ret = sdcard.formatDisk();
			if (ret == 0) {
				ret = sdcard.init();
			}
			p_net_thread->signal_set(SD_CLEANUP_END);
			if (ret == 0) {
				db.cardFormatted();
				response = CMD_RSP_SUCCESS;
				LOG("[SD] was formated");
			}
			else {
				response = CMD_RSP_SDC_CLEANUP_ERROR;
				LOG("[SD] format error");
			}

			ble_cmd_write_response(response);

			// Сбрасываем флаг апдейта базы независимо от того, успех или нет
			db.resetUpdate();
			break;

		case CMD_SET_DEFAULT:
			LOG("[BLE:CMD]:CMD_SET_DEFAULT");
			response = CMD_RSP_SUCCESS;
			ble_cmd_write_response(response);

			settings.resetSettings();
			ble_disconnect();
			rebootMCU();
			break;

		case CMD_SAVE_SETTINGS:
			LOG("[BLE:CMD]:CMD_SAVE_SETTINGS");
			core_util_critical_section_enter();
			memcpy(&settings.data, &localSett, sizeof(Sett_t));
			core_util_critical_section_exit();

			if (settings.save()) {
				response = CMD_RSP_SETTINGS_SAVE_OK;
				// Настройки сети здесь не применяем потому что для них требуется перезагрузка, где они применятся
				tx_pwr_index = localSett.tx_pwr;
				tx_pwr_dbm = tx_pwr_val[tx_pwr_index - 1];
				beac_trh = localSett.beac_trh;
				dev_number = localSett.dev_number;
				dev_id = localSett.devID;
				sdcard.printSettingsLog();
			}
			else {
				response = CMD_RSP_SETTINGS_SAVE_FAIL;
				settings.load();
			}

			ble_cmd_write_response(response);
			break;

		case CMD_INTERNAL_DFU_OK: {
			uint32_t err_code = ble_fus_status_update(&m_fus, DFU_OK);
			if (err_code != NRF_SUCCESS) {
				NRF_LOG_ERROR("ble_fus_status_update(), code 0x%x", err_code);
				APP_ERROR_CHECK(err_code);
			}

			LOG("[BLE:INFO]:Firmware download completed, reboot...");
			rebootMCU();
			} break;

		default:
			LOG("[BLE:CMD]:CMD_UNKNOWN");
			response = CMD_RSP_NO_PRIVILEGES;
			ble_cmd_write_response(response);
			break;
		}
	}
}

static void selfTest() {
	static uint8_t local_selftest_status = 0;

	if (isServiceModeOn && authorised && dos_selftest_notification_subscribed) {
		//сбрасываю флаги проверки всего, что связано с сетью, остальные не надо
		selftest_status &= ~(PHY_OK | CABLE_CONNECTED | NET_OK | SERVER_CONNECTED | SERVER_DATA_OK | CARD_DETECTED | CARD_HEALTH_OK);

		if (cardDetected()) {
			selftest_status |= CARD_DETECTED;
			if (sdcard.cardError() < 1){
				selftest_status |= CARD_HEALTH_OK;
			}
			else {
				selftest_status &= ~CARD_HEALTH_OK;
			}
		}

		// Иерархично проверяю статусы сети
		if  (EzServer.checkPHY()) {
			selftest_status |= PHY_OK;
			if (EzServer.isCableConnected()) {
				selftest_status |= CABLE_CONNECTED;
				if (EzServer.isLink()) {
					selftest_status |= NET_OK;
					if  (EzServer.connectedToServer()) {
						selftest_status |= SERVER_CONNECTED;
						selftest_status |= SERVER_DATA_OK;
					}
				}
			}
		}

		if (m_conn_handle != BLE_CONN_HANDLE_INVALID) {
			if (local_selftest_status != selftest_status) {
				uint32_t err_code = ble_selftest_result_update(&m_dos, selftest_status);
				if (err_code != NRF_SUCCESS) {
					NRF_LOG_ERROR("ble_selftest_result_update(), code 0x%x", err_code);
					APP_ERROR_CHECK(err_code);
				}
				else {
					local_selftest_status = selftest_status;
				}
			}
		}
	}
	else {
		local_selftest_status = 0;
	}
}

static void service_mode_process(void) {
	selfTest();
	service_cmd_process();

	if ((bleServiceModeRequested || bleNormalModeRequested) && !BleConnected) {
		uint32_t err_code = nrf_sdh_disable_request();
		if (err_code != NRF_SUCCESS) {
			NRF_LOG_ERROR("nrf_sdh_disable_request(), code 0x%x", err_code);
			APP_ERROR_CHECK(err_code);
		}

		Thread::wait(100);
		BLE_Init();
		advertising_start();
		scan_start();

		if (bleServiceModeRequested) {
			p_net_thread->signal_set(SERVICE_MODE_ON);
			osSignalSet(mainThreadID, SERVICE_MODE_ON);

			bleServiceModeRequested = false;
			isServiceModeOn = true;
			fus_packet_cnt = 0;
			timeSinceServiceModeEnter = mtime.get();

			LOG("[BLE:Info] Service Mode started");
		}

		if (bleNormalModeRequested) {
			p_net_thread->signal_set(SERVICE_MODE_OFF);
			osSignalSet(mainThreadID, SERVICE_MODE_OFF);

			bleNormalModeRequested = false;
			isServiceModeOn = false;

			settings.set_changed_flag(true);

			LOG("[BLE:Info] Normal Mode started");
		}
	}

	if (isServiceModeOn) {
		if (m_conn_handle == BLE_CONN_HANDLE_INVALID) {	// Если нет коннекта
			if (mtime.get() - timeSinceServiceModeEnter > 20) {
				bleNormalModeRequested = true;		// Переходим в нормальный режим по таймауту
			}
		}
	}
}

static void init_from_settings(void) {
	settings.set_changed_flag(true);
	settings_process();
}

static void TempTimerClbk (void const *n) {
	osEvent evt = TemperatureMailbox.get(0);
	if (evt.status == osEventMail) {
		int8_t *mail = (int8_t*)evt.value.p;
		temperature = *mail;
		TemperatureMailbox.free(mail);
	}
}

static RtosTimer TempTimer(TempTimerClbk, osTimerPeriodic);

// ==========================================================

void ConParam_thread(void const *args);

void BLE_thread(void const *args) {
	init_static_vars();
	nrf_log_init();
	BLE_Init();
	NRF_LOG_INFO("Normal Mode started");

	TempTimer.start(1000);

//	Thread con_param_thread(ConParam_thread, NULL, osPriorityBelowNormal, 0x200);

	while (1) {
		getDist();

		if (m_conn_handle != BLE_CONN_HANDLE_INVALID) {	// Если активен коннект
			time_t timeout = !isServiceModeOn ? config.auth_timeout : 15;
			if ( !authorised && (mtime.get() - timeSinceNotConected > timeout) ) {	// Таймаут ожидания авторизации
				NRF_LOG_INFO("Timeout. Disconnect...");
				ble_disconnect();
				while (m_conn_handle != BLE_CONN_HANDLE_INVALID) {
					Thread::wait(10);
				}
			}
		}
		else {
			timeSinceNotConected = mtime.get();
		}

		gate_state_process();
		gate_open_process();
		settings_process();
		service_mode_process();

		NRF_LOG_PROCESS();

		Thread::wait(10);
		wdg.kick(TASK_BLE);
	}
}

// ==========================================================

typedef struct {
	uint8_t cmd;
	uint32_t time_ms;
}AppTimerMail_t;

Mail<AppTimerMail_t, 4> AppTimerMailbox;

enum {
	APP_TIMER_CMD_NONE,
	APP_TIMER_CMD_START,
	APP_TIMER_CMD_STOP,
};

app_timer_timeout_handler_t timeout_handler = NULL;

extern "C" {
void app_rtos_tmr_create(app_timer_timeout_handler_t p_timeout_handler) {
	timeout_handler = p_timeout_handler;
}

uint32_t app_rtos_tmr_start(uint32_t timeout_ticks) {
	AppTimerMail_t *mail = AppTimerMailbox.alloc();
	if (mail) {
		mail->cmd = APP_TIMER_CMD_START;
		mail->time_ms = timeout_ticks;
		AppTimerMailbox.put(mail);
		return NRF_SUCCESS;
	}
	else {
		return NRF_ERROR_INTERNAL;
	}
}

uint32_t app_rtos_tmr_stop(void) {
	AppTimerMail_t *mail = AppTimerMailbox.alloc();
	if (mail) {
		mail->cmd = APP_TIMER_CMD_STOP;
		AppTimerMailbox.put(mail);
		return NRF_SUCCESS;
	}
	else {
		return NRF_ERROR_INTERNAL;
	}
}
}

void ConParam_thread(void const *args) {
	uint8_t app_tmr_cmd;
	Timer app_tmr;
	uint32_t timeuot = 0;
	bool tmr_running = false;

	while(1) {
		osEvent evt = AppTimerMailbox.get(0);
		if (evt.status == osEventMail) {
			AppTimerMail_t *mail = (AppTimerMail_t*)evt.value.p;
			app_tmr_cmd = mail->cmd;
			timeuot = mail->time_ms;
			AppTimerMailbox.free(mail);

			switch (app_tmr_cmd) {
			case APP_TIMER_CMD_START:
				app_tmr.reset();
				app_tmr.start();
				tmr_running = true;
				break;

			case APP_TIMER_CMD_STOP:
				app_tmr.stop();
				tmr_running = false;
				break;
			}
		}

		if (tmr_running) {
			if ((uint32_t)app_tmr.read_ms() > timeuot) {
				app_tmr.stop();
				tmr_running = false;
				timeout_handler(NULL);
			}
		}

		Thread::wait(70);


//		static bool upd_success = false;
//
//		if (m_conn_handle != BLE_CONN_HANDLE_INVALID && !upd_success) {
//			Thread::wait(1000);
//			if (m_conn_handle != BLE_CONN_HANDLE_INVALID) {
//				uint32_t err_code = ble_conn_params_change_conn_params(&conn_params);
//				if (err_code != NRF_SUCCESS) {
//					NRF_LOG_ERROR("ble_conn_params_change_conn_params(), code 0x%x", err_code);
//					APP_ERROR_CHECK(err_code);
//				}
//				else {
//					upd_success = true;
//				}
//			}
//		}
//		else {
//			upd_success = false;
//		}
	}
}

