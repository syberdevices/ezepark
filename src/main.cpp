﻿/*
 * main.cpp
 *
 *  Created on: 16 θών 2016 γ.
 *      Author: Chuyec
 */

#include <mbed.h>
#include <rtos.h>
#include <SDFileSystem/SDFileSystem.h>
#include <Settings/Settings.h>
#include <DataBase/DataBase.h>
#include <EzeparkServer/EzeparkServer.h>
#include <MyTime/MyTime.h>
#include <Watchdog/Watchdog.h>

#include <common.h>
#include <SdCard/SdCard.h>
#include <trace/trace.h>

#include <string>

#include <ultrasonic/ultrasonic.h>

//===================================================================

//uint8_t *p_DFU_Data = (uint8_t*)(0x60000000 + 512*1024);
uint8_t *p_DFU_Data = (uint8_t*)(0x60000000);

#define SPI_MOSI_PIN		PC_12
#define SPI_MISO_PIN		PC_11
#define SPI_SCK_PIN			PC_10
#define SPI_CS_PIN			PA_15

//SDFileSystem sd(SPI_MOSI_PIN, SPI_MISO_PIN, SPI_SCK_PIN, SPI_CS_PIN, "sd");
DigitalIn CardDetect(PD_2, PullUp);

bool cardDetected() {
	return CardDetect == 0;
}

Watchdog wdg(TASKS_NUM, 25000);

static bool isServiceModeOn = false;
uint32_t isLowVoltage = 0;
bool getTemp = false;
bool locdbInited = true;
bool ADCinited = false;

#define UNIQUE_DEV_ID_ADDR		(void*)(0x1FFF7A10)

void generate_mac(char* mac) {
	char *ptr = (char*)UNIQUE_DEV_ID_ADDR;

	for(int q = 0; q < 6; q++) {
		mac[q] = ptr[q] ^ ptr[11 - q];
	}

	mac[0] = (mac[0] | 0x02) & 0xFE;
}

void settToDef (void * data) {
	Sett_t * pset = (Sett_t*)data;

	// devID не устанавливаем в значение по умолчанию.
	// Этот параметр должен сохраняться при сбросе настроек, а также при перепрошивке на версию с измененной структурой настроек
	// Важно, чтобы он лежал первый в стуктуре настроек
	pset->devID = *(uint32_t*)0x080E0000;
	pset->logs_mode = 1;
	pset->net_mode = 0;
	pset->ip = 0;
	pset->gateway = 0;
	pset->mask = 0x00FFFFFF;
	pset->dns1 = 0;
	pset->dns2 = 0;
	generate_mac((char*)&pset->mac);
	pset->con_timeout = 15;
	pset->dev_type = 0;
	pset->dev_number = 0;
	pset->tx_pwr = 3;
	pset->beac_trh = -70;
	pset->enable_dist_sensor = 1;
	pset->enable_gate_feedback = 1;
	pset->dist_sensor_threshold = 150;
}

Settings<Sett_t> settings(settToDef, FLASH_SECTOR_11, 0x080E0000);

void mbed_mac_address(char* mac) {
	if (settings.isDefault) {
		generate_mac(mac);
	}
	else {
		core_util_critical_section_enter();
		memcpy(mac, settings.data.mac, 6);
		core_util_critical_section_exit();
	}
}

extern "C" {
//static char faultbuff[512] = {0};

volatile unsigned int stacked_r0;
volatile unsigned int stacked_r1;
volatile unsigned int stacked_r2;
volatile unsigned int stacked_r3;

volatile unsigned int stacked_r12;
volatile unsigned int stacked_lr;
volatile unsigned int stacked_pc;
volatile unsigned int stacked_psr;

volatile unsigned int BFAR;
volatile unsigned int CFSR;
volatile unsigned int HFSR;
volatile unsigned int DFSR;
volatile unsigned int AFSR;
volatile unsigned int SCB_SHCSR;

void error(const char* format, ...) {
	if (CoreDebug->DHCSR & 1)  {
		__BKPT (0);
	}

	va_list arg;
	va_start(arg, format);
//	core_util_critical_section_enter();
	static char buffer[128];
	int size = vsprintf(buffer, format, arg);
	if (size > 0) {
		LOG(buffer);
		sdcard.printfLog(SdCard::LOG_STACK, NULL);
	}
//	core_util_critical_section_exit();
	va_end(arg);
	_exit(1);
}

void mbed_die(void) {
    core_util_critical_section_enter();
    gpio_t led;
    gpio_init_out(&led, PD_14);

    int cnt = 3;
    while (cnt--) {
        gpio_write(&led, 1);
        wait_ms(150);
        gpio_write(&led, 0);
        wait_ms(150);
    }

    NVIC_SystemReset();
}

void NMI_Handler() {
    asm volatile(
        "tst lr, #4\t\n" /* Check EXC_RETURN[2] */
        "ite eq\t\n"
        "mrseq r0, msp\t\n"
        "mrsne r0, psp\t\n"
        "b nmi_handler_c\t\n"
        : /* no output */
        : /* no input */
        : "r0" /* clobber */
    );
}

void HardFault_Handler() {
    asm volatile(
        "tst lr, #4\t\n" /* Check EXC_RETURN[2] */
        "ite eq\t\n"
        "mrseq r0, msp\t\n"
        "mrsne r0, psp\t\n"
        "b hard_fault_handler_c\t\n"
        : /* no output */
        : /* no input */
        : "r0" /* clobber */
    );
}
void MemManage_Handler() {
    asm volatile(
        "tst lr, #4\t\n" /* Check EXC_RETURN[2] */
        "ite eq\t\n"
        "mrseq r0, msp\t\n"
        "mrsne r0, psp\t\n"
        "b mem_manage_handler_c\t\n"
        : /* no output */
        : /* no input */
        : "r0" /* clobber */
    );
}
void BusFault_Handler() {
    asm volatile(
        "tst lr, #4\t\n" /* Check EXC_RETURN[2] */
        "ite eq\t\n"
        "mrseq r0, msp\t\n"
        "mrsne r0, psp\t\n"
        "b bus_fault_handler_c\t\n"
        : /* no output */
        : /* no input */
        : "r0" /* clobber */
    );
}
void UsageFault_Handler() {
    asm volatile(
        "tst lr, #4\t\n" /* Check EXC_RETURN[2] */
        "ite eq\t\n"
        "mrseq r0, msp\t\n"
        "mrsne r0, psp\t\n"
        "b usage_fault_handler_c\t\n"
        : /* no output */
        : /* no input */
        : "r0" /* clobber */
    );
}

void fault_handler_c(unsigned int * hardfault_args, const char* source)
{
	//Exception stack frame
	stacked_r0 = ((unsigned long) hardfault_args[0]);
	stacked_r1 = ((unsigned long) hardfault_args[1]);
	stacked_r2 = ((unsigned long) hardfault_args[2]);
	stacked_r3 = ((unsigned long) hardfault_args[3]);

	stacked_r12 = ((unsigned long) hardfault_args[4]);
	stacked_lr = ((unsigned long) hardfault_args[5]);
	stacked_pc = ((unsigned long) hardfault_args[6]);
	stacked_psr = ((unsigned long) hardfault_args[7]);

	BFAR = (*((volatile unsigned long *) (0xE000ED38)));
	CFSR = (*((volatile unsigned long *) (0xE000ED28)));
	HFSR = (*((volatile unsigned long *) (0xE000ED2C)));
	DFSR = (*((volatile unsigned long *) (0xE000ED30)));
	AFSR = (*((volatile unsigned long *) (0xE000ED3C)));
	SCB_SHCSR = SCB->SHCSR;

	if (CoreDebug->DHCSR & 1)  {
		__BKPT (0);
	}

//	sprintf(faultbuff, "%s\n", source);
//	sprintf(faultbuff + strlen(faultbuff), "\tR0 = 0x%x\n", stacked_r0);
//	sprintf(faultbuff + strlen(faultbuff), "\tR1 = 0x%x\n", stacked_r1);
//	sprintf(faultbuff + strlen(faultbuff), "\tR2 = 0x%x\n", stacked_r2);
//	sprintf(faultbuff + strlen(faultbuff), "\tR3 = 0x%x\n", stacked_r3);
//	sprintf(faultbuff + strlen(faultbuff), "\tR12 = 0x%x\n", stacked_r12);
//	sprintf(faultbuff + strlen(faultbuff), "\tLR[R14] = 0x%x\n", stacked_lr);
//	sprintf(faultbuff + strlen(faultbuff), "\tPC[R15] = 0x%x\n", stacked_pc);
//	sprintf(faultbuff + strlen(faultbuff), "\tPSR = 0x%x\n", stacked_psr);
//	sprintf(faultbuff + strlen(faultbuff), "\tBFAR = 0x%x\n", BFAR);
//	sprintf(faultbuff + strlen(faultbuff), "\tCFSR = 0x%x\n", CFSR);
//	sprintf(faultbuff + strlen(faultbuff), "\tHFSR = 0x%x\n", HFSR);
//	sprintf(faultbuff + strlen(faultbuff), "\tDFSR = 0x%x\n", DFSR);
//	sprintf(faultbuff + strlen(faultbuff), "\tAFSR = 0x%x\n", AFSR);
//	sprintf(faultbuff + strlen(faultbuff), "\tSCB_SHCSR = 0x%x\n", SCB_SHCSR);
//
//	LOG(faultbuff);

	mbed_die();
}

void hard_fault_handler_c(unsigned int * hardfault_args)
{
	fault_handler_c(hardfault_args, "hard_fault");
}

void mem_manage_handler_c(unsigned int * hardfault_args)
{
	fault_handler_c(hardfault_args, "mem_manage");
}

void bus_fault_handler_c(unsigned int * hardfault_args)
{
	fault_handler_c(hardfault_args, "bus_fault");
}

void usage_fault_handler_c(unsigned int * hardfault_args)
{
	fault_handler_c(hardfault_args, "usage_fault");
}

void nmi_handler_c(unsigned int * hardfault_args)
{
	fault_handler_c(hardfault_args, "nmi");
}
}

//--------------------

extern void BLE_thread(void const *args);
extern void NET_thread(void const *args);

extern bool BleAdvertisingOn;
extern bool BleConnected;

Thread *p_ble_thread;
Thread *p_net_thread;
Thread *p_log_thread;

osThreadId mainThreadID;

#define LED_SD_PIN			PC_6
#define LED_NET_PIN			PC_9
#define LED_BLE_PIN			PC_8
#define LED_HEALTH_PIN		PC_7

Led_t leds[4] = {
		{ {LED_SD_PIN, 1}, 0, LED_OFF },
		{ {LED_NET_PIN, 1}, 0, LED_OFF },
		{ {LED_BLE_PIN, 1}, 0, LED_OFF },
		{ {LED_HEALTH_PIN, 1}, 0, LED_OFF }
};

DigitalOut Buz(PE_6, 0);

DigitalIn Butt(PG_10, PullUp);
InterruptIn comp(PF_7);
AnalogIn temp(PF_6);

SDFileSystem *p_sd;

Config_t config = {
		beeper				: BEEPER_ON_DEF,
		advertise			: ADVERTISE_DEF,
		scan				: SCAN_DEF,
		adv_interval		: ADV_INTERVAL_DEF,
		auth_timeout		: AUTHORISATION_TIMEOUT_DEF,
		att_mtu				: ATT_MTU_DEF,
		min_conn_interval	: MIN_CONN_INTERVAL_MS_DEF,
		max_conn_interval	: MAX_CONN_INTERVAL_MS_DEF,
		slave_latency		: SLAVE_LATENCY_DEF,
		supervision_timeout	: CONN_SUP_TIMEOUT_MS_DEF
};

static bool buttonIsPressed() {
	bool ret;

	if (Butt == 0) {
		ret = true;
	}
	else {
		ret = false;
	}

	return ret;
}

static void groundSpiPins() {
	DigitalOut led1(SPI_MOSI_PIN);
	DigitalOut led2(SPI_MISO_PIN);
	DigitalOut led3(SPI_SCK_PIN);
	DigitalOut led4(SPI_CS_PIN);

	led1 = 0;
	led2 = 0;
	led3 = 0;
	led4 = 0;
}

#define BUZ_TIMER_PERIOD		10		// ms
#define BUZ_BEEP_PERIOD			50		// ms

static void BuzTimerClbk (void const *n) {
	static uint32_t cnt = 0;
	static int state = 0;

	cnt++;

	if (BleConnected) {
		if (cnt % (BUZ_BEEP_PERIOD/BUZ_TIMER_PERIOD) == 0) {
			// Период переполнения таймера
			state = state == 0 ? 1 : 0;
			Buz = state;
		}
	}
	else if (BleAdvertisingOn) {
		if (state == 1) {
			Buz = state = 0;
		}
	}
	else {
		if (state == 0) {
			Buz = state = 1;
		}
	}
}

static RtosTimer BuzTimer(BuzTimerClbk, osTimerPeriodic);

ADC_HandleTypeDef AdcHandle;
ADC_ChannelConfTypeDef sConfig = {0};

static void initADC() {

	  GPIO_InitTypeDef GPIO_InitStruct;

	  /* Peripheral clock enable */
	  __HAL_RCC_GPIOA_CLK_ENABLE();

	  GPIO_InitStruct.Pin = GPIO_PIN_0;
	  GPIO_InitStruct.Mode = GPIO_MODE_ANALOG;
	  GPIO_InitStruct.Pull = GPIO_NOPULL;
	  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;

	  HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	__ADC1_CLK_ENABLE();
	__ADC3_CLK_ENABLE();

    // Configure ADC
	AdcHandle.Instance = (ADC_TypeDef *)(ADC_3);
	AdcHandle.Init.ClockPrescaler        = ADC_CLOCKPRESCALER_PCLK_DIV2;
	AdcHandle.Init.Resolution            = ADC_RESOLUTION12b;
	AdcHandle.Init.ScanConvMode          = DISABLE;
	AdcHandle.Init.ContinuousConvMode    = DISABLE;
	AdcHandle.Init.DiscontinuousConvMode = DISABLE;
	AdcHandle.Init.NbrOfDiscConversion   = 0;
	AdcHandle.Init.ExternalTrigConvEdge  = ADC_EXTERNALTRIGCONVEDGE_NONE;
	AdcHandle.Init.ExternalTrigConv      = ADC_EXTERNALTRIGCONV_T1_CC1;
	AdcHandle.Init.DataAlign             = ADC_DATAALIGN_RIGHT;
	AdcHandle.Init.NbrOfConversion       = 1;
	AdcHandle.Init.DMAContinuousRequests = DISABLE;
	AdcHandle.Init.EOCSelection          = DISABLE;

    // Configure ADC channel
    sConfig.Rank         = 1;
    sConfig.SamplingTime = ADC_SAMPLETIME_3CYCLES;
    sConfig.Offset       = 0;
    sConfig.Channel      = ADC_CHANNEL_0;

    if (HAL_ADC_Init(&AdcHandle) != HAL_OK) {
    	return;
    }
    if (HAL_ADC_ConfigChannel(&AdcHandle, &sConfig) != HAL_OK) {
    	return;
    }

    // Настройка VRef
    AdcHandle.Instance = (ADC_TypeDef *)(ADC_1);
    sConfig.Channel = ADC_CHANNEL_VREFINT;

    if (HAL_ADC_Init(&AdcHandle) != HAL_OK) {
    	return;
    }
    if (HAL_ADC_ConfigChannel(&AdcHandle, &sConfig) != HAL_OK) {
    	return;
    }

    ADCinited = true;
}

static uint16_t readADCVrefintVal() {
	AdcHandle.Instance = (ADC_TypeDef *)(ADC_1);
    HAL_ADC_Start(&AdcHandle); // Start conversion

    if (HAL_ADC_PollForConversion(&AdcHandle, 10) == HAL_OK) {
        return (HAL_ADC_GetValue(&AdcHandle));
    }
    else {
    	return -1;
    }
}

static uint16_t readADCvalue() {
	AdcHandle.Instance = (ADC_TypeDef *)(ADC_3);
    HAL_ADC_Start(&AdcHandle); // Start conversion

    if (HAL_ADC_PollForConversion(&AdcHandle, 10) == HAL_OK) {
        return (HAL_ADC_GetValue(&AdcHandle));
    }
    else {
    	return -1;
    }
}

static void GetTempTimerClbk (void const *n) {
	core_util_critical_section_enter();
	getTemp = true;
	core_util_critical_section_exit();
}

RtosTimer SyncTempTimer(GetTempTimerClbk, osTimerPeriodic);

static signed char calcTheTemp() {
	uint16_t  calibration = *(uint16_t*)0x1FFF7A2A; // читаю калибровочное значение для ацп из регистра, указан был в даташите
	uint16_t adc_val = readADCVrefintVal();
	double VREF = (double)3300 * calibration / adc_val;// рассчитываю опроное напряжение
	adc_val = readADCvalue(); // читаю код ацп с датчика
	double voltage = VREF * adc_val * (1.0f / (float)0xFFF); // преобразую в напряжение

	//float temperature = (10.888f-sqrt(pow(-10.888f,2.0f)+0.00347f*(1777.3f-voltage)*4.0f))/(-0.00347f*2.0f)+30.0f; - из даташита
	//temperature = sqrt(-0.01388 * voltage + 143.217468) * 144.09221902017291066 - 1538.8760806916426513; - упрощенная формула
	double temperature = (voltage - 424) / 6.25;
	return (signed char)round(temperature);
}

static bool needGetTemp() {
	bool ret;
	core_util_critical_section_enter();
	ret = getTemp;
	getTemp = false;
	core_util_critical_section_exit();
	return  (ret && ADCinited);
}

Mail<int8_t, 1> TemperatureMailbox;


static void sysInit() {
//	SCB->CCR |= 0x18;
	SCB->SHCSR |= SCB_SHCSR_USGFAULTENA_Msk | SCB_SHCSR_BUSFAULTENA_Msk | SCB_SHCSR_MEMFAULTENA_Msk;
}

static void disablePeripherals() {
//	__HAL_RCC_GPIOA_CLK_DISABLE();
	__HAL_RCC_GPIOB_CLK_DISABLE();
//	__HAL_RCC_GPIOC_CLK_DISABLE();
	__HAL_RCC_GPIOH_CLK_DISABLE();
	__HAL_RCC_DMA1_CLK_DISABLE();
	__HAL_RCC_DMA2_CLK_DISABLE();
//	__HAL_RCC_GPIOD_CLK_DISABLE();
	__HAL_RCC_GPIOE_CLK_DISABLE();
	__HAL_RCC_GPIOF_CLK_DISABLE();
	__HAL_RCC_GPIOG_CLK_DISABLE();
	__HAL_RCC_GPIOI_CLK_DISABLE();
	__HAL_RCC_WWDG_CLK_DISABLE();
	__HAL_RCC_SPI2_CLK_DISABLE();
	__HAL_RCC_USART2_CLK_DISABLE();
	__HAL_RCC_I2C1_CLK_DISABLE();
	__HAL_RCC_I2C2_CLK_DISABLE();
	__HAL_RCC_PWR_CLK_DISABLE();
	__HAL_RCC_USART1_CLK_DISABLE();
	__HAL_RCC_USART6_CLK_DISABLE();
	__HAL_RCC_ADC1_CLK_DISABLE();
	__HAL_RCC_USB_OTG_HS_CLK_DISABLE();
	__HAL_RCC_USB_OTG_HS_ULPI_CLK_DISABLE();
	__HAL_RCC_BKPSRAM_CLK_DISABLE();
	__HAL_RCC_CRC_CLK_DISABLE();
	__HAL_RCC_ETHMAC_CLK_DISABLE();
	__HAL_RCC_ETHMACTX_CLK_DISABLE();
	__HAL_RCC_ETHMACRX_CLK_DISABLE();
	__HAL_RCC_ETHMACPTP_CLK_DISABLE();
	__HAL_RCC_DCMI_CLK_DISABLE();
	__HAL_RCC_USB_OTG_FS_CLK_DISABLE();
	__HAL_RCC_RNG_CLK_DISABLE();
	__HAL_RCC_SPI3_CLK_DISABLE();
	__HAL_RCC_I2C3_CLK_DISABLE();
	__HAL_RCC_USART3_CLK_DISABLE();
	__HAL_RCC_UART4_CLK_DISABLE();
	__HAL_RCC_UART5_CLK_DISABLE();
	__HAL_RCC_CAN1_CLK_DISABLE();
	__HAL_RCC_CAN2_CLK_DISABLE();
	__HAL_RCC_DAC_CLK_DISABLE();
	__HAL_RCC_SDIO_CLK_DISABLE();
	__HAL_RCC_ADC2_CLK_DISABLE();
	__HAL_RCC_ADC3_CLK_DISABLE();
	__HAL_RCC_TIM1_CLK_DISABLE();
	__HAL_RCC_TIM2_CLK_DISABLE();
	__HAL_RCC_TIM3_CLK_DISABLE();
	__HAL_RCC_TIM4_CLK_DISABLE();
	__HAL_RCC_TIM6_CLK_DISABLE();
	__HAL_RCC_TIM7_CLK_DISABLE();
	__HAL_RCC_TIM8_CLK_DISABLE();
	__HAL_RCC_TIM9_CLK_DISABLE();
	__HAL_RCC_TIM10_CLK_DISABLE();
	__HAL_RCC_TIM11_CLK_DISABLE();
	__HAL_RCC_TIM12_CLK_DISABLE();
	__HAL_RCC_TIM13_CLK_DISABLE();
	__HAL_RCC_TIM14_CLK_DISABLE();
}

static void shutdownDueToPower() {
	leds[LED_NET].led = 1;
	leds[LED_BLE].led = 1;
	leds[LED_HEALTH].led = 1;
	leds[LED_SD].led = 1;
	EzServer.disablePHY();
	disablePeripherals();
	sdcard.safelyRemove();
	wait_ms(1000);
	while (1) {
		wdg.kick(TASK_ALL);
		if ((GPIOE->IDR & GPIO_IDR_IDR_13) != 0) {
			NVIC_SystemReset();
		}
	}
}

void shutdownCMD() {
	leds[LED_NET].led = 1;
	leds[LED_BLE].led = 1;
	leds[LED_HEALTH].led = 1;
	leds[LED_SD].led = 1;
	EzServer.disablePHY();
	disablePeripherals();
	sdcard.safelyRemove();
	db.backupEvents();
	p_ble_thread->terminate();
	p_net_thread->terminate();
	while (1) {
		Thread::wait(1000);
		wdg.kick(TASK_ALL);
	}
}

#ifndef DEBUG
static void ConfigOptionByte() {
	FLASH_OBProgramInitTypeDef OBInit;
	HAL_FLASHEx_OBGetConfig(&OBInit);

	if (OBInit.USERConfig != (OB_WDG_HW | OB_STOP_RST | OB_STDBY_RST)) {
		HAL_FLASH_OB_Unlock();

		OBInit.USERConfig = (OB_WDG_HW | OB_STOP_RST | OB_STDBY_RST);
		OBInit.OptionType = OPTIONBYTE_USER;

		HAL_FLASHEx_OBProgram(&OBInit);
		HAL_FLASH_OB_Launch();

		HAL_FLASH_OB_Lock();
	}
}
#endif

extern "C"
{
ETH_HandleTypeDef heth;
}

static void shutDown() {
	leds[LED_NET].led = 1;
	leds[LED_BLE].led = 1;
	leds[LED_HEALTH].led = 1;
	leds[LED_SD].led = 1;

	// Меняем режим работы
	uint32_t mode = (1 << 11);
	HAL_ETH_WritePHYRegister(&heth, PHY_BCR, mode);
	disablePeripherals();
	db.backupEvents();
	core_util_atomic_incr_u32(&isLowVoltage, 1);
}

void rebootMCU() {
	sdcard.safelyRemove();
	db.backupEvents();
	NVIC_SystemReset();
}


SRAM_HandleTypeDef hsram1;

/* FSMC initialization function */
void MX_FSMC_Init(void)
{
  FSMC_NORSRAM_TimingTypeDef Timing;

  /** Perform the SRAM1 memory initialization sequence
  */
  hsram1.Instance = FSMC_NORSRAM_DEVICE;
  hsram1.Extended = FSMC_NORSRAM_EXTENDED_DEVICE;
  /* hsram1.Init */
  hsram1.Init.NSBank = FSMC_NORSRAM_BANK1;
  hsram1.Init.DataAddressMux = FSMC_DATA_ADDRESS_MUX_DISABLE;
  hsram1.Init.MemoryType = FSMC_MEMORY_TYPE_SRAM;
  hsram1.Init.MemoryDataWidth = FSMC_NORSRAM_MEM_BUS_WIDTH_16;
  hsram1.Init.BurstAccessMode = FSMC_BURST_ACCESS_MODE_DISABLE;
  hsram1.Init.WaitSignalPolarity = FSMC_WAIT_SIGNAL_POLARITY_LOW;
  hsram1.Init.WrapMode = FSMC_WRAP_MODE_DISABLE;
  hsram1.Init.WaitSignalActive = FSMC_WAIT_TIMING_BEFORE_WS;
  hsram1.Init.WriteOperation = FSMC_WRITE_OPERATION_ENABLE;
  hsram1.Init.WaitSignal = FSMC_WAIT_SIGNAL_DISABLE;
  hsram1.Init.ExtendedMode = FSMC_EXTENDED_MODE_DISABLE;
  hsram1.Init.AsynchronousWait = FSMC_ASYNCHRONOUS_WAIT_DISABLE;
  hsram1.Init.WriteBurst = FSMC_WRITE_BURST_DISABLE;
  hsram1.Init.PageSize = FSMC_PAGE_SIZE_NONE;
  /* Timing */
  Timing.AddressSetupTime = 15;
  Timing.AddressHoldTime = 15;
  Timing.DataSetupTime = 255;
  Timing.BusTurnAroundDuration = 15;
  Timing.CLKDivision = 16;
  Timing.DataLatency = 17;
  Timing.AccessMode = FSMC_ACCESS_MODE_A;
  /* ExtTiming */

  if (HAL_SRAM_Init(&hsram1, &Timing, NULL) != HAL_OK)
  {
    mbed_die();
  }

}

static void HAL_FSMC_MspInit(void){
  GPIO_InitTypeDef GPIO_InitStruct;

  /* Peripheral clock enable */
  __HAL_RCC_FSMC_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOG_CLK_ENABLE();

  /** FSMC GPIO Configuration
  PF0   ------> FSMC_A0
  PF1   ------> FSMC_A1
  PF2   ------> FSMC_A2
  PF3   ------> FSMC_A3
  PF4   ------> FSMC_A4
  PF5   ------> FSMC_A5
  PF12   ------> FSMC_A6
  PF13   ------> FSMC_A7
  PF14   ------> FSMC_A8
  PF15   ------> FSMC_A9
  PG0   ------> FSMC_A10
  PG1   ------> FSMC_A11
  PE7   ------> FSMC_D4
  PE8   ------> FSMC_D5
  PE9   ------> FSMC_D6
  PE10   ------> FSMC_D7
  PE11   ------> FSMC_D8
  PE12   ------> FSMC_D9
  PE13   ------> FSMC_D10
  PE14   ------> FSMC_D11
  PE15   ------> FSMC_D12
  PD8   ------> FSMC_D13
  PD9   ------> FSMC_D14
  PD10   ------> FSMC_D15
  PD11   ------> FSMC_A16
  PD12   ------> FSMC_A17
  PD13   ------> FSMC_A18
  PD14   ------> FSMC_D0
  PD15   ------> FSMC_D1
  PG2   ------> FSMC_A12
  PG3   ------> FSMC_A13
  PG4   ------> FSMC_A14
  PG5   ------> FSMC_A15
  PD0   ------> FSMC_D2
  PD1   ------> FSMC_D3
  PD4   ------> FSMC_NOE
  PD5   ------> FSMC_NWE
  PD7   ------> FSMC_NE1
  PE0   ------> FSMC_NBL0
  PE1   ------> FSMC_NBL1
  */
  /* GPIO_InitStruct */
  GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3
                          |GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_12|GPIO_PIN_13
                          |GPIO_PIN_14|GPIO_PIN_15;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF12_FSMC;

  HAL_GPIO_Init(GPIOF, &GPIO_InitStruct);

  /* GPIO_InitStruct */
  GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3
                          |GPIO_PIN_4|GPIO_PIN_5;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF12_FSMC;

  HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

  /* GPIO_InitStruct */
  GPIO_InitStruct.Pin = GPIO_PIN_7|GPIO_PIN_8|GPIO_PIN_9|GPIO_PIN_10
                          |GPIO_PIN_11|GPIO_PIN_12|GPIO_PIN_13|GPIO_PIN_14
                          |GPIO_PIN_15|GPIO_PIN_0|GPIO_PIN_1;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF12_FSMC;

  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /* GPIO_InitStruct */
  GPIO_InitStruct.Pin = GPIO_PIN_8|GPIO_PIN_9|GPIO_PIN_10|GPIO_PIN_11
                          |GPIO_PIN_12|GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15
                          |GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_4|GPIO_PIN_5
                          |GPIO_PIN_7;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF12_FSMC;

  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);
}

void HAL_SRAM_MspInit(SRAM_HandleTypeDef* sramHandle){
  HAL_FSMC_MspInit();
}

/// >>> Test start
//__ESRAM char buf[1024];
/// <<< Test stop

Timer timerForLeds;

void ledsState(LedType_t led, LedState_t state) {
	if (leds[led].state != state) {
		leds[led].led = (int)state;
		leds[led].state = state;
	}
}

Mail<uint16_t, 1> mail_dist;

static void dist(int distance)
{
	uint16_t *dist = mail_dist.alloc();
	if (dist) {
		*dist = distance;
		mail_dist.put(dist);
	}
	else {
		///\todo Error
	}
	PRINTF("\n%u\n",distance);
}

ultrasonic mu(PG_6, PG_7, 1, 1, &dist);

//===================================================================

int main() {
	sysInit();

	MX_FSMC_Init();

	/// >>> Test start
	//	bzero(buf, sizeof(buf));
	//	strcpy((char*)buf, "My second sram data!");
	//	char buf3[50];
	//	strcpy(buf3, buf);
	/// <<< Test stop

	mtime.set(0);



#ifndef DEBUG
	ConfigOptionByte();
#endif

	comp.fall(&shutDown);
	comp.mode(PullUp);

	ledsState(LED_NET, LED_ON);
	ledsState(LED_BLE, LED_ON);
	ledsState(LED_HEALTH, LED_ON);
	ledsState(LED_SD, LED_ON);

	// Уводим в 0 ноги SPI
	groundSpiPins();

	sdcard.SdPowerOff();

	if (strcmp(wdg.getResetReason(), "PINRST") != 0 && strcmp(wdg.getResetReason(), "WWDGRST") != 0  && strcmp(wdg.getResetReason(), "IWDGRST") != 0) {
		if (db.checkEvents()) {
			HAL_FLASH_Unlock();
			FLASH_Erase_Sector(FLASH_SECTOR_10, VOLTAGE_RANGE_1);
			HAL_FLASH_Lock();
		}
		else {
			Thread::wait(1000);
		}
	}
	else {
		Thread::wait(1000);
	}

	sdcard.SdPowerOn();
	Thread::wait(100);

	// Заводим объект SD здесь, чтобы конструктор, настраивающий SPI вызывался после groundSpiPins()
	SDFileSystem sd(SPI_MOSI_PIN, SPI_MISO_PIN, SPI_SCK_PIN, SPI_CS_PIN, "sd");
	p_sd = &sd;

	ledsState(LED_NET, LED_OFF);
	ledsState(LED_BLE, LED_OFF);
	ledsState(LED_HEALTH, LED_OFF);
	ledsState(LED_SD, LED_OFF);

	SyncTempTimer.start(100);
	initADC();

	sdcard.init();

	if (sdcard.loadConfig(&config) != 0) {
		for (int q = 0; q < 10; q++) {
			ledsState(LED_HEALTH, LED_ON);
			Thread::wait(100);
			ledsState(LED_HEALTH, LED_OFF);
			Thread::wait(100);
		}
	}

	if (config.beeper) {
		BuzTimer.start(BUZ_TIMER_PERIOD);
	}

	wdg.kick(TASK_ALL);

	mtime.set(sdcard.lastTime());	// последнее время из лога common.log или 01 Jan 2016 00:00:00 GMT

	settings.load();

	sdcard.printfLog(SdCard::LOG_COMMON, "\n\n<< Boot Device ver %s >>\n", db.getCurrFirmwareVer());
	sdcard.printfLog(SdCard::LOG_COMMON, "Reset reason: %s\n", wdg.getResetReason());

	const char *const pfault = settings.read_fault_data();
	if (pfault != NULL) {
		sdcard.printfLog(SdCard::LOG_COMMON, "%s\n", pfault);
	}

	sdcard.printSettingsLog();
	sdcard.printConfigLog(&config);

	wdg.kick(TASK_ALL);

//	volatile uint32_t* varAtOddAddress = (uint32_t*)-1;
//	(*varAtOddAddress)++;

	mainThreadID = osThreadGetId();

	p_ble_thread = new Thread(BLE_thread, NULL, osPriorityNormal, BLE_TASK_STACK_SIZE);
	p_net_thread = new Thread(NET_thread, NULL, osPriorityNormal, NET_TASK_STACK_SIZE);
	
	wdg.kick(TASK_ALL);

	timerForLeds.start();

	mu.startUpdates();

	while(1) {
		mu.checkDistance();

//		PRINTF("\n%d\n", calcTheTemp());

//		Thread::wait(1000);

		osEvent evt = Thread::signal_wait(0, 0);
		if (evt.status == osEventSignal) {
			int32_t signals = evt.value.signals;

			if (signals & SERVICE_MODE_ON) {
				isServiceModeOn = true;
			}
			if (signals & SERVICE_MODE_OFF) {
				isServiceModeOn = false;
			}
			if (signals & SHUTDOWN_MODE_ON) {
				shutdownCMD();
			}
		}

		if (needGetTemp() && ADCinited) {
			int8_t *mail = TemperatureMailbox.alloc();
			if (mail) {
				*mail = calcTheTemp();
				TemperatureMailbox.put(mail);
			}
			else {
				///\todo Error
			}
		}

		if (!isLowVoltage) {
			uint32_t currentTime = (uint32_t)timerForLeds.read_ms();

			// Card LED
			if (!cardDetected()) {
				ledsState(LED_SD, LED_OFF);
			}
			else if (sdcard.cardError()) {
				if ((currentTime - leds[LED_SD].tr) >= 500 && leds[LED_SD].state == LED_OFF) {
					ledsState(LED_SD, LED_ON);
					leds[LED_SD].tr = currentTime;
				}
				if ((currentTime - leds[LED_SD].tr) >= 100 && leds[LED_SD].state == LED_ON) {
					ledsState(LED_SD, LED_OFF);
					leds[LED_SD].tr = currentTime;
				}
			}
			else if (sdcard.isBusy()) {
				if ((currentTime - leds[LED_SD].tr) >= 30 && leds[LED_SD].state == LED_OFF) {
					ledsState(LED_SD, LED_ON);
					leds[LED_SD].tr = currentTime;
				}
				if ((currentTime - leds[LED_SD].tr) >= 30 && leds[LED_SD].state == LED_ON) {
					ledsState(LED_SD, LED_OFF);
					leds[LED_SD].tr = currentTime;
				}
			}
			else {
				ledsState(LED_SD, LED_ON);
			}

			// Health LED
			if(!EzServer.checkPHY()) {
				if ((currentTime - leds[LED_HEALTH].tr) >= 500 && leds[LED_HEALTH].state == LED_OFF) {
					ledsState(LED_HEALTH, LED_ON);
					leds[LED_HEALTH].tr = currentTime;
				}
				if ((currentTime - leds[LED_HEALTH].tr) >= 100 && leds[LED_HEALTH].state == LED_ON) {
					ledsState(LED_HEALTH, LED_OFF);
					leds[LED_HEALTH].tr = currentTime;
				}
			}
			else if(!cardDetected() || sdcard.cardError() || !EzServer.connectedToServer()) {
				ledsState(LED_HEALTH, LED_OFF);
			}
			else {
				ledsState(LED_HEALTH, LED_ON);
			}

			// Net LED
			if (EzServer.isBusy()) {
				if ((currentTime - leds[LED_NET].tr) >= 30 && leds[LED_NET].state == LED_OFF) {
					ledsState(LED_NET, LED_ON);
					leds[LED_NET].tr = currentTime;
				}
				if ((currentTime - leds[LED_NET].tr) >= 30 && leds[LED_NET].state == LED_ON) {
					ledsState(LED_NET, LED_OFF);
					leds[LED_NET].tr = currentTime;
				}
			}
			else if (EzServer.connectedToServer()) {
				ledsState(LED_NET, LED_ON);
			}
			else {
				ledsState(LED_NET, LED_OFF);
			}

			// BLE LED
			if (BleAdvertisingOn) {
				ledsState(LED_BLE, LED_ON);
			}
			else if (BleConnected) {
				if ((currentTime - leds[LED_BLE].tr) >= 30 && leds[LED_BLE].state == LED_OFF) {
					ledsState(LED_BLE, LED_ON);
					leds[LED_BLE].tr = currentTime;
				}
				if ((currentTime - leds[LED_BLE].tr) >= 30 && leds[LED_BLE].state == LED_ON) {
					ledsState(LED_BLE, LED_OFF);
					leds[LED_BLE].tr = currentTime;
				}
			}
			else {
				ledsState(LED_BLE, LED_OFF);
			}

			// Button
			if (buttonIsPressed()) {
				ledsState(LED_HEALTH, LED_ON);
				Timer timer;
				timer.start();
				while (buttonIsPressed()) {
					if (timer.read_ms() == 5000) {
						settings.resetSettings();
						sdcard.printSettingsLog();
						LOG("Settings are reseted by pressing a button, Reboot...");
						ledsState(LED_HEALTH, LED_ON);
						Thread::wait(500);
						ledsState(LED_HEALTH, LED_OFF);
						Thread::wait(500);
						ledsState(LED_HEALTH, LED_ON);
						Thread::wait(500);
						ledsState(LED_HEALTH, LED_OFF);
						Thread::wait(500);
						ledsState(LED_HEALTH, LED_ON);
						Thread::wait(500);
						ledsState(LED_HEALTH, LED_OFF);
						Thread::wait(500);
						rebootMCU();
					}
				}
			}
		}
		else {
			shutdownDueToPower();
		}

		Thread::wait(1);

		wdg.kick(TASK_MAIN);
	}
}

