﻿/*
 * net.cpp
 *
 *  Created on: 7 июл. 2016 г.
 *      Author: Chuyec
 */

#include <mbed.h>
#include <rtos.h>
#include <SDFileSystem/SDFileSystem.h>
#include <EzeparkServer/EzeparkServer.h>
#include <MyTime/MyTime.h>
#include <Settings/Settings.h>
#include <common.h>
#include <DataBase/DataBase.h>
#include <Watchdog/Watchdog.h>
#include <NTPClient/NTPClient.h>

#include <trace.h>

#include "EthernetInterface.h"
#include <HTTPClient/HTTPClient.h>
#include <SdCard/SdCard.h>

#include "nrf_dfu/Dfu.h"

extern EzeparkServer EzServer;
extern Settings<Sett_t> settings;
extern uint8_t *p_DFU_Data;
extern bool BleAdvertisingOn;
extern bool BleConnected;
extern void BLE_thread(void const *args);

//===================================================================

#define SYNC_ALL_TIME		4

#define SYNCH_EVENTS_PERIOD			25*1000
#define SYNCH_EVENTS_SLOW_PERIOD	60*1000
#define SYNCH_TIME_PERIOD			3600*1000
#define CHECK_PERIOD				5*1000

#define TIME_OF_REBOOT				3*60*1000


void LOG_thread(void const *args);

volatile static bool syncEvents = true;
volatile static bool syncTime = true;
volatile static bool check = false;

Mail<uint8_t, 1> sdcardCmdMailbox;

Dfu ble_dfu(p_DFU_Data);
bool BLEupdated = false;

NTPClient ntp;

static int updateTime() {
	static uint8_t serverNum = 0;
	const static char *servers[] = {"time.windows.com", "time.nist.gov", "0.pool.ntp.org", "1.pool.ntp.org"};
	int ret;

	if (EzServer.isCableConnected()){
		LOG("Sync time...");

		NTPResult res = ntp.setTime(servers[serverNum], NTP_DEFAULT_PORT, 2000);

		if (res != NTP_OK) {
			serverNum++;

			if (serverNum >= sizeof(servers)/sizeof(char*)) {
				serverNum = 0;
			}
		}

		if (res == NTP_OK) {
			sdcard.timeUpdated();
			LOG("Sync time OK");
			ret = 0;
		}
		else {
			LOG("Sync time FAIL");
			ret = -1;
		}
	}
	else {
		ret = -1;
	}

	return ret;
}

static void printNetLog() {
	char buf[512];
	sprintf(buf, "\t%s: %s\n", "IP address", EzServer.getIPstr());
	sprintf(buf + strlen(buf), "\t%s: %s\n", "Gateway", EzServer.getGatewayStr());
	sprintf(buf + strlen(buf), "\t%s: %s\n", "Network mask", EzServer.getMaskStr());
	sprintf(buf + strlen(buf), "\t%s: %s\n", "DNS 1", EzServer.getDNS1Str());
	sprintf(buf + strlen(buf), "\t%s: %s\n", "DNS 2", EzServer.getDNS2Str());
	sdcard.printfLog(SdCard::LOG_COMMON, "NetIf UP: \n%s", buf);
}

static bool needSyncEvents() {
	bool ret;
	core_util_critical_section_enter();
	ret = syncEvents;
	core_util_critical_section_exit();
	return  ret;
}

static bool needSyncTime() {
	bool ret;
	core_util_critical_section_enter();
	ret = syncTime;
	core_util_critical_section_exit();
	return  ret;
}

static bool needCheck() {
	bool ret;
	core_util_critical_section_enter();
	ret = check;
	core_util_critical_section_exit();
	return  ret;
}

static void resetSyncEventsFlag() {
	core_util_critical_section_enter();
	syncEvents = false;
	core_util_critical_section_exit();
}

static void resetSyncTimeFlag() {
	core_util_critical_section_enter();
	syncTime = false;
	core_util_critical_section_exit();
}

static void resetCheckFlag() {
	core_util_critical_section_enter();
	check = false;
	core_util_critical_section_exit();
}

static void SyncEventsTimerClbk (void const *n) {
	core_util_critical_section_enter();
	syncEvents = true;
	core_util_critical_section_exit();
}

static void SyncTimeTimerClbk (void const *n) {
	core_util_critical_section_enter();
	syncTime = true;
	core_util_critical_section_exit();
}

static void CheckTimerClbk (void const *n) {
	core_util_critical_section_enter();
	check = true;
	core_util_critical_section_exit();
}


static RtosTimer SyncEventsTimer(SyncEventsTimerClbk, osTimerPeriodic);
static RtosTimer SyncTimeTimer(SyncTimeTimerClbk, osTimerPeriodic);
static RtosTimer CheckTimer(CheckTimerClbk, osTimerPeriodic);

extern Watchdog wdg;

extern Thread *p_ble_thread;
extern Thread *p_net_thread;
extern Thread *p_log_thread;

extern bool locdbInited;

extern bool isNetUp;
extern bool isNetDown;
static bool isInetUp = false;
static bool isInetDown = true;


static bool isServiceModeOn = false;

static bool deviceHasBeenDisabled = false;
static bool timeAlreadySynched = false;

static bool logUploading = false;

//-------------------------------------------------------------------
//--------- net common ----------------------------------------------

extern Mutex FW_Mutex;

Timer timerForReboot;

extern int32_t fopencnt;
extern int32_t opendircnt;

extern void rebootMCU();
extern bool cardDetected();

static uint8_t SD_format_cnt = 0;

static void trySDRecovery() {
	static bool isNeedCardCheck = true;
	if (isNeedCardCheck) {
		isNeedCardCheck = sdcard.checkSDcard();
	}
	else if (fopencnt == 0 && opendircnt == 0) {
		while (SD_format_cnt < 3) {
			Thread::wait(400);
			int ret = sdcard.formatDisk();
			if (ret == 0) {
				ret = sdcard.init();
				if (ret == 0) {
					db.cardFormatted();
					isNeedCardCheck = true;
				}
			}

			if (ret == 0) {
				SD_format_cnt = 0;
				break;
			}
			else {
				SD_format_cnt++;
			}
		}
	}
}

void NET_thread(void const *args) {
	EzServer.reset();
	PRINTF("\nClient IP Address is %s\n", EzServer.getIPstr());

//	EzServer.checkPHY();

	wdg.kick(TASK_NET);

	// Ждем 2 сек для того, чтобы физика очухалась после режима Loopback
	Thread::wait(2000);

	Thread log_thread(LOG_thread, NULL, osPriorityNormal, 0x3000);
	p_log_thread = &log_thread;

	SyncEventsTimer.start(SYNCH_EVENTS_PERIOD);
	SyncTimeTimer.start(SYNCH_TIME_PERIOD);
	CheckTimer.start(CHECK_PERIOD);

	while(1) {
        core_util_critical_section_enter();
        isInetUp = isNetUp;
        isInetDown = isNetDown;
        core_util_critical_section_exit();

        if (isInetUp) {
        	printNetLog();

        	core_util_critical_section_enter();
        	isNetUp = false;
        	core_util_critical_section_exit();
        }

        if (isInetDown) {
        	LOG("NetIf DOWN!!!\n");

        	core_util_critical_section_enter();
        	isNetDown = false;
        	core_util_critical_section_exit();
        }

		if (logUploading == false) {
			if (db.isNeedLogTransmit()) {
				p_log_thread->signal_set(LOG_UPLOAD);
				logUploading = true;
			}
		}
		else {
			osEvent evt = Thread::signal_wait(LOG_UPLOADED, 0);
			if (evt.status == osEventSignal) {
				logUploading = false;
			}
		}

		wdg.kick(TASK_NET);

		osEvent evt = Thread::signal_wait(0, 0);
		if (evt.status == osEventSignal) {
			int32_t signals = evt.value.signals;

			if (signals & SERVICE_MODE_ON) {
				isServiceModeOn = true;
			}
			if (signals & SERVICE_MODE_OFF) {
				isServiceModeOn = false;
			}
			if (signals & SD_CLEANUP) {
				p_ble_thread->signal_set(NET_READY_FOR_SD_CLEANUP);
				Thread::signal_wait(SD_CLEANUP_END);
			}
		}

		// Синхронизируем время
		if (needSyncTime()) {
			if (updateTime() == 0) {
				resetSyncTimeFlag();
				timeAlreadySynched = true;
				timerForReboot.stop();
				timerForReboot.reset();
				PRINTF("\nTIME: %u\n", mtime.get());
			}
			else {
				timerForReboot.start();
			}
		}


		if (isServiceModeOn && needCheck()) {
			EzServer.check();
			resetCheckFlag();
		}

		wdg.kick(TASK_NET);
		
		if (db.isDeviceOff() && !deviceHasBeenDisabled) {
			SyncEventsTimer.stop();
			SyncEventsTimer.start(SYNCH_EVENTS_SLOW_PERIOD);
			deviceHasBeenDisabled = true;
		}
		else if (!db.isDeviceOff() && deviceHasBeenDisabled){
			SyncEventsTimer.stop();
			SyncEventsTimer.start(SYNCH_EVENTS_PERIOD);
			deviceHasBeenDisabled = false;
		}

		if (needSyncEvents() && !isServiceModeOn && db.isUpdated()) {
//			int ret = locDB.updateBase();
//			locDB.syncCounters();
			int ret = db.sync(DataBase::SyncEvents);
			if (ret == 0) {
				resetSyncEventsFlag();
				timerForReboot.stop();
				timerForReboot.reset();
			}
			else {
				timerForReboot.start();
			}
		}

		wdg.kick(TASK_NET);

		if (timeAlreadySynched && !db.isUpdated() && !isServiceModeOn) {
//			int ret = locDB.fullUpdateBase();
//			locDB.syncCounters();
			int ret = db.sync(DataBase::SyncAll);
//			int ret = 0;
			if (ret == 0) {
				timerForReboot.stop();
				timerForReboot.reset();
			}
			else {
				timerForReboot.start();
			}
		}

		time_t tim = mtime.get();
		struct tm ts;
		mtime.convert(&ts, tim);
		static int prehour = 0;

		if ( ((prehour == SYNC_ALL_TIME - 1) && (ts.tm_hour == SYNC_ALL_TIME))) {
			if (!isServiceModeOn && db.isUpdated()) {
				int ret = db.sync(DataBase::SyncAll);
				if (ret == 0) {
					timerForReboot.stop();
					timerForReboot.reset();
				}
				else {
					timerForReboot.start();
				}
			}
		}
		prehour = ts.tm_hour;

		if (sdcard.cardError() > 0 && cardDetected()) {
			trySDRecovery();
		}

		if (timerForReboot.read_ms() >= TIME_OF_REBOOT) {

			core_util_critical_section_enter();
			uint32_t devid = settings.data.devID;
			core_util_critical_section_exit();

			if (EzServer.isCableConnected() && locdbInited && devid != 0) {
				LOG("Device offline, PHY reboot...");
				EzServer.reInit();
				timerForReboot.stop();
				timerForReboot.reset();

			}
			else {
				timerForReboot.stop();
				timerForReboot.reset();
			}
		}

		wdg.kick(TASK_NET);

//		if (FW_Mutex.trylock()) {
//			if (EzServer.isHaveFirmwareUpdate() && !isServiceModeOn) {
//				if (EzServer.getFirmware() == 0) {
//					LOG("Firmware updated. Reboot...");
//					rebootMCU();
//				}
//			}
//			FW_Mutex.unlock();
//		}

		if (FW_Mutex.trylock()) {
			if (db.fwUpdate() && !isServiceModeOn) {
				if (EzServer.getFirmware() == 0) {
					db.resetUpdate();
					LOG("BLE firmware update...");
					wdg.kick(TASK_ALL);

					// Ждём пока задача BLE не будет в состоянии ожидания...
//					Thread::State ble_task_state;
//					do {
//						ble_task_state = p_ble_thread->get_state();
//					} while(ble_task_state != Thread::WaitingDelay && ble_task_state != Thread::WaitingInterval);

					// ...и убиваем задачу
					if (p_ble_thread->terminate() == osOK) {
						p_ble_thread->join();
						delete p_ble_thread;

						// Сбрасываем флаги, чтобы нормально отрабатывали светодиоды
						BleAdvertisingOn = false;
						BleConnected = false;

						// Начинаем обновление BLE
						wdg.kick(TASK_ALL);
						if (ble_dfu.dfu_send_images() == 0) {
							wdg.kick(TASK_ALL);
							LOG("Success");
							BLEupdated = true;
							db.sync(DataBase::SyncEmty);//для экшена

							if (ble_dfu.isHaveFw()) {

								LOG("Firmware update, reboot...");
								ble_dfu.crcFw();
							}
							rebootMCU();
						}
						else {
							wdg.kick(TASK_ALL);
							LOG("Fail");
							BLEupdated = false;
						}

						// Запускаем задачу BLE снова
						p_ble_thread = new Thread(BLE_thread, NULL, osPriorityNormal, BLE_TASK_STACK_SIZE);
					}
				}
			}
			FW_Mutex.unlock();
		}

		if (db.isNeedReboot()) {
			LOG("Reboot from server...");
			rebootMCU();
		}

		if (db.isNeedReset()) {
			settings.resetSettings();
			sdcard.printSettingsLog();
			LOG("Settings are reseted from server, Reboot...");
			rebootMCU();
		}

    	Thread::wait(10);

    	wdg.kick(TASK_NET);

//		tim = mtime.get();
//		static time_t pretim = 0;
//		if (tim - pretim >= 20) {
//			pretim = tim;
//			locDB.printfLog(LocalDataBase::LOG_STACK, NULL);
//		}
	}
}

void LOG_thread(void const *args) {

	while(1) {
		osEvent evt = Thread::signal_wait(LOG_UPLOAD, 0);
		if (evt.status == osEventSignal) {
			if (!sdcard.cardError() && cardDetected()) {
				if (EzServer.uploadLogs() == 0) {
					db.resetLogTransmitFlag();
					p_net_thread->signal_set(LOG_UPLOADED);
				}
			}
		}

		evt = sdcardCmdMailbox.get(0);
		if (evt.status == osEventMail) {
			uint8_t cmd = *(uint8_t*)evt.value.p;
			sdcardCmdMailbox.free((uint8_t*)evt.value.p);

			if (cmd == FORMAT_SDCARD_CMD) {
				int ret = sdcard.formatDisk();
				if (ret == 0) {
					ret = sdcard.init();
					if (ret == 0) {
						db.cardFormatted();
						sdcard.printfLog(SdCard::LOG_COMMON, "[SD:Info] SD Card Formated");
					}
				}

				if (ret != 0) {
					sdcard.printfLog(SdCard::LOG_COMMON, "[SD:Error] SD Card Format error");
				}
			}
		}

		Thread::wait(100);
	}
}
