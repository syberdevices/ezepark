/*
 * Dfu.h
 *
 *  Created on: 5 окт. 2017 г.
 *      Author: Denis Shreiber
 */

#ifndef NRF_DFU_DFU_H_
#define NRF_DFU_DFU_H_

#include "DfuTransportSerial.h"

class Dfu {
public:
	Dfu(uint8_t * pdfu_sram);

	ret_code_t dfu_send_images();

	bool isHaveFw();

	void crcFw();

private:
	uint32_t fw_len;
	bool _isHaveFw;
	typedef struct {
		uint8_t * pdat;
		uint8_t * pbin;
		uint32_t  dat_len;
		uint32_t  bin_len;
	}image_t;

	typedef struct {
		image_t sd_bl;
		image_t sd;
		image_t bl;
		image_t app;
	}images_t;

	enum typeFirmware{
		ctrl = 1,
		sd_bl,
		sd,
		bl,
		app
	};

	DfuTransportSerial _dfu_transport;
	images_t _images;
	uint8_t * _pdfu_sram;

	ret_code_t _parse_images();
	ret_code_t _dfu_send_image(image_t * pimage);
};

#endif /* NRF_DFU_DFU_H_ */
