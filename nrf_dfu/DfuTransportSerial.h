/*
 * DfuTransportSerial.h
 *
 *  Created on: 5 окт. 2017 г.
 *      Author: Denis Shreiber
 */

#ifndef NRF_DFU_DFUTRANSPORTSERIAL_H_
#define NRF_DFU_DFUTRANSPORTSERIAL_H_

#include "mbed.h"
#include "slip.h"
#include "crc32/crc32.h"


class DfuAdapter {
public:
	DfuAdapter();

	void open();
	void close();

	ret_code_t send_message(uint8_t * pdata, uint32_t len);
	uint8_t * get_message();

private:
	slip_t _slip;
	uint8_t _buf[128 + 1];
};


class DfuTransportSerial {
public:
	DfuTransportSerial();

	ret_code_t open();
	void close();
	ret_code_t send_init_packet(uint8_t * pdata, uint32_t len);
	ret_code_t send_firmware(uint8_t * pdata, uint32_t len);

private:
	typedef enum OP_CODE {
		OP_CREATE_OBJECT	= 0x01,
		OP_SET_PRN			= 0x02,
		OP_CALC_CHECK_SUM	= 0x03,
		OP_EXECUTE			= 0x04,
		OP_READ_ERROR		= 0x05,
		OP_READ_OBJECT		= 0x06,
		OP_GET_SERIAL_MTU	= 0x07,
		OP_WRITE_OBJECT		= 0x08,
		OP_PING				= 0x09,
		OP_RESPONSE			= 0x60,
	} OpCode_t;

	typedef enum RES_CODE {
		RES_INVALID_CODE				= 0x00,
		RES_SUCCSESS					= 0x01,
		RES_NOT_SUPPORTED				= 0x02,
		RES_INVALID_PARAMETER			= 0x03,
		RES_INSUFFICIENT_RESOURCES		= 0x04,
		RES_INVALID_OBJECTS				= 0x05,
		RES_INVALID_SIGNATURE			= 0x06,
		RES_UNSUPPORTED_TYPE			= 0x07,
		RES_OPERATION_NOT_PERMITTED		= 0x08,
		RES_OPERATION_FAILED			= 0x0A,
		RES_EXTENDED_ERROR				= 0x0B,
    } ResCode_t;

	typedef enum OBJ_TYPE {
		TYPE_COMMAND	= 0x01,
		TYPE_DATA		= 0x02,
	} ObjType_t;

	typedef struct {
		uint32_t max_size;
		uint32_t offset;
		uint32_t crc;
	} SelObjRsp_t;

	typedef struct {
		uint32_t offset;
		uint32_t crc;
	} GetCrcRsp_t;

	DfuAdapter _dfu_adapter;
	CRC32 _crc;
	uint8_t _ping_id;
	uint16_t _prn;
	uint16_t _mtu;

	ret_code_t _set_prn();
	ret_code_t _get_mtu();
	ret_code_t _ping();
	ret_code_t _create_command(uint32_t size);
	ret_code_t _create_data(uint32_t size);
	ret_code_t _create_object(ObjType_t obj_type, uint32_t size);
	GetCrcRsp_t * _calculate_checksum();
	ret_code_t _execute();
	SelObjRsp_t * _select_command();
	SelObjRsp_t * _select_data();
	SelObjRsp_t * _select_object(ObjType_t obj_type);
	GetCrcRsp_t * _get_checksum_response();
	ret_code_t _stream_data(uint8_t * data, uint32_t len, uint32_t crc_in/* = 0*/, uint32_t * crc_out,  uint32_t offset/* = 0*/);
	uint8_t * _get_response(OpCode_t operation);
};

#endif /* NRF_DFU_DFUTRANSPORTSERIAL_H_ */
