/*
 * DfuTransportSerial.cpp
 *
 *  Created on: 5 окт. 2017 г.
 *      Author: Denis Shreiber
 */

#include "DfuTransportSerial.h"

extern "C" {
extern void dfu_mode_enter();
extern void dfu_mode_exit();
extern uint32_t dfu_tx_data(uint8_t *data, uint16_t len);
extern uint32_t dfu_rx_byte(uint8_t *data);
}

//=============================================================

DfuAdapter::DfuAdapter() {
	memset(&_slip, 0, sizeof(slip_t));
	_slip.buffer_len = sizeof(_buf);
}

void DfuAdapter::open() {
	dfu_mode_enter();
}

void DfuAdapter::close() {
	dfu_mode_exit();
}

ret_code_t DfuAdapter::send_message(uint8_t * pdata, uint32_t len) {
	uint32_t encoded_buf_len;
	uint32_t ret = NRF_ERROR_NULL;

	ret = slip_encode(_buf, pdata, len, &encoded_buf_len);
	if (ret == NRF_SUCCESS) {
		ret = dfu_tx_data(_buf, encoded_buf_len);
	}

	return ret;
}

uint8_t * DfuAdapter::get_message() {
	bool finished = false;
	uint8_t * pret = NULL;

	_slip.state = SLIP_STATE_DECODING;
	_slip.p_buffer = _buf;

	while (!finished) {
		uint8_t byte;
		ret_code_t ret = dfu_rx_byte(&byte);
		if (ret == NRF_SUCCESS) {
			ret = slip_decode_add_byte(&_slip, byte);
			switch (ret) {
			case NRF_SUCCESS:
				pret = _slip.p_buffer;
			case NRF_ERROR_NO_MEM:
			case NRF_ERROR_NULL:
				finished = true;
				break;
			default:
				break;
			}
		}
		else {
			break;
		}
	}

	_slip.p_buffer = NULL;
	_slip.current_index = 0;

	return pret;
}

//=============================================================

DfuTransportSerial::DfuTransportSerial() : _ping_id(0), _prn(256), _mtu(0) {

}

ret_code_t DfuTransportSerial::open() {
	_dfu_adapter.open();

	ret_code_t ret = NRF_SUCCESS;

	for(int i = 0; i < 5; i++) {
		ret = _ping();
	}

	if (ret != NRF_SUCCESS) {
		return ret;
	}

	ret = _set_prn();
	if (ret != NRF_SUCCESS) {
		return ret;
	}

	ret = _get_mtu();

	return ret;
}

void DfuTransportSerial::close() {
	_dfu_adapter.close();
}

ret_code_t DfuTransportSerial::send_init_packet(uint8_t * pdata, uint32_t len) {
	SelObjRsp_t * psel_obj_rsp = _select_command();
	bool try_to_recover_ret_val = false;
	uint32_t crc;

	if (psel_obj_rsp) {
		if (len <= psel_obj_rsp->max_size) {
			do {	// try_to_recover
				if (psel_obj_rsp->offset == 0 || psel_obj_rsp->offset > len) {
					// There is no init packet or present init packet is too long
					break;
				}

//				 expected_crc = (binascii.crc32(init_packet[:response['offset']]) & 0xFFFFFFFF)
				uint32_t expected_crc = _crc.get(&pdata[0], psel_obj_rsp->offset);
				if (expected_crc != psel_obj_rsp->crc) {
					// Present init packet is invalid
					break;
				}

				if (len > psel_obj_rsp->offset) {
					// Send missing part
					if (_stream_data(&pdata[psel_obj_rsp->offset], len - psel_obj_rsp->offset, expected_crc, &crc, psel_obj_rsp->offset) != NRF_SUCCESS) {
						break;;
					}
				}

				if (_execute() != NRF_SUCCESS) {
					break;;
				}

				try_to_recover_ret_val = true;
			} while(0);

			if (try_to_recover_ret_val) {
				return NRF_SUCCESS;
			}

			ret_code_t ret;
			ret = _create_command(len);
			if (ret != NRF_SUCCESS) {
				return ret;
			}

			ret = _stream_data(pdata, len, 0, &crc, 0);
			if (ret != NRF_SUCCESS) {
				return ret;
			}

			return _execute();
		}
		else {
//			LOG("Init command is too long");
			return NRF_ERROR_NULL;
		}
	}
	else {
		return NRF_ERROR_NULL;
	}
}

ret_code_t DfuTransportSerial::send_firmware(uint8_t * pdata, uint32_t len) {
	SelObjRsp_t * psel_obj_rsp = _select_data();

	if (psel_obj_rsp) {
		SelObjRsp_t response;
		response.crc = psel_obj_rsp->crc;
		response.offset = psel_obj_rsp->offset;
		response.max_size = psel_obj_rsp->max_size;

		do {	// try_to_recover
			if (response.offset == 0) {
				// Nothing to recover
				break;
			}

			uint32_t expected_crc = _crc.get(&pdata[0], response.offset);
            uint32_t remainder = response.offset % response.max_size;

            if (expected_crc != response.crc) {
            	// Invalid CRC. Remove corrupted data
            	response.offset -= remainder ? remainder : response.max_size;
            	response.crc = _crc.get(&pdata[0], response.offset);
            	break;
            }

            if (remainder && response.offset != len) {
            	// Send rest of the page
            	uint8_t *pdata_to_send = &pdata[response.offset];
            	uint32_t len_to_send = response.max_size - remainder;
            	ret_code_t ret = _stream_data(pdata_to_send, len_to_send, response.crc, &response.crc, response.offset);
            	if (ret == NRF_SUCCESS) {
            		response.offset += len_to_send;
            	}
            	else {
            		// Remove corrupted data
            		response.offset -= remainder;
            		response.crc = _crc.get(&pdata[0], response.offset);
            		break;
            	}
            }

			if (_execute() != NRF_SUCCESS) {
				break;;
			}

//			self._send_event(event_type=DfuEvent.PROGRESS_EVENT, progress=response['offset'])
		} while(0);

		uint32_t step = response.max_size;
		for (uint32_t i = response.offset; i < len; i += step) {
        	uint8_t *pdata_to_send = &pdata[i];
    		if (i + step > len) {
    			step = len - i;
    		}

    		ret_code_t ret = _create_data(step);
			if (ret != NRF_SUCCESS) {
				return ret; ///@todo
			}

			ret = _stream_data(pdata_to_send, step, response.crc, &response.crc, i);
			if (ret != NRF_SUCCESS) {
				return ret; ///@todo
			}

			ret = _execute();
			if (ret != NRF_SUCCESS) {
				return ret; ///@todo
			}

//			self._send_event(event_type=DfuEvent.PROGRESS_EVENT, progress=len(data))
		}
	}
	else {
		return NRF_ERROR_NULL;
	}

	return NRF_SUCCESS;
}


//=============================================================

ret_code_t DfuTransportSerial::_set_prn() {
	uint8_t buf_to_send[1 + sizeof(_prn)];

	buf_to_send[0] = OP_SET_PRN;
	memcpy(&buf_to_send[1], &_prn, sizeof(_prn));

	if (_dfu_adapter.send_message(buf_to_send, sizeof(buf_to_send)) == NRF_SUCCESS) {

		uint8_t * prsp = _get_response(OP_SET_PRN);
		if (prsp == NULL) {
			return NRF_ERROR_NULL;
		}
	}
	else {
		return NRF_ERROR_NULL;
	}

	return NRF_SUCCESS;
}

ret_code_t DfuTransportSerial::_get_mtu() {
	uint8_t buf_to_send[] = {OP_GET_SERIAL_MTU};

	if (_dfu_adapter.send_message(buf_to_send, sizeof(buf_to_send)) == NRF_SUCCESS) {

		uint8_t * prsp = _get_response(OP_GET_SERIAL_MTU);
		if (prsp == NULL) {
			return NRF_ERROR_NULL;
		}
		memcpy(&_mtu, prsp, sizeof(_mtu));
	}
	else {
		return NRF_ERROR_NULL;
	}

	return NRF_SUCCESS;
}

ret_code_t DfuTransportSerial::_ping() {
	_ping_id += 1;

	uint8_t buf_to_send[] = {OP_PING, _ping_id};

	if (_dfu_adapter.send_message(buf_to_send, sizeof(buf_to_send)) == NRF_SUCCESS) {
		uint8_t * prsp = _dfu_adapter.get_message();

		if (prsp == NULL) {
//			"No ping response"
			return NRF_ERROR_NOT_FOUND;
		}
		if (prsp[0] != OP_RESPONSE) {
//			LOG("No Response");
			return NRF_ERROR_NOT_FOUND;
		}
		if (prsp[1] != OP_PING) {
//			LOG("Unexpected Executed OP_CODE");
			return NRF_ERROR_NOT_FOUND;
		}
		if (prsp[2] != RES_SUCCSESS) {
			return NRF_SUCCESS;
		}
		else {
			if (prsp[3] == _ping_id) {
//				Returning an error code is seen as good enough. The bootloader is up and running
				return NRF_SUCCESS;
			}
			else {
				return NRF_ERROR_NOT_FOUND;
			}
		}
	}
	else {
		return NRF_ERROR_INTERNAL;
	}
}

ret_code_t DfuTransportSerial::_create_command(uint32_t size) {
	return _create_object(TYPE_COMMAND, size);
}

ret_code_t DfuTransportSerial::_create_data(uint32_t size) {
	return _create_object(TYPE_DATA, size);
}

ret_code_t DfuTransportSerial::_create_object(ObjType_t obj_type, uint32_t size) {
	uint8_t buf_to_send[2 + sizeof(uint32_t)];

	buf_to_send[0] = OP_CREATE_OBJECT;
	buf_to_send[1] = obj_type;
	memcpy(&buf_to_send[2], &size, sizeof(uint32_t));

	if (_dfu_adapter.send_message(buf_to_send, sizeof(buf_to_send)) == NRF_SUCCESS) {

		uint8_t * prsp = _get_response(OP_CREATE_OBJECT);
		if (prsp == NULL) {
			return NRF_ERROR_NULL;
		}
	}
	else {
		return NRF_ERROR_NULL;
	}

	return NRF_SUCCESS;
}

DfuTransportSerial::GetCrcRsp_t * DfuTransportSerial::_calculate_checksum() {
	static GetCrcRsp_t get_crc_rsp;
	uint8_t msg = OP_CALC_CHECK_SUM;

	if (_dfu_adapter.send_message(&msg, 1) == NRF_SUCCESS) {

		uint8_t * prsp = _get_response(OP_CALC_CHECK_SUM);
		if (prsp != NULL) {
			get_crc_rsp.offset = *(uint32_t*)&prsp[0];
			get_crc_rsp.crc = *(uint32_t*)&prsp[4];
		}
		else {
			return NULL;
		}
	}
	else {
		return NULL;
	}

	return &get_crc_rsp;
}

ret_code_t DfuTransportSerial::_execute() {
	uint8_t msg = OP_EXECUTE;

	if (_dfu_adapter.send_message(&msg, 1) == NRF_SUCCESS) {

		uint8_t * prsp = _get_response(OP_EXECUTE);
		if (prsp == NULL) {
			return NRF_ERROR_NULL;
		}
	}
	else {
		return NRF_ERROR_NULL;
	}

	return NRF_SUCCESS;
}

DfuTransportSerial::SelObjRsp_t * DfuTransportSerial::_select_command() {
	return _select_object(TYPE_COMMAND);
}

DfuTransportSerial::SelObjRsp_t * DfuTransportSerial::_select_data() {
	return _select_object(TYPE_DATA);
}

DfuTransportSerial::SelObjRsp_t * DfuTransportSerial::_select_object(ObjType_t obj_type) {
	static SelObjRsp_t sel_obj_rsp;
	uint8_t send_buf[] = {OP_READ_OBJECT, obj_type};

	ret_code_t ret = _dfu_adapter.send_message(send_buf, sizeof(send_buf));

	if (ret == NRF_SUCCESS) {
		uint8_t * prsp = _get_response(OP_READ_OBJECT);
		if (prsp != NULL) {
			sel_obj_rsp.max_size = *(uint32_t*)&prsp[0];
			sel_obj_rsp.offset = *(uint32_t*)&prsp[4];
			sel_obj_rsp.crc = *(uint32_t*)&prsp[8];
		}
		else {
			return NULL;
		}
	}
	else {
		return NULL;
	}

	return &sel_obj_rsp;
}

DfuTransportSerial::GetCrcRsp_t * DfuTransportSerial::_get_checksum_response() {
	static GetCrcRsp_t get_crc_rsp;

	uint8_t * prsp = _get_response(OP_CALC_CHECK_SUM);
	if (prsp != NULL) {
		get_crc_rsp.offset = *(uint32_t*)&prsp[0];
		get_crc_rsp.crc = *(uint32_t*)&prsp[4];
	}
	else {
		return NULL;
	}

	return &get_crc_rsp;
}

ret_code_t DfuTransportSerial::_stream_data(uint8_t * data, uint32_t len, uint32_t crc_in, uint32_t * crc_out, uint32_t offset) {
	ret_code_t ret;
	uint32_t current_prn = 0;
	uint32_t step = (_mtu - 1)/2 - 1;
	uint32_t crc;
	GetCrcRsp_t * get_crc_rsp = NULL;
//	if (crc_in == 0) {
		crc_in ^= -1 ;
//	}
	_crc.set_current_process_crc(crc_in);

	uint8_t buf_to_send[step + 1];
	buf_to_send[0] = OP_WRITE_OBJECT;

	for (uint32_t i = 0; i < len; i += step) {
		if (i + step > len) {
			step = len - i;
		}

		memcpy(&buf_to_send[1], &data[i], step);
		ret = _dfu_adapter.send_message(buf_to_send, step + 1);
		if (ret != NRF_SUCCESS) {
			return ret;
		}

//		crc     = binascii.crc32(to_transmit[1:], crc) & 0xFFFFFFFF
		crc = _crc.process(&data[i], step);
		offset += step;
		current_prn += 1;
		if (_prn == current_prn) {
			current_prn = 0;
			get_crc_rsp = _get_checksum_response();
			crc = _crc.complete();
			if (get_crc_rsp) {
				if (crc != get_crc_rsp->crc || offset != get_crc_rsp->offset) {
					return NRF_ERROR_INVALID_DATA;
				}
			}
			else {
				return NRF_ERROR_NULL;
			}

			if (i + step == len) {
				*crc_out = crc;
				return NRF_SUCCESS;
			}
		}
	}

	get_crc_rsp = _calculate_checksum();
	crc = _crc.complete();
//	*crc_out = _crc.get(data, len);
	if (crc != get_crc_rsp->crc || offset != get_crc_rsp->offset) {
		return NRF_ERROR_INVALID_DATA;
	}

	*crc_out = crc;
	return NRF_SUCCESS;
}

uint8_t * DfuTransportSerial::_get_response(OpCode_t operation) {
	uint8_t * prsp = _dfu_adapter.get_message();

	if (prsp == NULL) {
		return NULL;
	}
	if (prsp[0] != OP_RESPONSE) {
//		LOG("No Response");
		return NULL;
	}
	if (prsp[1] != operation) {
//		LOG("Unexpected Executed OP_CODE");
		return NULL;
	}
	if (prsp[2] == RES_SUCCSESS) {
		return &prsp[3];
	}
	else {
		return NULL;
	}
}



