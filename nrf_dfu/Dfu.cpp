/*
 * Dfu.cpp
 *
 *  Created on: 5 окт. 2017 г.
 *      Author: Denis Shreiber
 */

#include "Dfu.h"
#include <Watchdog/Watchdog.h>
#include <common.h>

extern Watchdog wdg;

//===================================================================

Dfu::Dfu(uint8_t * pdfu_sram) : _pdfu_sram(pdfu_sram) {
	memset(&_images, 0, sizeof(_images));
	fw_len = 0;
	_isHaveFw = false;
}

ret_code_t Dfu::dfu_send_images() {
	ret_code_t ret = _parse_images();
	if (ret != NRF_SUCCESS) {
		return ret;
	}

#define IS_HAVE_IMAGE(image)	( _images.image.pbin && _images.image.bin_len && _images.image.pdat && _images.image.dat_len )

	if (IS_HAVE_IMAGE(sd_bl)) {
		ret = _dfu_send_image(&_images.sd_bl);
		if (ret != NRF_SUCCESS) {
			return ret;
		}
		wdg.kick(TASK_ALL);
	}
	if (IS_HAVE_IMAGE(sd)) {
		ret = _dfu_send_image(&_images.sd);
		if (ret != NRF_SUCCESS) {
			return ret;
		}
		wdg.kick(TASK_ALL);
	}
	if (IS_HAVE_IMAGE(bl)) {
		ret = _dfu_send_image(&_images.bl);
		if (ret != NRF_SUCCESS) {
			return ret;
		}
		wdg.kick(TASK_ALL);
	}
	if (IS_HAVE_IMAGE(app)) {
		ret = _dfu_send_image(&_images.app);
		if (ret != NRF_SUCCESS) {
			return ret;
		}
		wdg.kick(TASK_ALL);
	}

	return NRF_SUCCESS;
}

//===================================================================

#include <SDFileSystem/SDFileSystem.h>

ret_code_t Dfu::_parse_images() {
	uint8_t type = 0;
	uint32_t bin_len = 0;
	uint32_t dat_len = 0;
	uint8_t * pbin = NULL;
	uint8_t * pdat = NULL;
	uint8_t *_pdfu = _pdfu_sram + 4;	// Первые 4 байта - общий размер данных
	uint8_t numberOfFiles  = *_pdfu;
	_pdfu++;

	if (numberOfFiles > 3) {
		return NRF_ERROR_INVALID_DATA;
	}

	for(; numberOfFiles > 0; numberOfFiles--) {
		type = *_pdfu;
		_pdfu++;

		if (type == ctrl) {
			memcpy(&fw_len, _pdfu, 4);
			_pdfu = _pdfu + 4 + fw_len;
			_isHaveFw = true;
			continue;
		}

		memcpy(&dat_len, _pdfu, 4);
		if (dat_len > 256) {
			return NRF_ERROR_INVALID_DATA;
		}
		_pdfu += 4;

		pdat = _pdfu;
		_pdfu += dat_len;

		memcpy(&bin_len, _pdfu, 4);
		if (bin_len > 500*1024) {
			return NRF_ERROR_INVALID_DATA;
		}
		_pdfu += 4;

		pbin = _pdfu;
		_pdfu += bin_len;

		image_t * pimage = NULL;

		switch (type) {
			case sd_bl: pimage = &_images.sd_bl; break;
			case sd:    pimage = &_images.sd; break;
			case bl:    pimage = &_images.bl; break;
			case app:   pimage = &_images.app; break;
			default:
				break;
		}

		pimage->dat_len = dat_len;
		pimage->pdat = pdat;
		pimage->bin_len = bin_len;
		pimage->pbin = pbin;
	}

	return NRF_SUCCESS;
}

bool Dfu::isHaveFw() {
	return _isHaveFw;
}

void Dfu::crcFw() {
	CRC32 crcObj;
	uint32_t crc_loc = crcObj.get(_pdfu_sram + 10, fw_len);
	memcpy(_pdfu_sram + 10 + fw_len, &crc_loc, 4);
}

ret_code_t Dfu::_dfu_send_image(image_t * pimage) {
	ret_code_t ret = _dfu_transport.open();
	if (ret != NRF_SUCCESS) {
		return ret;
	}

	ret = _dfu_transport.send_init_packet(pimage->pdat, pimage->dat_len);
	if (ret != NRF_SUCCESS) {
		return ret;
	}

	ret = _dfu_transport.send_firmware(pimage->pbin, pimage->bin_len);
	if (ret != NRF_SUCCESS) {
		return ret;
	}

	_dfu_transport.close();

	memset(pimage, 0, sizeof(image_t));

	return ret;
}


