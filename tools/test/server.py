import socket
import signal
import sys
 
 
def signal_handler(signal, frame):
    print ('You pressed Ctrl+C!')
    s.close()
    sys.exit(0)
 
signal.signal(signal.SIGINT, signal_handler)
 
ECHO_SERVER_ADDRESS = "192.168.192.74"
ECHO_PORT = 50007
message = 'Hello, world'
 
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
s.connect((ECHO_SERVER_ADDRESS, ECHO_PORT))
 
print (('Sending'), repr(message))
s.sendall(message.encode('utf-8'))
data = s.recv(1024)
s.close()
print (('Received'), repr(data))
