import socket
import signal
import sys
 
 
def signal_handler(signal, frame):
    print ('You pressed Ctrl+C!')
    s.close()
    sys.exit(0)
 
signal.signal(signal.SIGINT, signal_handler)
 
print (('Server Running at '), socket.gethostbyname(socket.gethostname()))
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('', 50007))
s.listen(1)
 
while True:
    conn, addr = s.accept()
    print (('Connected by'), addr)
    while True:
        data = conn.recv(1024)
        print (('Received data '), (data))
        if not data: break
        conn.sendall(data)
    conn.close()
