#! /usr/bin/env python
# -*- coding: utf-8 -*-

import re
import string

path_ver = "../version/version.txt"
path_build = "../version/build.txt"

file = open(path_build)
strs = file.read()
file.close()

version = int(strs)
minor = str(version % 256)
major = str(int(version / 256) % 256)
environment = "'d'"

file = open(path_ver, "w")
file.write("/*---Do not edit!---*/\n")
file.write(environment + "," + major + "," + minor)
file.close()

print ("Environment:\t" + environment)
print ("Major:\t" + major)
print ("Minor:\t" + minor)

