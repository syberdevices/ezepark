#! /usr/bin/env python
# -*- coding: utf-8 -*-

import re
import string

#===========================================
#===== Increment version ===================

path_build = "../version/build.txt"

file = open(path_build)
ver = file.read()
file.close()
ver = str(int(ver) + 1)

file = open(path_build, "w")
file.write(ver)
file.close();

print ("Version autoincrement success!")

#===========================================
#===== Make version ========================

path_ver = "../version/version.txt"

file = open(path_build)
strs = file.read()
file.close()

version = int(strs)
minor = str(version % 256)
major = str(int(version / 256) % 256)
environment = "'d'"

file = open(path_ver, "w")
file.write("/*---Do not edit!---*/\n")
file.write(environment + "," + major + "," + minor)
file.close()

print ("Environment:\t" + environment)
print ("Major:\t" + major)
print ("Minor:\t" + minor)

