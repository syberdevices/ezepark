/*
 * Watchdog.h
 *
 *  Created on: 5 дек. 2016 г.
 *      Author: Chuyec
 */

#ifndef WATCHDOG_H_
#define WATCHDOG_H_

#include "mbed.h"

class Watchdog {
public:
	Watchdog(uint8_t tasks_num, uint32_t ms);

	void kick(uint32_t task);

	const char *getResetReason();

private:
	IWDG_HandleTypeDef _hiwdg;
	uint8_t _tasks_num;
	uint32_t _mask;
	uint32_t _rstflags;
	const char *_rstReason;
	uint8_t _rstFlags;

	void kick();
};

#endif /* WATCHDOG_H_ */
