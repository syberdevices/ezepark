/*
 * Watchdog.cpp
 *
 *  Created on: 5 дек. 2016 г.
 *      Author: Chuyec
 */

#include "Watchdog.h"

// ====================================================================

Watchdog::Watchdog(uint8_t tasks_num, uint32_t ms) : _tasks_num(tasks_num) {
	if (__HAL_RCC_GET_FLAG(RCC_FLAG_PORRST)) {
		_rstReason = "PORRST";
	}
	else if (__HAL_RCC_GET_FLAG(RCC_FLAG_SFTRST)) {
		_rstReason = "SFTRST";
	}
	else if (__HAL_RCC_GET_FLAG(RCC_FLAG_LPWRRST)) {
		_rstReason = "LPWRRST";
	}
	else if (__HAL_RCC_GET_FLAG(RCC_FLAG_BORRST)) {
		_rstReason = "BORRST";
	}
	else if (__HAL_RCC_GET_FLAG(RCC_FLAG_WWDGRST)) {
		_rstReason = "WWDGRST";
	}
	else if (__HAL_RCC_GET_FLAG(RCC_FLAG_IWDGRST)) {
		_rstReason = "IWDGRST";
	}
	else if (__HAL_RCC_GET_FLAG(RCC_FLAG_PINRST)) {
		_rstReason = "PINRST";
	}
	else {
		_rstReason = "UNKNOWNRST";
	}

	_rstFlags = (uint8_t)(RCC->CSR >> 25);

	/*
	 * SFTRST: 	биты SFTRST и PINRST
	 * PINRST: 	бит PINRST
	 * IWDGRST: бит IWDGRST и PINRST
	 * PORRST:  биты: PORRST, BORRST, PINRST
	 */

	__HAL_RCC_CLEAR_RESET_FLAGS();


	_mask = (uint32_t)-1 << tasks_num;

	_hiwdg.Instance = IWDG;
	_hiwdg.Init.Prescaler = IWDG_PRESCALER_256;

	uint32_t reload = (ms * LSI_VALUE / 1000) / 256;
	_hiwdg.Init.Reload = reload < 0xFFF ? reload : 0xFFF;

	HAL_IWDG_Init(&_hiwdg);
	__HAL_DBGMCU_FREEZE_IWDG();
}

void Watchdog::kick(uint32_t task) {
	_mask |= task;

	if (_mask == (uint32_t)-1) {
		_mask <<= _tasks_num;
		kick();
	}
}

const char *Watchdog::getResetReason() {
	return _rstReason;
}

// ====================================================================

void Watchdog::kick() {
	__HAL_IWDG_RELOAD_COUNTER(&_hiwdg);
}

