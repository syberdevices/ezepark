/*
 * DataBase.h
 *
 *  Created on: 5 сент. 2016 г.
 *      Author: Denis
 */

#ifndef DATABASE_H_
#define DATABASE_H_

#include <mbed.h>
#include <rtos.h>

#define MAX_EVENTS_PER_TRANSACTION			10

#define J_REQ_FIRMWARE		"reqFirmware"
#define J_CURR_FIRMWARE		"currFirmware"
#define J_TEMPERATURE 		"temperature"
#define J_CODE				"code"
#define J_TIME				"time"
#define J_TOKEN				"token"
#define J_LASTSYNCTIME		"lastSyncTime"
#define J_EVENTS			"events"
#define J_UUID				"uuid"
#define J_MAC				"mac"
#define J_SYNCTIME			"syncTime"
#define J_CONTROLLER		"controller"
#define J_TYPE				"type"
#define J_SETTINGS			"settings"
#define J_MINBALANCE		"minBalance"
#define J_SESSIONS			"sessions"
#define J_ISACTIVE			"isActive"
#define J_BILLING			"billing"
#define J_USERS				"users"
#define J_ID				"id"
#define J_BALANCE			"balance"
#define J_TYPE				"type"
#define J_UUIDS				"UUIDs"
#define J_MOBILE			"mobile"
#define J_HARDWARE			"hardware"
#define J_ENTER				"ENTER"
#define J_EXIT				"EXIT"
#define J_IS_REBOOTED		"isRebooted"
#define J_BOOT_REASON		"bootReason"
#define J_IS_RESETED		"isReseted"
#define J_CARD_HEALTH		"cardHealth"
#define J_CARD_FORMATTED	"cardFormatted"
#define J_ACTIONS			"actions"
#define J_ACTIONS_TYPE		"type"
#define	J_ACTIONS_MESSAGE	"message"
#define J_SETTINGS			"settings"

class DataBase {
public:
	typedef enum {
		BALANCE_OK,
		NO_BALANCE,
		NO_SESSION,
		NO_USER,
		NO_UUID,
		GATE_BUSY,
		CORPORATE_USER,
		RESERVED_USER,
		NO_PLACES,
		SESSION_IS_OPENED,
	}BillingStatus_t;

	DataBase();

	enum SyncMode {
		SyncAll,
		SyncEvents,
		SyncEmty,
	};

	int sync(SyncMode sync);

	int readData(char* buf, size_t len, size_t* pReadLen);

	BillingStatus_t checkBilling(uint8_t *uuid, uint32_t id, BillingStatus_t *sessionStatus);

	BillingStatus_t checkBillingBeacon(uint8_t* mac, BillingStatus_t *sessionStatus);

	BillingStatus_t checkBillingExit(uint8_t *uuid, uint32_t id);

	BillingStatus_t checkBillingExitBeacon(uint8_t* mac);

	void writeEvent(uint8_t *uuid, time_t tm, bool isBeacon);

	bool isUpdated();

	void updated();

	void resetUpdate();

	void cardFormatted();

	char* getCurrFirmwareVer();

	char* getReqFirmwareVer();

	bool isNeedReboot();

	bool isNeedReset();

	bool isNeedLogTransmit();

	void resetLogTransmitFlag();

	time_t getSyncTime();

	bool isDeviceOff();

	void decrementOfGeneral();

	void decrementOfCorporate();

	void decrementOfReserved();

	void syncCounters();

	bool allowReservedSpaces();

	bool backupEvents();

	int updateBase(const char* buf, size_t len);

	void updateUsers();

	bool checkEvents();

	void resetDataSync();

	bool fwUpdate();

private:
	typedef enum {
		USER_GENERAL = BALANCE_OK,
		USER_CORPORATE = CORPORATE_USER,
		USER_RESERVED = RESERVED_USER,

		USER_UNKNOWN = USER_GENERAL + USER_CORPORATE + USER_RESERVED,
	}UserType_t;

	Mutex _mutex;

	char jdata[2048];

	SyncMode _syncMode;
	bool _updated;
	bool _isRebooted;
	bool _isReseted;
	bool _cardFormatted;
	bool _cardHealth;

	time_t _syncTime;

	int _reserved;
	int _corporate;
	int _general;
	int _generalLocal;
	int _corporateLocal;
	int _reservedLocal;

	bool _allowReservedSpaces;

	bool _needReboot;
	bool _needReset;
	bool _needLogTransmit;

	bool _fwUpdate;

	bool _isDeviceOff;

	bool _endParseSize;

	bool _endParseJson;

	bool _endParseUsers;

	bool _endParseBeacons;

	bool _endParseSessions;

	uint32_t _transmittedEvents;
	bool _endSettings;
	bool _endActions;
	bool _endEvents;

	uint32_t sizeOfJson;

	uint32_t sizeOfusers;

	uint32_t sizeOfbeacons;

	uint32_t sizeOfsessions;

	uint32_t stayWrite;

	uint32_t _beacons;

	BillingStatus_t finishBilling(UserType_t usertype, BillingStatus_t sessionstatus, char billing, bool isBeacon);

	BillingStatus_t findSession(uint32_t userid);

	BillingStatus_t binBilling(uint8_t *uuid, uint32_t id, BillingStatus_t *sessionStatus, bool isBeacon);

	bool findEvent(uint8_t *uuid, bool isBeacon);

	void convertUuid2Str(char* to, uint8_t* from);

	void convertMac2Str(char* to, uint8_t* from);

	int parseJson();
};

extern DataBase db;

#endif /* DATABASE_H_ */
