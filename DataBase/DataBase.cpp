/*
 * DataBase.cpp
 *
 *  Created on: 5 сент. 2016 г.
 *      Author: Denis
 */

#include <rtos.h>
#include <EzeparkServer/EzeparkServer.h>
#include <MyTime/MyTime.h>
#include <Settings/Settings.h>
#include <common.h>
#include <Watchdog/Watchdog.h>
#include <ctype.h>

#include <DataBase/DataBase.h>
#include <SdCard/SdCard.h>

#define ALL_USERS_ARRAY_LEN				100000
#define ALL_BEACONS_ARRAY_LEN			20000
#define ALL_SESSIONS_ARRAY_LEN			1000
#define USERS_EVENTS_ARRAY_LEN			50
#define ALL_BEACONS_EVENTS_ARRAY_LEN	20
#define ALL_USERS_EVENTS_ARRAY_LEN		50

static const uint8_t curFirmwareVer[3] = {
#include <version/version.txt>
};

extern Watchdog wdg;
extern Settings<Sett_t> settings;
extern Mail<int8_t, 1> TemperatureMailbox;
extern Mail<uint8_t, 1> sdcardCmdMailbox;
extern osThreadId mainThreadID;
extern bool cardDetected();
extern void shutdownCMD();
extern void rebootMCU();

extern bool BLEupdated;

static int8_t temperature = -127;

__ESRAM users_t allUsers[ALL_USERS_ARRAY_LEN];
__ESRAM beacons_t allBeacons[ALL_BEACONS_ARRAY_LEN];
__ESRAM sessions_t allSessions[ALL_SESSIONS_ARRAY_LEN];
__CCM events_t usersEvents[USERS_EVENTS_ARRAY_LEN];
__CCM beacons_t allBeaconsEvents[ALL_BEACONS_EVENTS_ARRAY_LEN];
__CCM usersEvents_t allUsersEvents[ALL_USERS_EVENTS_ARRAY_LEN];
__CCM uint32_t _numbEvents;

static char currFirmware[16];
static char reqFirmware[16];

DataBase::DataBase(): _updated(false), _isRebooted(true), _isReseted(true), _cardFormatted(false), _cardHealth(true), _syncTime(0), _allowReservedSpaces(true), _needReboot(false), _needReset(false), _needLogTransmit(true), _fwUpdate(false), _isDeviceOff(false), _endParseSize(false), _endParseJson(false), _endParseUsers(false), _endParseBeacons(false), _endParseSessions(false), _transmittedEvents(0), _endSettings(false), _endActions(false), _endEvents(false), _beacons(0) {
	const char *templ = isdigit(curFirmwareVer[0]) ? "%u.%u.%u" : "%c.%u.%u";

	sprintf(currFirmware, templ, curFirmwareVer[0], curFirmwareVer[1], curFirmwareVer[2]);
	strcpy(reqFirmware, "none");
}

int DataBase::readData(char* buf, size_t len, size_t* pReadLen) {
//	char *pbuf = buf;
	*pReadLen = 0;

	wdg.kick(TASK_NET);

	if(!_endSettings) {
		core_util_critical_section_enter();
		uint32_t devid = settings.data.devID;
		core_util_critical_section_exit();

		uint32_t tm = mtime.get();

		osEvent evt = TemperatureMailbox.get(0);
		if (evt.status == osEventMail) {
			int8_t *mail = (int8_t*)evt.value.p;
			temperature = *mail;
			TemperatureMailbox.free(mail);
		}

		strcpy(buf, "{\"");

		strcat(buf, J_CURR_FIRMWARE);
		strcat(buf, "\":\"");
		strcat(buf, getCurrFirmwareVer());
		strcat(buf, "\",\"");

		strcat(buf, J_TEMPERATURE);
		strcat(buf, "\":\"");
		sprintf(buf + strlen(buf), "%d", temperature);
		strcat(buf, "\",\"");

		strcat(buf, J_CODE);
		strcat(buf, "\":\"");
		sprintf(buf + strlen(buf), "%u", (unsigned int)devid);
		strcat(buf, "\",\"");

		strcat(buf, J_TIME);
		strcat(buf, "\":");
		sprintf(buf + strlen(buf), "%u", (unsigned int)tm);
		strcat(buf, ",\"");

		strcat(buf, J_TOKEN);
		strcat(buf, "\":\"");
		sprintf(buf + strlen(buf), "%u", (unsigned int)EzServer.token(devid, tm));
		strcat(buf, "\",\"");

		if (_syncMode == SyncEvents) {
			strcat(buf, J_LASTSYNCTIME);
			strcat(buf, "\":");
			sprintf(buf + strlen(buf), "%u", (unsigned int)getSyncTime());
			strcat(buf, ",\"");
		}

		strcat(buf, J_SETTINGS);
		strcat(buf, "\":{\"");
		strcat(buf, "netMode");
		strcat(buf, "\":\"");

		core_util_critical_section_enter();
		if (settings.data.net_mode == NET_MODE_DHCP) {
			strcat(buf, "DHCP");
		}
		else if (settings.data.net_mode == NET_MODE_STATIC) {
			strcat(buf, "Static");
		}
		core_util_critical_section_exit();

		strcat(buf, "\",\"");

		strcat(buf, "IP");
		strcat(buf, "\":\"");
		strcat(buf, EzServer.getIPstr());
		strcat(buf, "\",\"");

		strcat(buf, "mask");
		strcat(buf, "\":\"");
		strcat(buf, EzServer.getMaskStr());
		strcat(buf, "\",\"");

		strcat(buf, "gateway");
		strcat(buf, "\":\"");
		strcat(buf, EzServer.getGatewayStr());
		strcat(buf, "\",\"");

		strcat(buf, "DNS1");
		strcat(buf, "\":\"");
		strcat(buf, EzServer.getDNS1Str());
		strcat(buf, "\",\"");

		strcat(buf, "DNS2");
		strcat(buf, "\":\"");
		strcat(buf, EzServer.getDNS2Str());
		strcat(buf, "\",\"");

		strcat(buf, "netTimeout");
		strcat(buf, "\":");

		core_util_critical_section_enter();
		sprintf(buf + strlen(buf), "%u", settings.data.con_timeout);
		core_util_critical_section_exit();

		strcat(buf, ",\"");

		strcat(buf, "mac");
		strcat(buf, "\":\"");

		core_util_critical_section_enter();
		sprintf(buf + strlen(buf), "%02X:%02X:%02X:%02X:%02X:%02X", settings.data.mac[0], settings.data.mac[1], settings.data.mac[2], settings.data.mac[3], settings.data.mac[4], settings.data.mac[5]);
		core_util_critical_section_exit();

		strcat(buf, "\",\"");

		strcat(buf, "devID");
		strcat(buf, "\":");
		sprintf(buf + strlen(buf), "%u", (unsigned int)devid);
		strcat(buf, ",\"");

		strcat(buf, "devNumber");
		strcat(buf, "\":");

		core_util_critical_section_enter();
		sprintf(buf + strlen(buf), "%u", settings.data.dev_number);
		core_util_critical_section_exit();

		strcat(buf, ",\"");

		strcat(buf, "tx_pwr");
		strcat(buf, "\":");

		core_util_critical_section_enter();
		sprintf(buf + strlen(buf), "%u", settings.data.tx_pwr);
		core_util_critical_section_exit();

		strcat(buf, ",\"");

		strcat(buf, "beac_trh");
		strcat(buf, "\":");

		core_util_critical_section_enter();
		sprintf(buf + strlen(buf), "%d", settings.data.beac_trh);
		core_util_critical_section_exit();

		strcat(buf, ",\"");

		strcat(buf, "enableLogs");
		strcat(buf, "\":");

		core_util_critical_section_enter();
		if (settings.data.logs_mode) {
			strcat(buf, "true");
		}
		else {
			strcat(buf, "false");
		}
		core_util_critical_section_exit();

		strcat(buf, ",\"");

		strcat(buf, "enableSensor");
		strcat(buf, "\":");

		core_util_critical_section_enter();
		if (settings.data.enable_dist_sensor) {
			strcat(buf, "true");
		}
		else {
			strcat(buf, "false");
		}
		core_util_critical_section_exit();

		strcat(buf, ",\"");

		strcat(buf, "enableFeedback");
		strcat(buf, "\":");

		core_util_critical_section_enter();
		if (settings.data.enable_gate_feedback) {
			strcat(buf, "true");
		}
		else {
			strcat(buf, "false");
		}
		core_util_critical_section_exit();

		strcat(buf, ",\"");

		strcat(buf, "sensorDistance");
		strcat(buf, "\":");

		core_util_critical_section_enter();
		float ftmp = ((float)settings.data.dist_sensor_threshold)/100;
		core_util_critical_section_exit();
		sprintf(buf + strlen(buf), "\"%.1f\"", ftmp);

		strcat(buf, "},\"");

		*pReadLen = strlen(buf);
		_endSettings = true;
		goto exit;
	}
	if (!_endActions) {
		strcpy(buf, J_ACTIONS);
		strcat(buf, "\":[");

		_cardHealth = sdcard.cardError() == 0 ? true : false;

		if (fwUpdate()) {
			strcat(buf, "{\"");
			strcat(buf, J_ACTIONS_TYPE);
			strcat(buf, "\":\"");
			strcat(buf, "BleUpdated\",\"");

			strcat(buf, J_ACTIONS_MESSAGE);
			strcat(buf, "\":\"");
			if (BLEupdated) {
				strcat(buf, "Success");
			}
			else {
				strcat(buf, "Fail");
			}
			strcat(buf, "\"}");
			if (_isRebooted || _isReseted || !_cardHealth || !cardDetected() || _cardFormatted) {
				strcat(buf, ",");
			}
		}

		if (_isRebooted) {
			strcat(buf, "{\"");
			strcat(buf, J_ACTIONS_TYPE);
			strcat(buf, "\":\"");
			strcat(buf, "Rebooted\",\"");

			strcat(buf, J_ACTIONS_MESSAGE);
			strcat(buf, "\":\"");
			strcat(buf, wdg.getResetReason());
			strcat(buf, "\"}");
			if (_isReseted || !_cardHealth || !cardDetected() || _cardFormatted) {
				strcat(buf, ",");
			}
		}

		if (_isReseted) {
			strcat(buf, "{\"");
			strcat(buf, J_ACTIONS_TYPE);
			strcat(buf, "\":\"");
			strcat(buf, "Reseted\",\"");

			strcat(buf, J_ACTIONS_MESSAGE);
			strcat(buf, "\":\"");
			strcat(buf, "\"}");
			if (!_cardHealth || !cardDetected() || _cardFormatted) {
				strcat(buf, ",");
			}
		}

		if (!cardDetected()) {
			strcat(buf, "{\"");
			strcat(buf, J_ACTIONS_TYPE);
			strcat(buf, "\":\"");
			strcat(buf, "Card\",\"");

			strcat(buf, J_ACTIONS_MESSAGE);
			strcat(buf, "\":\"");
			strcat(buf, "Absent");
			strcat(buf, "\"}");
		}
		else if (_cardFormatted) {
			strcat(buf, "{\"");
			strcat(buf, J_ACTIONS_TYPE);
			strcat(buf, "\":\"");
			strcat(buf, "Card\",\"");

			strcat(buf, J_ACTIONS_MESSAGE);
			strcat(buf, "\":\"");
			strcat(buf, "Formatted");
			strcat(buf, "\"}");
		}
		else if (!_cardHealth) {
			strcat(buf, "{\"");
			strcat(buf, J_ACTIONS_TYPE);
			strcat(buf, "\":\"");
			strcat(buf, "Card\",\"");

			strcat(buf, J_ACTIONS_MESSAGE);
			strcat(buf, "\":\"");
			strcat(buf, "Died");
			strcat(buf, "\"}");
		}

		strcat(buf, "]");
		if (_syncMode == SyncEvents) {
			strcat(buf, ",\"");

			strcat(buf, J_EVENTS);
			strcat(buf, "\":[");
		}
		else {
			strcat(buf, "}");
			_endEvents = true;
		}
		_endActions = true;
		*pReadLen = strlen(buf);
		goto exit;
	}
	if (!_endEvents) {
		buf[0] = '\0';
			while (1) {
				PRINTF("Tx event... %d\n", (int)_transmittedEvents);
				if (_transmittedEvents >= _numbEvents) {
					int last = strlen(buf) - 1;
					if (buf[last] == ',') {	// Если уже успели поставить запятую
						buf[last] = '\0';
					}
					_endEvents = true;
					break;
				}
				else if (_transmittedEvents % MAX_EVENTS_PER_TRANSACTION == 0 && buf[0] != '\0') {
					break;
				}
				_mutex.lock();
				strcat(buf, "{\"");
				strcat(buf, J_TIME);
				strcat(buf, "\":");
				sprintf(buf + strlen(buf), "%u", (unsigned int)usersEvents[_transmittedEvents].tm);
				strcat(buf, ",\"");
				if (usersEvents[_transmittedEvents].isUser) {
					if (usersEvents[_transmittedEvents].isBeacon) {
						strcat(buf, J_MAC);
						strcat(buf, "\":\"");
						convertMac2Str(buf + strlen(buf), usersEvents[_transmittedEvents].uuid);
					}
					else {
						strcat(buf, J_UUID);
						strcat(buf, "\":\"");
						convertUuid2Str(buf + strlen(buf), usersEvents[_transmittedEvents].uuid);
					}
				}
				else {
					strcat(buf, J_UUID);
					strcat(buf, "\":\"");
				}

				strcat(buf, "\"}");
				_mutex.unlock();

				strcat(buf, ",");

				_transmittedEvents++;
			}

			if (_endEvents) {
				strcat(buf, "]");
				strcat(buf, "}");
			}

		*pReadLen = strlen(buf);
	}

	exit:

	return 0;
}

void DataBase::resetDataSync() {
	_transmittedEvents = 0;
	_endSettings = false;
	_endActions = false;
	_endEvents = false;
	_endParseBeacons = false;
	_endParseJson = false;
	_endParseSessions = false;
	_endParseUsers = false;
	_endParseSize = false;
}

int DataBase::sync(SyncMode sync) {
	EzServer.isBusyNet(true);

	core_util_critical_section_enter();
	uint32_t devid = settings.data.devID;
	core_util_critical_section_exit();

	if (devid == 0) {
		return -1;		// Не делаем синк, если дефолтное значение
	}

	_syncMode = sync;

	int ret = EzServer.sync(_syncMode);

	if (ret == 0) {
		_isRebooted = false;
		_isReseted = false;
		_cardFormatted = false;
		BLEupdated = false;

		if (sync != SyncEmty) {
			if (sync == SyncEvents) {
				if (_numbEvents != 0) {
					PRINTF("Events synched\n");
				}

				_mutex.lock();
				_numbEvents = _numbEvents - _transmittedEvents;
				_mutex.unlock();
			}

			sdcard.printfLog(SdCard::LOG_COMMON, "[DB:UPDATE]:%s", "Start");

			updateUsers();
			if (ret == 0 || isNeedReboot() || isNeedReset()) {
				sdcard.printfLog(SdCard::LOG_COMMON, "[DB:UPDATE]:%s", "Success");
			}
			else {
				sdcard.printfLog(SdCard::LOG_COMMON, "[DB:UPDATE]:%s", "Error");
			}
		}
	}
//	else if (sync == SyncEvents) {
////		locDB.unmarkEvents();
//	}

	resetDataSync();

	if (ret == 0 && sync != SyncEmty) {
		if (sync == SyncAll) {
			core_util_critical_section_enter();
			_updated = true;
			core_util_critical_section_exit();
		}

		db.syncCounters();
	}

	EzServer.isBusyNet(false);

	return ret;
}

int DataBase::parseJson() {
	int ret = 0;
	char *buf = NULL;
	union {
		uint8_t netSet[4];
		uint32_t tmpNetSet;
	};
	uint8_t mac[6];
	int8_t beac_trh = 0;
	float dist;
	bool settingsChanged = false;
	bool netSettingsChanged = false;

	buf = strstr(jdata, "\"netMode\":\"");
	if (buf) {
		buf = buf + strlen("\"netMode\":\"");
		if (memcmp(buf, "Static", 6) == 0) {
			core_util_critical_section_enter();
			if(settings.data.net_mode == NET_MODE_DHCP) {
				settings.data.net_mode = NET_MODE_STATIC;
				settingsChanged = true;
				netSettingsChanged = true;
			}
			core_util_critical_section_exit();
		}
		else if (memcmp(buf, "DHCP", 4) == 0) {
			core_util_critical_section_enter();
			if(settings.data.net_mode == NET_MODE_STATIC) {
				settings.data.net_mode = NET_MODE_DHCP;
				settingsChanged = true;
				netSettingsChanged = true;
			}
			core_util_critical_section_exit();
		}
		else {
			ret = -1;
			goto exit;
		}
	}
	buf = strstr(jdata, "\"IP\":\"");
	if (buf) {
		buf = buf + strlen("\"IP\":\"");
		sscanf(buf, "%u.%u.%u.%u", (unsigned int*)&netSet[0], (unsigned int*)&netSet[1], (unsigned int*)&netSet[2], (unsigned int*)&netSet[3]);
		core_util_critical_section_enter();
		if (settings.data.net_mode == NET_MODE_STATIC && settings.data.ip != tmpNetSet) {
			settings.data.ip = tmpNetSet;
			settingsChanged = true;
			netSettingsChanged = true;
		}
		core_util_critical_section_exit();
	}
	buf = strstr(jdata, "\"mask\":\"");
	if (buf) {
		buf = buf + strlen("\"mask\":\"");
		sscanf(buf, "%u.%u.%u.%u", (unsigned int*)&netSet[0], (unsigned int*)&netSet[1], (unsigned int*)&netSet[2], (unsigned int*)&netSet[3]);
		core_util_critical_section_enter();
		if (settings.data.net_mode == NET_MODE_STATIC && settings.data.mask != tmpNetSet) {
			settings.data.mask = tmpNetSet;
			settingsChanged = true;
			netSettingsChanged = true;
		}
		core_util_critical_section_exit();
	}
	buf = strstr(jdata, "\"gateway\":\"");
	if (buf) {
		buf = buf + strlen("\"gateway\":\"");
		sscanf(buf, "%u.%u.%u.%u", (unsigned int*)&netSet[0], (unsigned int*)&netSet[1], (unsigned int*)&netSet[2], (unsigned int*)&netSet[3]);
		core_util_critical_section_enter();
		if (settings.data.net_mode == NET_MODE_STATIC && settings.data.gateway != tmpNetSet) {
			settings.data.gateway = tmpNetSet;
			settingsChanged = true;
			netSettingsChanged = true;
		}
		core_util_critical_section_exit();
	}
	buf = strstr(jdata, "\"DNS1\":\"");
	if (buf) {
		buf = buf + strlen("\"DNS1\":\"");
		sscanf(buf, "%u.%u.%u.%u", (unsigned int*)&netSet[0], (unsigned int*)&netSet[1], (unsigned int*)&netSet[2], (unsigned int*)&netSet[3]);
		core_util_critical_section_enter();
		if (settings.data.net_mode == NET_MODE_STATIC && settings.data.dns1 != tmpNetSet) {
			settings.data.dns1 = tmpNetSet;
			settingsChanged = true;
			netSettingsChanged = true;
		}
		core_util_critical_section_exit();
	}
	buf = strstr(jdata, "\"DNS2\":\"");
	if (buf) {
		buf = buf + strlen("\"DNS2\":\"");
		sscanf(buf, "%u.%u.%u.%u", (unsigned int*)&netSet[0], (unsigned int*)&netSet[1], (unsigned int*)&netSet[2], (unsigned int*)&netSet[3]);
		core_util_critical_section_enter();
		if (settings.data.net_mode == NET_MODE_STATIC && settings.data.dns2 != tmpNetSet) {
			settings.data.dns2 = tmpNetSet;
			settingsChanged = true;
			netSettingsChanged = true;
		}
		core_util_critical_section_exit();
	}
	buf = strstr(jdata, "\"mac\":\"");
	if (buf) {
		buf = buf + strlen("\"mac\":\"");
		int ret = sscanf(buf, "%02X:%02X:%02X:%02X:%02X:%02X", (unsigned int*)&mac[0], (unsigned int*)&mac[1], (unsigned int*)&mac[2], (unsigned int*)&mac[3], (unsigned int*)&mac[4], (unsigned int*)&mac[5]);
		if (ret == 6) {
			core_util_critical_section_enter();
			if ((mac[0] & 0x03) == 0x02 && memcmp(settings.data.mac, mac, 6) != 0) {
				memcpy(settings.data.mac, mac, 6);
				settingsChanged = true;
				netSettingsChanged = true;
			}
			core_util_critical_section_exit();
		}
	}
	buf = strstr(jdata, "\"netTimeout\":");
	if (buf) {
		buf = buf + strlen("\"netTimeout\":");
		sscanf(buf, "%u", (unsigned int*)&tmpNetSet);
		core_util_critical_section_enter();
		if (settings.data.con_timeout != tmpNetSet) {
			settings.data.con_timeout = tmpNetSet;
			settingsChanged = true;
		}
		core_util_critical_section_exit();
	}
	buf = strstr(jdata, "\"devID\":");
	if (buf) {
		buf = buf + strlen("\"devID\":");
		sscanf(buf, "%u", (unsigned int*)&tmpNetSet);
		core_util_critical_section_enter();
		if (settings.data.devID != tmpNetSet) {
			settings.data.devID = tmpNetSet;
			settingsChanged = true;
		}
		core_util_critical_section_exit();
	}
	buf = strstr(jdata, "\"devNumber\":");
	if (buf) {
		buf = buf + strlen("\"devNumber\":");
		sscanf(buf, "%u", (unsigned int*)&tmpNetSet);
		core_util_critical_section_enter();
		if (settings.data.dev_number != tmpNetSet) {
			settings.data.dev_number = tmpNetSet;
			settingsChanged = true;
		}
		core_util_critical_section_exit();
	}
	buf = strstr(jdata, "\"tx_pwr\":");
	if (buf) {
		buf = buf + strlen("\"tx_pwr\":");
		sscanf(buf, "%u", (unsigned int*)&tmpNetSet);
		if (tmpNetSet && tmpNetSet <= TX_POWER_VALUES_NUMBER) {
			core_util_critical_section_enter();
			if (settings.data.tx_pwr != (uint8_t)tmpNetSet) {
				settings.data.tx_pwr = tmpNetSet;
				settingsChanged = true;
			}
			core_util_critical_section_exit();
		}
	}
	buf = strstr(jdata, "\"beac_trh\":");
	if (buf) {
		buf = buf + strlen("\"beac_trh\":");
		sscanf(buf, "%d", (unsigned int*)&beac_trh);
		core_util_critical_section_enter();
		if (settings.data.beac_trh != beac_trh) {
			settings.data.beac_trh = beac_trh;
			settingsChanged = true;
		}
		core_util_critical_section_exit();
	}
	buf = strstr(jdata, "\"enableLogs\":");
	if (buf) {
		buf = buf + strlen("\"enableLogs\":");
		if (memcmp(buf, "true", 4) == 0) {
			core_util_critical_section_enter();
			if(settings.data.logs_mode == LOGS_MODE_DISABLED) {
				settings.data.logs_mode = LOGS_MODE_ENABLED;
				settingsChanged = true;
			}
			core_util_critical_section_exit();
		}
		else if (memcmp(buf, "false", 5) == 0) {
			core_util_critical_section_enter();
			if(settings.data.logs_mode == LOGS_MODE_ENABLED) {
				settings.data.logs_mode = LOGS_MODE_DISABLED;
				settingsChanged = true;
			}
			core_util_critical_section_exit();
		}
		else {
			ret = -1;
			goto exit;
		}
	}
	buf = strstr(jdata, "\"enableSensor\":");
	if (buf) {
		buf = buf + strlen("\"enableSensor\":");
		if (memcmp(buf, "true", 4) == 0) {
			core_util_critical_section_enter();
			if(settings.data.enable_dist_sensor == (uint8_t)false) {
				settings.data.enable_dist_sensor = (uint8_t)true;
				settingsChanged = true;
			}
			core_util_critical_section_exit();
		}
		else if (memcmp(buf, "false", 5) == 0) {
			core_util_critical_section_enter();
			if(settings.data.enable_dist_sensor == (uint8_t)true) {
				settings.data.enable_dist_sensor = (uint8_t)false;
				settingsChanged = true;
			}
			core_util_critical_section_exit();
		}
		else {
			ret = -1;
			goto exit;
		}
	}
	buf = strstr(jdata, "\"enableFeedback\":");
	if (buf) {
		buf = buf + strlen("\"enableFeedback\":");
		if (memcmp(buf, "true", 4) == 0) {
			core_util_critical_section_enter();
			if(settings.data.enable_gate_feedback == (uint8_t)false) {
				settings.data.enable_gate_feedback = (uint8_t)true;
				settingsChanged = true;
			}
			core_util_critical_section_exit();
		}
		else if (memcmp(buf, "false", 5) == 0) {
			core_util_critical_section_enter();
			if(settings.data.enable_gate_feedback == (uint8_t)true) {
				settings.data.enable_gate_feedback = (uint8_t)false;
				settingsChanged = true;
			}
			core_util_critical_section_exit();
		}
		else {
			ret = -1;
			goto exit;
		}
	}
	buf = strstr(jdata, "\"sensorDistance\":\"");
	if (buf) {
		buf = buf + strlen("\"sensorDistance\":\"");
		sscanf(buf, "%f", &dist);
		dist *= 100;
		core_util_critical_section_enter();
		if (settings.data.dist_sensor_threshold != (uint16_t)dist) {
			settings.data.dist_sensor_threshold = (uint16_t)dist;
			settingsChanged = true;
		}
		core_util_critical_section_exit();
	}
	buf = strstr(jdata, "\"reboot\":");
	if (buf) {
		buf = buf + strlen("\"reboot\":");
		if (memcmp(buf, "true", 4) == 0) {
			_needReboot = true;
		}
		else if (memcmp(buf, "false", 5) == 0) {
			_needReboot = false;
		}
		else {
			ret = -1;
			goto exit;
		}
	}
	buf = strstr(jdata, "\"reset\":");
	if (buf) {
		buf = buf + strlen("\"reset\":");
		if (memcmp(buf, "true", 4) == 0) {
			_needReset = true;
		}
		else if (memcmp(buf, "false", 5) == 0) {
			_needReset = false;
		}
		else {
			ret = -1;
			goto exit;
		}
	}
	buf = strstr(jdata, "\"needLogs\":");
	if (buf) {
		buf = buf + strlen("\"needLogs\":");
		if (memcmp(buf, "true", 4) == 0) {
			_needLogTransmit = true;
		}
		else if (memcmp(buf, "false", 5) == 0) {
			//сбросим флаг после отправки логов
		}
		else {
			ret = -1;
			goto exit;
		}
	}
	buf = strstr(jdata, "\"flushSD\":");
	if (buf) {
		buf = buf + strlen("\"flushSD\":");
		if (memcmp(buf, "true", 4) == 0) {
			uint8_t *mail = sdcardCmdMailbox.alloc();
			if (mail) {
				*mail = FORMAT_SDCARD_CMD;
				sdcardCmdMailbox.put(mail);
			}
		}
		else if (memcmp(buf, "false", 5) == 0) {
			//ничего не делаем
		}
		else {
			ret = -1;
			goto exit;
		}
	}
	buf = strstr(jdata, "\"shutDown\":");
	if (buf) {
		buf = buf + strlen("\"shutDown\":");
		if (memcmp(buf, "true", 4) == 0) {
			osSignalSet(mainThreadID, SHUTDOWN_MODE_ON);
		}
		else if (memcmp(buf, "false", 5) == 0) {
			//ничего не делаем
		}
		else {
			ret = -1;
			goto exit;
		}
	}
//	buf = strstr(jdata, "\"reqFirmware\":\"");
//	if (buf) {
//		buf = buf + strlen("\"reqFirmware\":\"");
//		sscanf(buf , "%[^\"]", reqFirmware);
//	}
	buf = strstr(jdata, "\"fileToUpdate\":");
	if (buf) {
		buf = buf + strlen("\"fileToUpdate\":");
		if (memcmp(buf, "null", 4) == 0) {
			_fwUpdate = false;
		}
		else  {
			buf++;
			sscanf(buf, "%[^\"]", EzServer.fileName);
			_fwUpdate = true;
		}
	}
	buf = strstr(jdata, "\"syncTime\":");
	if (buf) {
		buf = buf + strlen("\"syncTime\":");
		sscanf(buf , "%u", (unsigned int*)(&_syncTime));
	}
	buf = strstr(jdata, "\"controller\":{\"type\":\"");
	if (buf) {
		buf = buf + strlen("\"controller\":{\"type\":\"");
		uint8_t devtype;
		if (memcmp(buf, "ENTER", 5) == 0) {
			devtype = DEVICE_TYPE_IN;
		}
		else if (memcmp(buf, "EXIT", 4) == 0) {
			devtype = DEVICE_TYPE_OUT;
		}
		else if (memcmp(buf, "OFF", 3) == 0) {
			devtype = DEVICE_TYPE_OFF;
		}
		else {
			ret = -1;
			goto exit;
		}

		core_util_critical_section_enter();
		if (devtype != settings.data.dev_type) {
			settings.data.dev_type = devtype;
			settingsChanged = true;
		}
		core_util_critical_section_exit();
	}
	buf = strstr(jdata, "\"general\":");
	if (buf) {
		buf = buf + strlen("\"general\":");
		sscanf(buf , "%d", &_general);
	}
	buf = strstr(jdata, "\"corporate\":");
	if (buf) {
		buf = buf + strlen("\"corporate\":");
		sscanf(buf , "%d", &_corporate);
		_allowReservedSpaces = true;
		buf = strstr(jdata, "\"reserved\":");
		if (buf) {
			buf = buf + strlen("\"reserved\":");
			sscanf(buf , "%d", &_reserved);
			_allowReservedSpaces = true;
		}
	}
	else {
		_allowReservedSpaces = false;
	}

	exit:

	if (settingsChanged) {
		settings.save();
		settings.set_changed_flag(true);
		LOG("Settings successfully changed");
		if (netSettingsChanged) {
			LOG("Reboot...");
			rebootMCU();
		}
	}

	return ret;
}

int DataBase::updateBase(const char* buf, size_t len) {
	int ret = 0;
	if (_syncMode == SyncEmty) {
		return ret;
	}

	char *usersArray;
	char *beaconsArray;

	if (_syncMode == SyncAll) {
		usersArray = (char*)allUsers;
		beaconsArray = (char*)allBeacons;
	}
	else if (_syncMode == SyncEvents){
		usersArray = (char*)allUsersEvents;
		beaconsArray = (char*)allBeaconsEvents;
	}
	else {
		ret = -1;
		LOG("Unknown sync type");
		goto exit;
	}

	_mutex.lock();

	if (len) {
		if (!_endParseSize) {
			if (memcmp(buf, "{\"code\"", 7) == 0) {	// Пришла ошибка с сервера "{"code": 4xxxx}"
				ret = -1;
				goto exit;
			}

			memcpy(&sizeOfJson, buf, 4);
			memcpy(&sizeOfusers, buf + 4, 4);
			memcpy(&sizeOfbeacons, buf + 8, 4);
			memcpy(&sizeOfsessions, buf + 12, 4);
			_endParseSize = true;

			bzero((uint8_t*)allSessions + sizeOfsessions, sizeof(allSessions) - sizeOfsessions);

			if (_syncMode == SyncAll && (sizeOfJson > sizeof(jdata) || sizeOfusers > sizeof(allUsers) || sizeOfbeacons > sizeof(allBeacons) || sizeOfsessions > sizeof(allSessions))) {
				ret = -1;
				LOG("Receive buffer overflow!");
				goto exit;
			}
			else if (_syncMode == SyncEvents && (sizeOfJson > sizeof(jdata) || sizeOfusers > sizeof(allUsersEvents) || sizeOfbeacons > sizeof(allBeaconsEvents) || sizeOfsessions > sizeof(allSessions))) {
				ret = -1;
				LOG("Receive buffer overflow!");
				goto exit;
			}

			if (sizeOfJson <= len - 16) {
				memcpy(jdata, buf + 16, sizeOfJson);
				_endParseJson = true;
				parseJson();

				if (sizeOfusers >= len - 16 - sizeOfJson) {
					memcpy(usersArray, buf + 16 + sizeOfJson, len - 16 - sizeOfJson);
					stayWrite = sizeOfusers - (len - 16 - sizeOfJson);
					goto exit;
				}
				else {
					memcpy(usersArray, buf + 16 + sizeOfJson, sizeOfusers);
					_endParseUsers = true;
					if (sizeOfbeacons >= len - 16 - sizeOfJson - sizeOfusers) {
						memcpy(beaconsArray, buf + 16 + sizeOfJson + sizeOfusers, len - 16 - sizeOfJson - sizeOfusers);
						stayWrite = sizeOfbeacons - (len - 16 - sizeOfJson - sizeOfusers);
						goto exit;
					}
					else {
						memcpy(beaconsArray, buf + 16 + sizeOfJson + sizeOfusers, sizeOfbeacons);
						_endParseBeacons = true;
						if (sizeOfsessions >= len - 16 - sizeOfJson - sizeOfusers - sizeOfbeacons) {
							memcpy((char*)allSessions, buf + 16 + sizeOfJson + sizeOfusers + sizeOfbeacons, len - 16 - sizeOfJson - sizeOfusers - sizeOfbeacons);
							stayWrite = sizeOfsessions - (len - 16 - sizeOfJson - sizeOfusers - sizeOfbeacons);
							goto exit;
						}
						else {
							memcpy((char*)allSessions, buf + 16 + sizeOfJson + sizeOfusers + sizeOfbeacons, sizeOfsessions);
							_endParseSessions = true;
							goto exit;
						}
					}
				}
			}
			else {
				memcpy(jdata, buf + 16, len - 16);
				stayWrite = sizeOfJson - (len - 16);
				goto exit;
			}
		}
		else {
			if (_syncMode == SyncAll && (sizeOfJson > sizeof(jdata) || sizeOfusers > sizeof(allUsers) || sizeOfbeacons > sizeof(allBeacons) || sizeOfsessions > sizeof(allSessions))) {
				ret = -1;
				LOG("Receive buffer overflow!");
				goto exit;
			}
			else if (_syncMode == SyncEvents && (sizeOfJson > sizeof(jdata) || sizeOfusers > sizeof(allUsersEvents) || sizeOfbeacons > sizeof(allBeaconsEvents) || sizeOfsessions > sizeof(allSessions))) {
				ret = -1;
				LOG("Receive buffer overflow!");
				goto exit;
			}
		}
		if (!_endParseJson) {
			if (stayWrite >= len) {
				memcpy(jdata + (sizeOfJson - stayWrite), buf, len);
				stayWrite -= len;
				goto exit;
			}
			else {
				memcpy(jdata + (sizeOfJson - stayWrite), buf, stayWrite);
				_endParseJson = true;
				parseJson();
				if (sizeOfusers >= len - stayWrite) {
					memcpy(usersArray, buf + stayWrite, len - stayWrite);
					stayWrite = sizeOfusers - (len - stayWrite);
					goto exit;
				}
				else {
					memcpy(usersArray, buf + stayWrite, sizeOfusers);
					_endParseUsers = true;
					if (sizeOfbeacons >= len - stayWrite - sizeOfusers) {
						memcpy(beaconsArray, buf + stayWrite + sizeOfusers, len - stayWrite - sizeOfusers);
						stayWrite = sizeOfbeacons - (len - stayWrite - sizeOfusers);
						goto exit;
					}
					else {
						memcpy(beaconsArray, buf + stayWrite + sizeOfusers, sizeOfbeacons);
						_endParseBeacons = true;
						if (sizeOfsessions >= len - stayWrite - sizeOfusers - sizeOfbeacons) {
							memcpy((char*)allSessions, buf + stayWrite + sizeOfusers + sizeOfbeacons, len - stayWrite - sizeOfusers - sizeOfbeacons);
							stayWrite = sizeOfsessions - (len - stayWrite - sizeOfusers - sizeOfbeacons);
							goto exit;
						}
						else {
							memcpy((char*)allSessions, buf + stayWrite + sizeOfusers + sizeOfbeacons, sizeOfsessions);
							_endParseSessions = true;
							goto exit;
						}
					}
				}
			}
		}
		if (!_endParseUsers) {
			if (stayWrite >= len) {
				memcpy(usersArray + (sizeOfusers - stayWrite), buf, len);
				stayWrite -= len;
				goto exit;
			}
			else {
				memcpy(usersArray + (sizeOfusers - stayWrite), buf, stayWrite);
				_endParseUsers = true;
				if (sizeOfbeacons >= len - stayWrite) {
					memcpy(beaconsArray, buf + stayWrite, len - stayWrite);
					stayWrite = sizeOfbeacons - (len - stayWrite);
					goto exit;
				}
				else {
					memcpy(beaconsArray, buf + stayWrite, sizeOfbeacons);
					_endParseBeacons = true;
					if (sizeOfsessions >= len - stayWrite - sizeOfbeacons) {
						memcpy((char*)allSessions, buf + stayWrite + sizeOfbeacons, len - stayWrite - sizeOfbeacons);
						stayWrite = sizeOfsessions - (len - stayWrite - sizeOfbeacons);
						goto exit;
					}
					else {
						memcpy((char*)allSessions, buf + stayWrite + sizeOfbeacons, sizeOfsessions);
						_endParseSessions = true;
						goto exit;
					}
				}
			}
		}
		if (!_endParseBeacons) {
			if (stayWrite >= len) {
				memcpy(beaconsArray + (sizeOfbeacons - stayWrite), buf, len);
				stayWrite -= len;
				goto exit;
			}
			else {
				memcpy(beaconsArray + (sizeOfbeacons - stayWrite), buf, stayWrite);
				_endParseBeacons = true;

				if (sizeOfsessions >= len - stayWrite) {
					memcpy((char*)allSessions, buf + stayWrite, len - stayWrite);
					stayWrite = sizeOfsessions - (len - stayWrite);
					goto exit;
				}
				else {
					memcpy((char*)allSessions, buf + stayWrite, sizeOfsessions);
					_endParseSessions = true;
					goto exit;
				}

			}
		}
		if (!_endParseSessions) {
			if (stayWrite >= len) {
				memcpy((char*)allSessions + (sizeOfsessions - stayWrite), buf, len);
				stayWrite -= len;
				goto exit;
			}
			else {
				memcpy((char*)allSessions + (sizeOfsessions - stayWrite), buf, stayWrite);
				_endParseSessions = true;
				goto exit;
			}
		}
	}

	exit:

	_mutex.unlock();

	return ret;
}

void DataBase::updateUsers() {
	bool isHaveMac = false;

	_mutex.lock();

	if (_syncMode == SyncAll) {
		_beacons = sizeOfbeacons/sizeof(beacons_t);
	}
	else if (_syncMode == SyncEvents) {
		for(uint32_t i = 0; i < sizeOfusers/sizeof(usersEvents_t); i++) {
			allUsers[allUsersEvents[i].user_id].billing = allUsersEvents[i].user.billing;
			allUsers[allUsersEvents[i].user_id].type = allUsersEvents[i].user.type;
		}
		for (uint32_t i = 0; i < sizeOfbeacons/sizeof(usersEvents_t); i++) {
			for (uint32_t j = 0; j < _beacons; j++) {
				if (strcmp(allBeaconsEvents[i].mac, allBeacons[j].mac) == 0) {
					isHaveMac = true;
					if (allBeaconsEvents[i].id != 0xFFFFFFFF) {
						allBeacons[j].id = allBeaconsEvents[i].id;
					}
					else {
						strcpy(allBeacons[j].mac, allBeacons[_beacons - 1].mac);
						allBeacons[j].id = allBeacons[_beacons - 1].id;
					}
					break;
				}
			}
			if (!isHaveMac) {
				strcpy(allBeacons[_beacons].mac, allBeaconsEvents[i].mac);
				allBeacons[_beacons].id = allBeaconsEvents[i].id;
				_beacons++;
			}
			else {
				isHaveMac = false;
			}
		}
	}
	_mutex.unlock();
}

char* DataBase::getCurrFirmwareVer() {
	return currFirmware;
}

char* DataBase::getReqFirmwareVer() {
	return reqFirmware;
}

bool DataBase::isNeedReboot() {
	return _needReboot;
}

bool DataBase::isNeedReset() {
	return _needReset;
}

bool DataBase::isNeedLogTransmit() {
	core_util_critical_section_enter();
	bool ret = _needLogTransmit;
	core_util_critical_section_exit();
	return ret;
}

void DataBase::resetLogTransmitFlag() {
	core_util_critical_section_enter();
	_needLogTransmit = false;
	core_util_critical_section_exit();
}

time_t DataBase::getSyncTime() {
	_mutex.lock();
	time_t ret = _syncTime;
	_mutex.unlock();
	return ret;
}

bool DataBase::isDeviceOff() {
	return _isDeviceOff;
}

void DataBase::decrementOfGeneral() {
	core_util_critical_section_enter();
	_generalLocal--;
	core_util_critical_section_exit();
}

void DataBase::decrementOfCorporate() {
	core_util_critical_section_enter();
	_corporateLocal--;
	core_util_critical_section_exit();
}

void DataBase::decrementOfReserved() {
	core_util_critical_section_enter();
	_reservedLocal--;
	core_util_critical_section_exit();
}

void DataBase::syncCounters() {
	core_util_critical_section_enter();
	_reservedLocal = _reserved;
	_corporateLocal = _corporate;
	_generalLocal = _general;
	core_util_critical_section_exit();
}

bool DataBase::allowReservedSpaces() {
	return _allowReservedSpaces;
}

bool DataBase::fwUpdate() {
	return _fwUpdate;
}

bool DataBase::backupEvents() {
	bool ret = true;
	uint32_t written = 0;
	uint32_t size;
	uint32_t finishCRC = 0;
	uint32_t addr = EVENTS_FLASH_ADDR;
	CRC32 crc;
	_mutex.lock();

	HAL_FLASH_Unlock();

	if (_numbEvents == 0) goto exit;

	if (FLASH_WaitForLastOperation((uint32_t)HAL_FLASH_TIMEOUT_VALUE) != HAL_OK) {
		ret = false;
		goto exit;
	}
//	FLASH_Erase_Sector(FLASH_SECTOR_10, VOLTAGE_RANGE_1);

	size = sizeof(_numbEvents);
	while (size > written) {
		if (HAL_FLASH_Program(TYPEPROGRAM_BYTE, addr + written, ((uint8_t*)(&_numbEvents))[written]) != HAL_OK) {
			ret = false;
			goto exit;
		}
		written++;
	}
	addr += written;

	crc.process(&_numbEvents, 4);

	size = sizeof(events_t);

	for (uint32_t i = 0; i < _numbEvents; i++) {
		written = 0;
		while (size > written) {
			if (HAL_FLASH_Program(TYPEPROGRAM_BYTE, addr + written, ((uint8_t*)(&usersEvents[i]))[written]) != HAL_OK) {
				ret = false;
				goto exit;
			}
			written++;
		}
		addr += written;

		crc.process(&usersEvents[i], sizeof(events_t));
	}

	finishCRC = crc.complete();

	size = sizeof(finishCRC);
	written = 0;
	while (size > written) {
		if (HAL_FLASH_Program(TYPEPROGRAM_BYTE, addr + written, ((uint8_t*)(&finishCRC))[written]) != HAL_OK) {
			ret = false;
			goto exit;
		}
		written++;
	}

	exit:

	HAL_FLASH_Lock();

	_mutex.unlock();

	return ret;
}

bool DataBase::checkEvents() {
	uint32_t finishCRC = 0;
	uint32_t addr = EVENTS_FLASH_ADDR;
	uint32_t crcend = 0;
	CRC32 crc;

	memcpy(&_numbEvents, (uint32_t*)addr, 4);
	if (_numbEvents == 0xFFFFFFFF) {
		_numbEvents = 0;
		return false;
	}
	if (_numbEvents != 0) {
		memcpy(&finishCRC, (uint32_t*)(addr + 4 + _numbEvents * sizeof(events_t)), 4);
		crcend = crc.get((uint8_t*)addr, 4 + _numbEvents * sizeof(events_t));
		if (finishCRC == crcend) {
			memcpy((char*)usersEvents, (char*)(addr + 4), _numbEvents * sizeof(events_t));
			return true;
		}
		else {
			_numbEvents = 0;
			return true;
		}
	}
	else {
		return true;
	}
}

DataBase::BillingStatus_t DataBase::checkBilling(uint8_t *uuid, uint32_t id, BillingStatus_t *sessionStatus) {
	BillingStatus_t ret = NO_USER;

	if (id >= ALL_USERS_ARRAY_LEN) {
		goto exit;
	}

	_mutex.lock();
	ret = binBilling(uuid, id, sessionStatus, false);
	_mutex.unlock();

	exit:

	return ret;
}

DataBase::BillingStatus_t DataBase::binBilling(uint8_t *uuid, uint32_t id, BillingStatus_t *sessionStatus, bool isBeacon) {
	BillingStatus_t ret = NO_USER;
	UserType_t usertype = USER_UNKNOWN;
	char billing = 'f';

	if (allUsers[id].type == 'c') {
		usertype = USER_CORPORATE;
	}
	else if (allUsers[id].type == 'r') {
		usertype = USER_RESERVED;
	}
	else if (allUsers[id].type == 'g') {
		usertype = USER_GENERAL;
	}

	*sessionStatus = findSession(id);
	if (*sessionStatus == NO_SESSION) {
		// Если нет сессии, то, может быть, не успели синхронизироваться, поэтому ищем записанный нами ивент
		if (findEvent(uuid, isBeacon)) {
			*sessionStatus = BALANCE_OK;	// Если нашли, то пусть будет OK, т.к. для входа разницы нет. Главное - заезд до этого уже был
		}
	}

	billing = allUsers[id].billing;

	ret = finishBilling(usertype, *sessionStatus, billing, isBeacon);

	return ret;
}

DataBase::BillingStatus_t DataBase::checkBillingBeacon(uint8_t* mac, BillingStatus_t *sessionStatus) {
	BillingStatus_t ret = NO_USER;

	_mutex.lock();
	for (uint32_t i = 0; i < _beacons; i++) {
		if (memcmp(mac, allBeacons[i].mac, 6) == 0) {
			ret = binBilling(mac, allBeacons[i].id, sessionStatus, true);
			break;
		}
	}
	_mutex.unlock();

	return ret;
}

DataBase::BillingStatus_t DataBase::checkBillingExit(uint8_t *uuid, uint32_t id) {
	BillingStatus_t ret = NO_SESSION;

	_mutex.lock();

	if (allUsers[id].type == 'c') {
		ret = CORPORATE_USER;
		goto exit;
	}
	else if (allUsers[id].type == 'r') {
		ret = RESERVED_USER;
		goto exit;
	}
	else if (allUsers[id].type == 'g') {
		if (findEvent(uuid, false)) {
			goto exit;
		}
		ret = findSession(id);
	}

	exit:

	_mutex.unlock();

	return ret;
}

DataBase::BillingStatus_t DataBase::checkBillingExitBeacon(uint8_t* mac) {
	BillingStatus_t ret = NO_SESSION;

	_mutex.lock();
	if (findEvent(mac, true)) {
		goto exit;
	}

	for (uint32_t i = 0; i < _beacons; i++) {
		if (memcmp(mac, allBeacons[i].mac, 6) == 0) {
			if (allUsers[allBeacons[i].id].type == 'c') {
				ret = CORPORATE_USER;
				goto exit;
			}
			else if (allUsers[allBeacons[i].id].type == 'r') {
				ret = RESERVED_USER;
				goto exit;
			}
			ret = findSession(allBeacons[i].id);
			break;
		}
	}

	exit:

	_mutex.unlock();

	return ret;
}

DataBase::BillingStatus_t DataBase::findSession(uint32_t userid) {
	BillingStatus_t ret = NO_SESSION;

	for (uint32_t i = 0; i < sizeof(allSessions)/sizeof(sessions_t); i++) {
		if (allSessions[i].id == userid) {
			if (allSessions[i].billing == 't') {
				ret = BALANCE_OK;
			}
			else {
				ret = NO_BALANCE;
			}
			break;
		}
	}

	return ret;
}

DataBase::BillingStatus_t DataBase::finishBilling(UserType_t usertype, BillingStatus_t sessionstatus, char billing, bool isBeacon) {
	BillingStatus_t ret = NO_USER;

	switch (usertype) {
	case USER_GENERAL: {
		if (sessionstatus == NO_SESSION) {
			if (_general > 0 && _generalLocal > 0) {
				if (billing == 't') {
					ret = BALANCE_OK;
				}
				else {
					ret = NO_BALANCE;
				}
			}
			else {
				ret = NO_PLACES;
			}
		}
		else {
			ret = SESSION_IS_OPENED;
		}
		break;
	}
	case USER_CORPORATE: {
		bool isHaveSpaces;

		if (_allowReservedSpaces) {
			isHaveSpaces = (_corporate > 0 && _corporateLocal > 0);
		}
		else {
			isHaveSpaces = (_general > 0 && _generalLocal > 0);
		}

		if (isHaveSpaces || sessionstatus != NO_SESSION) {
			if (isBeacon) {
				if (billing == 't') {
					ret = CORPORATE_USER;
				}
				else {
					ret = NO_BALANCE;
				}
			}
			else {
				ret = CORPORATE_USER;
			}
		}
		else {
			ret = NO_PLACES;
		}
		break;
	}
	case USER_RESERVED: {
		bool isHaveSpaces;

		if (_allowReservedSpaces) {
			isHaveSpaces = (_reserved > 0 && _reservedLocal > 0);
		}
		else {
			isHaveSpaces = (_general > 0 && _generalLocal > 0);
		}
		if (isHaveSpaces || sessionstatus != NO_SESSION) {
			if (isBeacon) {
				if (billing == 't') {
					ret = RESERVED_USER;
				}
				else {
					ret = NO_BALANCE;
				}
			}
			else {
				ret = RESERVED_USER;
			}
		}
		else {
			ret = NO_PLACES;
		}
		break;
	}
	default:
		break;
	}

	return ret;
}

bool DataBase::findEvent(uint8_t *uuid, bool isBeacon) {
	bool ret = false;

	for (uint32_t i = 0; i < _numbEvents; i++) {
		if (isBeacon) {
			if (memcmp(uuid, usersEvents[i].uuid, 6) == 0) {
				ret = true;
				break;
			}
		}
		else {
			if (memcmp(uuid, usersEvents[i].uuid, 16) == 0) {
				ret = true;
				break;
			}
		}
	}
	return ret;
}

void DataBase::writeEvent(uint8_t *uuid, time_t tm, bool isBeacon) {
	_mutex.lock();

	if (uuid != NULL) {
		usersEvents[_numbEvents].isUser = true;
		usersEvents[_numbEvents].isBeacon = isBeacon;
		if (isBeacon){
			memcpy(usersEvents[_numbEvents].uuid, uuid, 6);
		}
		else {
			memcpy(usersEvents[_numbEvents].uuid, uuid, 16);
		}
	}
	else {
		usersEvents[_numbEvents].isUser = false;
	}
	usersEvents[_numbEvents].tm = tm;

	_numbEvents++;
	_mutex.unlock();
}

bool DataBase::isUpdated() {
	core_util_critical_section_enter();
	bool ret = _updated;
	core_util_critical_section_exit();
	return ret;
}

void DataBase::updated() {
	core_util_critical_section_enter();
	_updated = true;
	core_util_critical_section_exit();
}

void DataBase::resetUpdate() {
	core_util_critical_section_enter();
	_updated = false;
	core_util_critical_section_exit();
}

void DataBase::cardFormatted() {
	_cardFormatted = true;
}

//============================================================================

void DataBase::convertUuid2Str(char* to, uint8_t* from) {
	for (int q = 0; q < 4; q ++) {
		sprintf(&to[q*2], "%02x", from[q]);
	}
	to[8] = '-';
	for (int q = 0; q < 2; q ++) {
		sprintf(&to[9 + q*2], "%02x", from[4 + q]);
	}
	to[13] = '-';
	for (int q = 0; q < 2; q ++) {
		sprintf(&to[14 + q*2], "%02x", from[6 + q]);
	}
	to[18] = '-';
	for (int q = 0; q < 2; q ++) {
		sprintf(&to[19 + q*2], "%02x", from[8 + q]);
	}
	to[23] = '-';
	for (int q = 0; q < 6; q ++) {
		sprintf(&to[24 + q*2], "%02x", from[10 + q]);
	}
}

void DataBase::convertMac2Str(char* to, uint8_t* from) {
	for (int q = 0; q < 1; q ++) {
		sprintf(&to[q*2], "%02x", from[q]);
	}
	to[2] = '-';
	for (int q = 0; q < 1; q ++) {
		sprintf(&to[3 + q*2], "%02x", from[1 + q]);
	}
	to[5] = '-';
	for (int q = 0; q < 1; q ++) {
		sprintf(&to[6 + q*2], "%02x", from[2 + q]);
	}
	to[8] = '-';
	for (int q = 0; q < 1; q ++) {
		sprintf(&to[9 + q*2], "%02x", from[3 + q]);
	}
	to[11] = '-';
	for (int q = 0; q < 1; q ++) {
		sprintf(&to[12 + q*2], "%02x", from[4 + q]);
	}
	to[14] = '-';
	for (int q = 0; q < 1; q ++) {
		sprintf(&to[15 + q*2], "%02x", from[5 + q]);
	}

}

//================================================================================

DataBase db;
