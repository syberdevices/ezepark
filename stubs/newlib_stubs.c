#include <string.h>

#include <errno.h>
#include <stm32f4xx_hal.h>
#include <sys/stat.h>
#include <sys/times.h>
#include <sys/unistd.h>

//#include <cmsis_os.h>

extern uint32_t __get_MSP( void );

#define STDOUT_USART 0
#define STDERR_USART 0
#define STDIN_USART 0

// errno
#undef errno
extern int errno;

/*
 ���������� ����� - ������ ������.
 */
char *__env[1] = { 0 };
char **environ = __env;

int _write( int file, char *ptr, int len );

// exit - ���������� �����. � �������� ������ - �������������.
//void _exit( int status ) {
//	(void)status;
//
//	while( 1 )
//		;
//}

// close - �������� ����� - ���������� ������
//int _close( int file ) {
//	(void)file;
//
//	return -1;
//}
/*
 execve - �������� ���������� ������ �������� - ��������� ��� -> ���������� ������.
 */
int _execve( char *name, char **argv, char **env ) {
	(void)name;
	(void)argv;
	(void)env;

	errno = ENOMEM;
	return -1;
}

/*
 fork = �������� ������ ��������
 */
int _fork() {
	errno = EAGAIN;
	return -1;
}

/*
 fstat - ��������� ��������� �����
 */
//int _fstat( int file, struct stat *st ) {
//	(void)file;
//	(void)st;
//
//	st->st_mode = S_IFCHR;
//	return 0;
//}

/*
 getpid - �������� ID �������� ��������
 */

int _getpid() {
	return 1;
}

/*
 isatty - �������� �� ���� ����������.
 */
//int _isatty( int file ) {
//	switch( file ) {
//		case STDOUT_FILENO:
//		case STDERR_FILENO:
//		case STDIN_FILENO:
//			return 1;
//		default:
//			//errno = ENOTTY;
//			errno = EBADF;
//			return 0;
//	}
//}

/*
 kill - ������� ������ ��������
 */
int _kill( int pid, int sig ) {
	(void)pid;
	(void)sig;

	errno = EINVAL;
	return ( -1 );
}

/*
 link - ��������� ����� ��� ��� ������������� �����.
 */

int _link( char *old, char *new ) {
	(void)old;
	(void)new;

	errno = EMLINK;
	return -1;
}

/*
 lseek - ���������� ������� � �����
 */
//int _lseek( int file, int ptr, int dir ) {
//	(void)file;
//	(void)ptr;
//	(void)dir;
//
//	return 0;
//}

/*
 sbrk - ��������� ������ ������� ������, ����������� ��� malloc
 */
//caddr_t _sbrk( int incr ) {
//	extern char _ebss;
//	static char *heap_end;
//	char *prev_heap_end;
//
//	if( heap_end == 0 ) {
//		heap_end = &_ebss;
//	}
//	prev_heap_end = heap_end;
//
//	char * stack = (char*)__get_MSP();
//	if( heap_end + incr > stack ) {
//		_write( STDERR_FILENO, "Heap and stack collision\n", 25 );
//		errno = ENOMEM;
//		return (caddr_t)-1;
//		//abort ();
//	}
//
//	heap_end += incr;
//	return (caddr_t)prev_heap_end;
//}

/*
 read - ������ �� �����, � ��� ���� ��� ������ ���� ������ stdin
 */

//int _read( int file, char *ptr, int len ) {
//	(void)ptr;
//	(void)len;
//
//	switch( file ) {
//		case STDIN_FILENO: /*stdout*/
//			break;
//		default:
//			errno = EBADF;
//			return -1;
//	}
//	return -1;
//}

/*
 stat - ��������� ��������� �����.
 */

int _stat( const char *filepath, struct stat *st ) {
	(void)filepath;

	st->st_mode = S_IFCHR;
	return 0;
}

/*
 times - ��������� ���������� � �������� (������� �����: ���������, ������������ � �.�.)
 */

clock_t _times( struct tms *buf ) {
	(void) buf;

	return -1;
}

/*
 unlink - ������� ��� �����.
 */
int _unlink( char *name ) {
	(void) name;

	errno = ENOENT;
	return -1;
}

/*
 wait - �������� �������� ���������
 */
int _wait( int *status ) {
	(void) status;

	errno = ECHILD;
	return -1;
}

/*
 write - ������ � ���� - � ��� ���� ������ stderr/stdout
 */
//int _write( int file, char *ptr, int len ) {
//	(void) ptr;
//	(void) len;
//
//	switch( file ) {
//		case STDOUT_FILENO: /*stdout*/
//			break;
//		case STDERR_FILENO: /* stderr */
//			break;
//		default:
//			errno = EBADF;
//			return -1;
//	}
//	return len;
//}

/*\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\*/

/*
 * Lock routine called by Newlib on malloc / realloc / free entry to guarantee a
 * safe section as memory allocation management uses global data.
 * See the aforementioned details.
 */
//void __malloc_lock( struct _reent *ptr ) {
//	(void) ptr;
//
//	vTaskSuspendAll();
//}

/*
 * Unlock routine called by Newlib on malloc / realloc / free exit to guarantee
 * a safe section as memory allocation management uses global data.
 * See the aforementioned details.
 */
//void __malloc_unlock( struct _reent *ptr ) {
//	(void) ptr;
//
//	xTaskResumeAll();
//}

/*\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\*/

void __attribute__ ((weak)) _init( void ) {
}

/*\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\*/

