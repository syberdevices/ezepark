/******************************************************************************
	���� ���������� ��������� CRC32 �� ����������� ��������� 0xEDB88320.
	�������� �����: ������������ ����� �������������
	E-mail: paveldvlip@mail.ru
	2005 ���
******************************************************************************/

#include "crc32.h"

#define CRC_POLY 0xEDB88320

static uint32_t table[256];
static void init() {
	if (!CRC32::inited) {
		for (uint32_t i = 0; i <= 255; i++) {
			uint32_t crc_tab = i;

			for (uint32_t j = 8; j > 0; j--)
			{
				if ((crc_tab & 1) == 1) {
					crc_tab = (crc_tab >> 1);
					crc_tab ^= CRC_POLY;
				}
				else {
					crc_tab >>= 1;
				}
			}
			table[i] = crc_tab;
		}
		CRC32::inited = true;
	}
}

bool CRC32::inited = false;

CRC32::CRC32() {
	init();
	_crc = -1;
}

uint32_t CRC32::process(const void* pData, uint32_t nLen) {
	for (uint32_t i = 0; i < nLen; i++) {
		_crc = (_crc >> 8) ^ table[(_crc & 0xFF) ^ ((uint8_t*)pData)[i]];
	}

	return _crc;
}

void CRC32::set_current_process_crc(uint32_t curr_crc) {
	_crc = curr_crc;
}

uint32_t CRC32::complete() {
	uint32_t crc = _crc;
	_crc = -1;

	return crc ^ _crc;
}

uint32_t CRC32::get(const void* pData, uint32_t nLen) {
	process(pData, nLen);
	return complete();
}


