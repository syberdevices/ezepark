﻿/*
 * main.cpp
 *
 *  Created on: 8 августа 2016 г.
 *      Author: Chuyec
 */

#include <mbed.h>
#include <SDFileSystem/SDFileSystem.h>
#include <Watchdog/Watchdog.h>
#include <cstdio>
#include <cstring>
#include <crc32/crc32.h>
//===================================================================

#define APP_ADDR			0x08020000		// Сектор 5
#define APP_START_SECTOR	5
#define APP_END_SECTOR		9
#define APP_SECTOR_SIZE		0x20000
#define APP_MAX_SIZE		(APP_SECTOR_SIZE * (APP_END_SECTOR - APP_START_SECTOR + 1))

#define DFU_FIRMWARE_MAX_SIZE			1024*512

// Значение из HAL
#define FLASH_TIMEOUT_VALUE       ((uint32_t)50000U)/* 50 s */

#define LED_SD_PIN			PC_6
#define LED_NET_PIN			PC_9
#define LED_BLE_PIN			PC_8
#define LED_HEALTH_PIN		PC_7

#define BUTTON_PIN			PG_10

#define SD_POWER_PIN		PD_3
#define SD_DETECT_PIN		PD_2
#define SPI_MOSI_PIN		PC_12
#define SPI_MISO_PIN		PC_11
#define SPI_SCK_PIN			PC_10
#define SPI_CS_PIN			PA_15

#define COMPARTATOR_PIN		PF_7

#define SD_POWER_ON() 		sdOn = 0
#define SD_POWER_OFF()		sdOn = 1

//SDFileSystem sd(SPI_MOSI_PIN, SPI_MISO_PIN, SPI_SCK_PIN, SPI_CS_PIN, "sd");
DigitalIn CardDetect(SD_DETECT_PIN, PullUp);
DigitalOut ledSd(LED_SD_PIN, 1);
DigitalOut ledNet(LED_NET_PIN, 1);
DigitalOut ledBle(LED_BLE_PIN, 1);
DigitalOut ledHealth(LED_HEALTH_PIN, 1);
DigitalIn Butt(BUTTON_PIN, PullUp);
DigitalOut sdOn(SD_POWER_PIN, 1);
InterruptIn comp(COMPARTATOR_PIN);
Watchdog wdg(1, 25000);

typedef enum {
	STATE_NORMAL,
	STATE_NEW_IMAGE,
	STATE_TOO_LARGE_IMAGE,
	STATE_NO_IMAGE,
	STATE_READ_FAIL,
	STATE_WRITE_FAIL,
}State_t;

const char fwpath[] = "/sd/bin/fw.bin";
const char backuppath[] = "/sd/bin/backup.bin";
const char bindir[] = "/sd/bin";

extern "C" {
	void mbed_die(void) {
		core_util_critical_section_enter();
		gpio_t led;
		gpio_init_out(&led, LED_HEALTH_PIN);

		while (1) {
			gpio_write(&led, 1);
			wait_ms(150);
			gpio_write(&led, 0);
			wait_ms(150);
		}
	}

	void NMI_Handler() {
		mbed_die();
	}
	void HardFault_Handler() {
		mbed_die();
	}
	void MemManage_Handler() {
		mbed_die();
	}
	void BusFault_Handler() {
		mbed_die();
	}
	void UsageFault_Handler() {
		mbed_die();
	}
}

static void eraseFlash() {
	// Стираем всё, кроме первых пяти секторов (bootloader) и последнего сектора (настройки)
	for (uint8_t sector = APP_START_SECTOR; sector <= APP_END_SECTOR; sector++) {
		while( FLASH_WaitForLastOperation((uint32_t)HAL_FLASH_TIMEOUT_VALUE) );
		FLASH_Erase_Sector(sector, VOLTAGE_RANGE_1);
	}
}

static State_t loadApp2Flash(const char* path) {
	State_t ret;
	uint32_t fwSize = 0;
	uint32_t written = 0;

	FILE *fw = fopen(path, "r");
	if (fw == NULL) {
		ret = STATE_NORMAL;	// Файла нет
		goto exit;
	}

	fseek(fw, 0, SEEK_END);
	fwSize = ftell(fw);
	fseek(fw, 0, SEEK_SET);

	if (fwSize - 4 > APP_MAX_SIZE) { // -4 это crc
		ret = STATE_TOO_LARGE_IMAGE;
		goto exit;
	}

	HAL_FLASH_Unlock();

	eraseFlash();

	while (fwSize > written) {
		int dat = fgetc(fw);
		if (dat == EOF) {
			ret = STATE_READ_FAIL;
			goto exit;
		}

		if (HAL_FLASH_Program(TYPEPROGRAM_BYTE, APP_ADDR + written, (uint8_t)dat) != HAL_OK) {
			ret = STATE_WRITE_FAIL;
			goto exit;
		}
		written++;
		wdg.kick(-1);
	}

	if (fwSize == written) {
		ret = STATE_NEW_IMAGE;
	}

	exit:

	HAL_FLASH_Lock();

	switch (ret) {
		case STATE_NORMAL:
			break;
		case STATE_NEW_IMAGE:
			fclose(fw);
			if (strcmp(path, fwpath) == 0) {
				remove(path);
			}
			break;
		case STATE_TOO_LARGE_IMAGE:
		case STATE_READ_FAIL:
		case STATE_WRITE_FAIL:
		default:
			fclose(fw);
			int cnt = 0;
			while(20 > cnt++) {
				ledHealth = 1;
				wait_ms(100);
				ledHealth = 0;
				wait_ms(100);
			}
			break;
	}

return ret;
}

void (* const jump2App)(void) = (void (*)(void))(*(uint32_t*)(APP_ADDR + 4ul));

static int crcFWfile(uint32_t* resOfCRC, const char* fileName) {
	uint8_t data[1];
	CRC32 crc;
	int res = 0;

	FILE *fp = fopen(fileName, "r");
	if (fp != NULL) {
		res = fseek(fp, 0, SEEK_END);
		if (res == 0) {
			int size = ftell(fp) - 4;
			if (size > 1) {
				fseek(fp, 0, SEEK_SET);
				while(ftell(fp) < size){
					int count = fread(data, 1, 1, fp);
					if (count == 1) {
						crc.process(data, count);
					}
					else {
						goto exit;
					}
				}
			}
			else {
				goto exit;
			}
		}
		else {
			goto exit;
		}
	}
	else {
		goto exit;
	}

	fclose(fp);
	*resOfCRC = crc.complete();
	return 0;

	exit:
	fclose(fp);
	return -1;
}

static void groundSpiPins() {
	DigitalOut led1(SPI_MOSI_PIN);
	DigitalOut led2(SPI_MISO_PIN);
	DigitalOut led3(SPI_SCK_PIN);
	DigitalOut led4(SPI_CS_PIN);

	led1 = 0;
	led2 = 0;
	led3 = 0;
	led4 = 0;
}

bool isLowVoltage = false;
static void shutDown() {
	isLowVoltage = true;
}

int makeBackup() {  	// Пишем бэкаповскую прошивку в файл
	int ret = -1;
	CRC32 crc;
	FILE *fw = fopen(backuppath, "w");
	if (fw != NULL) {
		uint8_t *ptr = (uint8_t*)APP_ADDR;
		while (ptr < (uint8_t*)(APP_ADDR + APP_MAX_SIZE)) {
			fputc(*ptr, fw);
			crc.process(ptr, 1);
			ptr++;
		}
		uint32_t checkSum = crc.complete();
		fwrite((char *)&checkSum, 1, sizeof(checkSum), fw);
		fclose(fw);
		ret = 0;
	}
	return ret;
}


//uint8_t *p_DFU_Data = (uint8_t*)(0x60000000 + 512*1024);
uint8_t *p_DFU_Data = (uint8_t*)(0x60000000 + 6);

State_t try_dfu() {
	CRC32 crcObj;
	State_t ret = STATE_NO_IMAGE;

	uint32_t fw_size = *(uint32_t*)(p_DFU_Data);
	if (fw_size <= DFU_FIRMWARE_MAX_SIZE - 8 && fw_size != 0) {	// -8 потому что в начале размер, в конце crc
		uint32_t crc_loc = crcObj.get(p_DFU_Data + 4, fw_size);
		uint32_t crc_sram;
		memcpy(&crc_sram, p_DFU_Data + 4 + fw_size, sizeof(uint32_t));

		if (crc_loc == crc_sram) {
			HAL_FLASH_Unlock();

			eraseFlash();
			wdg.kick(-1);

			uint32_t written = 0;
			while (fw_size > written) {
				if (HAL_FLASH_Program(TYPEPROGRAM_BYTE, APP_ADDR + written, p_DFU_Data[written + 4]) != HAL_OK) {
					break;
				}
				written++;
			}

			HAL_FLASH_Lock();

			if (fw_size == written) {
				memset(p_DFU_Data, 0, 4);	//	Зануляем первые 4 байта, типа прошивки в SRAM больше нет
				ret = STATE_NEW_IMAGE;
			}
			else {
				ret = STATE_WRITE_FAIL;
			}
		}
	}

	return ret;
}


State_t try_backup() {
	State_t ret = STATE_NO_IMAGE;
	bool havePath = true;

	// Если нет директории /sd/bin, создаем
	DIR* fp = opendir(bindir);
	if (fp == NULL) {
		if (mkdir(bindir, 0) != 0) {
			havePath = false;
		}
	}
	else {
		closedir(fp);
	}

	if (havePath) {
		ledSd = 0;

		// Пишем бэкаповскую прошивку в файл
		FILE* fw = fopen(backuppath, "r");
		if (fw != NULL) {
			fclose(fw);

			Timer timer;
			timer.start();

			uint32_t crc = 0;
			int res = crcFWfile(&crc, backuppath);	// Считаем CRC файла
			if (res != -1) {
				uint32_t _crc = ~crc;	// Заведомо отличное значение
				FILE *fp = fopen(backuppath, "r");
				if (fp != NULL) {
					res = fseek( fp , -4 , SEEK_END );
					if (res == 0) {
						res = fread((char *)&_crc, 1, 4, fp);
					}

					fclose(fp);
				}

				if (crc == _crc) {
					// Делаем бэкап
					while (Butt == 0) {
						if (timer.read_ms() >= 5000) {
							ledSd = 1;
							while (Butt == 0);	// Ждём, пока отпустят кнопку
							ledSd = 0;
							ret = loadApp2Flash(backuppath);
							ledSd = 1;

							break;
						}
					}
				}
				else {
					remove(backuppath);
					makeBackup();
				}
			}
		}
		else {
			makeBackup();
		}

		ledSd = 1;
	}

	return ret;
}

SRAM_HandleTypeDef hsram1;

/* FSMC initialization function */
void MX_FSMC_Init(void)
{
  FSMC_NORSRAM_TimingTypeDef Timing;

  /** Perform the SRAM1 memory initialization sequence
  */
  hsram1.Instance = FSMC_NORSRAM_DEVICE;
  hsram1.Extended = FSMC_NORSRAM_EXTENDED_DEVICE;
  /* hsram1.Init */
  hsram1.Init.NSBank = FSMC_NORSRAM_BANK1;
  hsram1.Init.DataAddressMux = FSMC_DATA_ADDRESS_MUX_DISABLE;
  hsram1.Init.MemoryType = FSMC_MEMORY_TYPE_SRAM;
  hsram1.Init.MemoryDataWidth = FSMC_NORSRAM_MEM_BUS_WIDTH_16;
  hsram1.Init.BurstAccessMode = FSMC_BURST_ACCESS_MODE_DISABLE;
  hsram1.Init.WaitSignalPolarity = FSMC_WAIT_SIGNAL_POLARITY_LOW;
  hsram1.Init.WrapMode = FSMC_WRAP_MODE_DISABLE;
  hsram1.Init.WaitSignalActive = FSMC_WAIT_TIMING_BEFORE_WS;
  hsram1.Init.WriteOperation = FSMC_WRITE_OPERATION_ENABLE;
  hsram1.Init.WaitSignal = FSMC_WAIT_SIGNAL_DISABLE;
  hsram1.Init.ExtendedMode = FSMC_EXTENDED_MODE_DISABLE;
  hsram1.Init.AsynchronousWait = FSMC_ASYNCHRONOUS_WAIT_DISABLE;
  hsram1.Init.WriteBurst = FSMC_WRITE_BURST_DISABLE;
  hsram1.Init.PageSize = FSMC_PAGE_SIZE_NONE;
  /* Timing */
  Timing.AddressSetupTime = 15;
  Timing.AddressHoldTime = 15;
  Timing.DataSetupTime = 255;
  Timing.BusTurnAroundDuration = 15;
  Timing.CLKDivision = 16;
  Timing.DataLatency = 17;
  Timing.AccessMode = FSMC_ACCESS_MODE_A;
  /* ExtTiming */

  if (HAL_SRAM_Init(&hsram1, &Timing, NULL) != HAL_OK)
  {
    mbed_die();
  }

}

static void HAL_FSMC_MspInit(void){
  GPIO_InitTypeDef GPIO_InitStruct;

  /* Peripheral clock enable */
  __HAL_RCC_FSMC_CLK_ENABLE();
  __HAL_RCC_GPIOD_CLK_ENABLE();
  __HAL_RCC_GPIOE_CLK_ENABLE();
  __HAL_RCC_GPIOF_CLK_ENABLE();
  __HAL_RCC_GPIOG_CLK_ENABLE();

  /** FSMC GPIO Configuration
  PF0   ------> FSMC_A0
  PF1   ------> FSMC_A1
  PF2   ------> FSMC_A2
  PF3   ------> FSMC_A3
  PF4   ------> FSMC_A4
  PF5   ------> FSMC_A5
  PF12   ------> FSMC_A6
  PF13   ------> FSMC_A7
  PF14   ------> FSMC_A8
  PF15   ------> FSMC_A9
  PG0   ------> FSMC_A10
  PG1   ------> FSMC_A11
  PE7   ------> FSMC_D4
  PE8   ------> FSMC_D5
  PE9   ------> FSMC_D6
  PE10   ------> FSMC_D7
  PE11   ------> FSMC_D8
  PE12   ------> FSMC_D9
  PE13   ------> FSMC_D10
  PE14   ------> FSMC_D11
  PE15   ------> FSMC_D12
  PD8   ------> FSMC_D13
  PD9   ------> FSMC_D14
  PD10   ------> FSMC_D15
  PD11   ------> FSMC_A16
  PD12   ------> FSMC_A17
  PD13   ------> FSMC_A18
  PD14   ------> FSMC_D0
  PD15   ------> FSMC_D1
  PG2   ------> FSMC_A12
  PG3   ------> FSMC_A13
  PG4   ------> FSMC_A14
  PG5   ------> FSMC_A15
  PD0   ------> FSMC_D2
  PD1   ------> FSMC_D3
  PD4   ------> FSMC_NOE
  PD5   ------> FSMC_NWE
  PD7   ------> FSMC_NE1
  PE0   ------> FSMC_NBL0
  PE1   ------> FSMC_NBL1
  */
  /* GPIO_InitStruct */
  GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3
                          |GPIO_PIN_4|GPIO_PIN_5|GPIO_PIN_12|GPIO_PIN_13
                          |GPIO_PIN_14|GPIO_PIN_15;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF12_FSMC;

  HAL_GPIO_Init(GPIOF, &GPIO_InitStruct);

  /* GPIO_InitStruct */
  GPIO_InitStruct.Pin = GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_2|GPIO_PIN_3
                          |GPIO_PIN_4|GPIO_PIN_5;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF12_FSMC;

  HAL_GPIO_Init(GPIOG, &GPIO_InitStruct);

  /* GPIO_InitStruct */
  GPIO_InitStruct.Pin = GPIO_PIN_7|GPIO_PIN_8|GPIO_PIN_9|GPIO_PIN_10
                          |GPIO_PIN_11|GPIO_PIN_12|GPIO_PIN_13|GPIO_PIN_14
                          |GPIO_PIN_15|GPIO_PIN_0|GPIO_PIN_1;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF12_FSMC;

  HAL_GPIO_Init(GPIOE, &GPIO_InitStruct);

  /* GPIO_InitStruct */
  GPIO_InitStruct.Pin = GPIO_PIN_8|GPIO_PIN_9|GPIO_PIN_10|GPIO_PIN_11
                          |GPIO_PIN_12|GPIO_PIN_13|GPIO_PIN_14|GPIO_PIN_15
                          |GPIO_PIN_0|GPIO_PIN_1|GPIO_PIN_4|GPIO_PIN_5
                          |GPIO_PIN_7;
  GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
  GPIO_InitStruct.Alternate = GPIO_AF12_FSMC;

  HAL_GPIO_Init(GPIOD, &GPIO_InitStruct);
}

void HAL_SRAM_MspInit(SRAM_HandleTypeDef* sramHandle){
  HAL_FSMC_MspInit();
}

//===================================================================

int main() {
	MX_FSMC_Init();
	/// >>> Test start
//	bzero(p_DFU_Data, 20);
//	strcpy((char*)p_DFU_Data, "My second sram data!");
//	char buf3[50];
//	memcpy(buf3, (char*)p_DFU_Data, strlen("My second sram data!"));
	/// <<< Test stop

	ledSd = 0;
	ledHealth = 0;
	ledBle = 0;
	ledNet = 0;

	// Уводим в 0 ноги SPI
	groundSpiPins();

	SD_POWER_OFF();
	wait_ms(300);
	SD_POWER_ON();
	wait_ms(200);

	// Заводим объект SD здесь, чтобы конструктор, настраивающий SPI вызывался после groundSpiPins()
	SDFileSystem sd(SPI_MOSI_PIN, SPI_MISO_PIN, SPI_SCK_PIN, SPI_CS_PIN, "sd");

	ledSd = 1;
	ledHealth = 1;
	ledBle = 1;
	ledNet = 1;

	wait_ms(1000);

	comp.fall(&shutDown);
	comp.mode(PullUp);

	wdg.kick(-1);

	// =======================================

	State_t state = try_dfu();
	if (state == STATE_NEW_IMAGE) {
		goto exit;
	}

	state = try_backup();

	// =======================================

	exit:

	wdg.kick(-1);

	ledSd = 1;
	ledHealth = 1;
	ledBle = 1;
	ledNet = 1;

	core_util_critical_section_enter();
	HAL_DeInit();
	// Запрещаем все прерывания
	for (uint8_t q = 0; q < sizeof(NVIC->ICER)/sizeof(NVIC->ICER[0]); q++) {
		NVIC->ICER[q] = 0xFFFFFF;
		NVIC->ICPR[q] = 0xFFFFFF;
	}
	EXTI->IMR = 0;
	EXTI->EMR = 0;
	EXTI->RTSR = 0;
	EXTI->FTSR = 0;
	EXTI->SWIER = 0;
	EXTI->PR = (uint32_t)-1;
	SYSCFG->EXTICR[0] = 0;
	SYSCFG->EXTICR[1] = 0;
	SYSCFG->EXTICR[2] = 0;
	SYSCFG->EXTICR[3] = 0;
	core_util_critical_section_exit();

	SCB->VTOR = APP_ADDR;
	__set_MSP(*(uint32_t*)APP_ADDR);
	jump2App();

}
