/**
 * Copyright (c) 2014 - 2017, Nordic Semiconductor ASA
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 * 
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 * 
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 * 
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 * 
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 */
#include "ser_phy.h"
#include "ser_config.h"
#include "app_error.h"
#include "app_util.h"

#include "stm32f4xx.h"

/** ---=== export UART DFU functions ===--- **/

#include "ser_config.h"
#include "ser_app_hal.h"
#include "us_ticker_api.h"

extern uint32_t ser_app_hal_hw_init(ser_app_hal_flash_op_done_handler_t handler);
extern void ser_app_hal_nrf_dfu_pin_clear();
extern void ser_app_hal_nrf_dfu_pin_set();

static bool dfu_mode_enabled = false;
static bool volatile dfu_tx_completed = false;
static bool volatile dfu_byte_received = false;

static void UART_Init(void);
static void UART_DeInit(void);

void dfu_mode_enter() {
	UART_DeInit();
	UART_Init();

	ser_app_hal_hw_init(NULL);

	ser_app_hal_nrf_reset_pin_clear();
	ser_app_hal_nrf_dfu_pin_clear();

	ser_app_hal_delay(CONN_CHIP_RESET_TIME);

	ser_app_hal_nrf_reset_pin_set();

	ser_app_hal_delay(CONN_CHIP_WAKEUP_TIME);

	dfu_mode_enabled = true;
}

void dfu_mode_exit() {
	ser_app_hal_nrf_dfu_pin_set();
	ser_app_hal_nrf_reset_pin_clear();

	/// @note Оставляем модуль в сбросе, потому что всё равно он сбросится еще раз при вызове sdh_enable()
//	ser_app_hal_delay(CONN_CHIP_RESET_TIME);
//
//	ser_app_hal_nrf_reset_pin_set();

//	ser_app_hal_delay(CONN_CHIP_WAKEUP_TIME);

	UART_DeInit();

	dfu_mode_enabled = false;
}

static uint32_t tx(uint8_t *data, uint16_t len);
static uint32_t rx(uint8_t *data, uint16_t len);

uint32_t dfu_tx_data(uint8_t *data, uint16_t len) {
	dfu_tx_completed = false;

	tx(data, len);

	uint32_t start_us = us_ticker_read();
	uint32_t curr_us;

	do {	// Таймаут 1 сек, как в nrfutil
		if (dfu_tx_completed) {
			return NRF_SUCCESS;
		}
		curr_us = us_ticker_read();
	}while (curr_us - start_us < 1000000);

	return NRF_ERROR_TIMEOUT;
}

uint32_t dfu_rx_byte(uint8_t *data) {
	dfu_byte_received = false;

	rx(data, 1);

	uint32_t start_us = us_ticker_read();

	while (us_ticker_read() - start_us < 1000000) {	// Таймаут 1 сек, как в nrfutil
		if (dfu_byte_received) {
			return NRF_SUCCESS;
		}
	}

	return NRF_ERROR_TIMEOUT;
}

/** ---=== Serialisation ===--- **/

static UART_HandleTypeDef huart1;
static DMA_HandleTypeDef hdma_usart1_tx;
static DMA_HandleTypeDef hdma_usart1_rx;

static bool volatile   tx_header_transmited = false;
static uint8_t         m_tx_header_buf[SER_PHY_HEADER_SIZE];
static uint16_t        m_bytes_to_transmit;
static uint8_t const * mp_tx_buffer;

static bool volatile   rx_header_received = false;
static uint8_t         m_rx_header_buf[SER_PHY_HEADER_SIZE];
static uint16_t        m_bytes_to_receive;
static uint8_t         m_rx_drop_buf[1];

static ser_phy_events_handler_t m_ser_phy_event_handler;
static ser_phy_evt_t m_ser_phy_rx_event;

static uint32_t tx(uint8_t *data, uint16_t len) {
	return HAL_UART_Transmit_DMA(&huart1, data, len);
}

static uint32_t rx(uint8_t *data, uint16_t len) {
	return HAL_UART_Receive_DMA(&huart1, data, len);
}


static void packet_sent_callback(void)
{
	static ser_phy_evt_t const event = {
			.evt_type = SER_PHY_EVT_TX_PKT_SENT,
	};
	m_ser_phy_event_handler(event);
}

static void packet_rx_start(void) {
	rx_header_received = false;	///@todo Неправильно делать тут. Надо обработать все ситуации, когда он не может быть сброшен
	rx(m_rx_header_buf, SER_PHY_HEADER_SIZE);
}

static void packet_byte_drop(void) {
	rx(m_rx_drop_buf, 1);
}

static void buffer_request_callback(uint16_t num_of_bytes)
{
	m_ser_phy_rx_event.evt_type = SER_PHY_EVT_RX_BUF_REQUEST;
	m_ser_phy_rx_event.evt_params.rx_buf_request.num_of_bytes = num_of_bytes;
	m_ser_phy_event_handler(m_ser_phy_rx_event);
}

static void packet_dropped_callback(void)
{
	static ser_phy_evt_t const event = {
			.evt_type = SER_PHY_EVT_RX_PKT_DROPPED,
	};
	m_ser_phy_event_handler(event);
}

static void packet_received_callback(void)
{
	m_ser_phy_event_handler(m_ser_phy_rx_event);
}

/** ---=== UART ===--- **/

/**
 * @note	Установлены следующие приоритеты прерываний:
 * 			DMA2_Stream2_IRQn (rx) 				- 3
 * 			DMA2_Stream7_IRQn (tx), USART1_IRQn - 4
 * 			SD_EVT_IRQn 						- 5
 */

void USART1_IRQHandler(void);
void DMA2_Stream7_IRQHandler(void);
void DMA2_Stream2_IRQHandler(void);

static void MX_USART1_UART_Init(void) {
	huart1.Instance = USART1;
	huart1.Init.BaudRate = 115200;
	huart1.Init.WordLength = UART_WORDLENGTH_8B;
	huart1.Init.StopBits = UART_STOPBITS_1;
	huart1.Init.Parity = UART_PARITY_NONE;
	huart1.Init.Mode = UART_MODE_TX_RX;
	huart1.Init.HwFlowCtl = UART_HWCONTROL_RTS_CTS;
//	huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
	huart1.Init.OverSampling = UART_OVERSAMPLING_16;

	if (HAL_UART_Init(&huart1) != HAL_OK) {
		while(1);
	}
}

static void MX_DMA_Init(void) {
  /* DMA controller clock enable */
  __HAL_RCC_DMA2_CLK_ENABLE();

  /* DMA interrupt init */
  /* DMA2_Stream2_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA2_Stream2_IRQn, 3, 0);
  HAL_NVIC_EnableIRQ(DMA2_Stream2_IRQn);
  /* DMA2_Stream7_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(DMA2_Stream7_IRQn, 4, 0);
  HAL_NVIC_EnableIRQ(DMA2_Stream7_IRQn);

}

void HAL_UART_MspInit(UART_HandleTypeDef* uartHandle) {
	GPIO_InitTypeDef GPIO_InitStruct;

	if(uartHandle->Instance == USART1) {
		__HAL_RCC_USART1_CLK_ENABLE();
		__HAL_RCC_GPIOA_CLK_ENABLE();

		/**USART1 GPIO Configuration
		PA9     ------> USART1_TX
		PA10     ------> USART1_RX
		PA11     ------> USART1_CTS
		PA12     ------> USART1_RTS
		*/
		GPIO_InitStruct.Mode = GPIO_MODE_AF_PP;
		GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;
		GPIO_InitStruct.Alternate = GPIO_AF7_USART1;

		GPIO_InitStruct.Pin = GPIO_PIN_9|GPIO_PIN_10;
		GPIO_InitStruct.Pull = GPIO_PULLUP;
		HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

		GPIO_InitStruct.Pin = GPIO_PIN_11|GPIO_PIN_12;
		GPIO_InitStruct.Pull = GPIO_NOPULL;
		HAL_GPIO_Init(GPIOA, &GPIO_InitStruct);

	    /* Peripheral DMA init*/

	    hdma_usart1_tx.Instance = DMA2_Stream7;
	    hdma_usart1_tx.Init.Channel = DMA_CHANNEL_4;
	    hdma_usart1_tx.Init.Direction = DMA_MEMORY_TO_PERIPH;
	    hdma_usart1_tx.Init.PeriphInc = DMA_PINC_DISABLE;
	    hdma_usart1_tx.Init.MemInc = DMA_MINC_ENABLE;
	    hdma_usart1_tx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
	    hdma_usart1_tx.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
	    hdma_usart1_tx.Init.Mode = DMA_NORMAL;
	    hdma_usart1_tx.Init.Priority = DMA_PRIORITY_HIGH;
	    hdma_usart1_tx.Init.FIFOMode = DMA_FIFOMODE_DISABLE;
	    if (HAL_DMA_Init(&hdma_usart1_tx) != HAL_OK) {
	    	while(1);
	    }

	    __HAL_LINKDMA(uartHandle, hdmatx, hdma_usart1_tx);

	    hdma_usart1_rx.Instance = DMA2_Stream2;
	    hdma_usart1_rx.Init.Channel = DMA_CHANNEL_4;
	    hdma_usart1_rx.Init.Direction = DMA_PERIPH_TO_MEMORY;
	    hdma_usart1_rx.Init.PeriphInc = DMA_PINC_DISABLE;
	    hdma_usart1_rx.Init.MemInc = DMA_MINC_ENABLE;
	    hdma_usart1_rx.Init.PeriphDataAlignment = DMA_PDATAALIGN_BYTE;
	    hdma_usart1_rx.Init.MemDataAlignment = DMA_MDATAALIGN_BYTE;
	    hdma_usart1_rx.Init.Mode = DMA_NORMAL;
	    hdma_usart1_rx.Init.Priority = DMA_PRIORITY_HIGH;
	    hdma_usart1_rx.Init.FIFOMode = DMA_FIFOMODE_DISABLE;
	    if (HAL_DMA_Init(&hdma_usart1_rx) != HAL_OK) {
	    	while(1);
	    }

	    __HAL_LINKDMA(uartHandle, hdmarx, hdma_usart1_rx);

		HAL_NVIC_SetPriority(USART1_IRQn, 4, 0);
		HAL_NVIC_EnableIRQ(USART1_IRQn);
	}
}

void HAL_UART_MspDeInit(UART_HandleTypeDef* uartHandle) {
	__HAL_RCC_USART1_CLK_DISABLE();

	HAL_DMA_DeInit(uartHandle->hdmatx);
	HAL_DMA_DeInit(uartHandle->hdmarx);

//	HAL_NVIC_DisableIRQ(USART1_IRQn);
}

static void UART_Init(void) {
	MX_DMA_Init();
	MX_USART1_UART_Init();
}

static void UART_DeInit(void) {
	HAL_UART_DMAStop(&huart1);
	HAL_UART_DeInit(&huart1);
}

void USART1_IRQHandler(void) {
	HAL_UART_IRQHandler(&huart1);
}

void DMA2_Stream7_IRQHandler(void) {
	HAL_DMA_IRQHandler(&hdma_usart1_tx);
}

void DMA2_Stream2_IRQHandler(void) {
	HAL_DMA_IRQHandler(&hdma_usart1_rx);
}

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart) {
	if(huart->Instance == USART1) {
		switch (dfu_mode_enabled) {
		case false:
			if (!tx_header_transmited) {
				tx_header_transmited = true;
				tx((uint8_t*)mp_tx_buffer, m_bytes_to_transmit);
			}
			else {
				tx_header_transmited = false;
				packet_sent_callback();
			}
		break;

		default:
			dfu_tx_completed = true;
		break;
		}
	}
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart) {
	if(huart->Instance == USART1) {
		switch (dfu_mode_enabled) {
		case false:
			if (!rx_header_received) {
				rx_header_received = true;

				m_bytes_to_receive = uint16_decode(m_rx_header_buf);
				buffer_request_callback(m_bytes_to_receive);
			}
			else if (huart->hdmarx->Instance->M0AR == (uint32_t)m_rx_drop_buf) {
				// Если принимаем отсекаемые байты, то так и продолжаем по байту, пока не закончим
				if (--m_bytes_to_receive != 0) {
					packet_byte_drop();
				}
				else {
					packet_dropped_callback();
					packet_rx_start();
				}
			}
			else {
				packet_received_callback();
				packet_rx_start();
			}
		break;

		default:
			dfu_byte_received = true;
		break;
		}
	}
}

/** ---=== API FUNCTIONS ===--- */

uint32_t ser_phy_open(ser_phy_events_handler_t events_handler) {
    if (events_handler == NULL) {
        return NRF_ERROR_NULL;
    }

    // Check if function was not called before.
    if (m_ser_phy_event_handler != NULL) {
        return NRF_ERROR_INVALID_STATE;
    }

    UART_Init();

    m_ser_phy_event_handler = events_handler;

    packet_rx_start();

    return NRF_SUCCESS;
}

uint32_t ser_phy_tx_pkt_send(const uint8_t * p_buffer, uint16_t num_of_bytes) {
    if (p_buffer == NULL) {
        return NRF_ERROR_NULL;
    }
    else if (num_of_bytes == 0) {
        return NRF_ERROR_INVALID_PARAM;
    }
    else if (0) {
    	///\todo
    	return NRF_ERROR_BUSY;
    }

    (void)uint16_encode(num_of_bytes, m_tx_header_buf);
    m_bytes_to_transmit = num_of_bytes;
    mp_tx_buffer = p_buffer;

    tx_header_transmited = false;	///@todo Неправильно делать тут. Надо обработать все ситуации, когда он не может быть сброшен
    tx(m_tx_header_buf, SER_PHY_HEADER_SIZE);

    return NRF_SUCCESS;
}

uint32_t ser_phy_rx_buf_set(uint8_t * p_buffer) {
    if (m_ser_phy_rx_event.evt_type != SER_PHY_EVT_RX_BUF_REQUEST) {
        return NRF_ERROR_INVALID_STATE;
    }

    m_ser_phy_rx_event.evt_type = SER_PHY_EVT_RX_PKT_RECEIVED;
    m_ser_phy_rx_event.evt_params.rx_pkt_received.p_buffer = p_buffer;
    m_ser_phy_rx_event.evt_params.rx_pkt_received.num_of_bytes = m_bytes_to_receive;

    // If there is not enough memory to receive the packet (no buffer was
    // provided), drop its data byte by byte (using an internal 1-byte buffer).
    if (p_buffer == NULL) {
        packet_byte_drop();
    }
    else {
    	rx(p_buffer, m_bytes_to_receive);
    }

    return NRF_SUCCESS;
}

void ser_phy_close(void) {
    m_ser_phy_event_handler = NULL;
}

void ser_phy_interrupts_enable(void) {
	HAL_NVIC_EnableIRQ(DMA2_Stream2_IRQn);
	HAL_NVIC_EnableIRQ(DMA2_Stream7_IRQn);
	HAL_NVIC_EnableIRQ(USART1_IRQn);
}

void ser_phy_interrupts_disable(void) {
	HAL_NVIC_DisableIRQ(DMA2_Stream2_IRQn);
	HAL_NVIC_DisableIRQ(DMA2_Stream7_IRQn);
	HAL_NVIC_DisableIRQ(USART1_IRQn);
}




