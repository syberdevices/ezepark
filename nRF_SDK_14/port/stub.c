/*
 * stub.c
 *
 *  Created on: 3 мая 2017 г.
 *      Author: Chuyec
 */

#include <stdint.h>
#include <stddef.h>
#include "compiler_abstraction.h"
#include "stm32f407xx.h"
#include "nrf_fstorage.h"

static uint32_t m_in_critical_region = 0;

void app_error_fault_handler(uint32_t id, uint32_t pc, uint32_t info)
{
	UNUSED_PARAMETER(id);
	UNUSED_PARAMETER(pc);
	UNUSED_PARAMETER(info);

	if (CoreDebug->DHCSR & 1)  {
		__BKPT (0);
	}
}

uint32_t sd_app_evt_wait(void)
{
//    __WFE();
//    return NRF_SUCCESS;
	return 0;
}

void app_util_critical_region_enter(void) {
	__disable_irq();
	m_in_critical_region++;
}

void app_util_critical_region_exit(void) {
    m_in_critical_region--;
    if (m_in_critical_region == 0)
    {
        __enable_irq();
    }
}


