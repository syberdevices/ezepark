/**
 * Copyright (c) 2012 - 2017, Nordic Semiconductor ASA
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 * 
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 * 
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 * 
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 * 
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 */
#include <nrf.h>
#include "sdk_common.h"
#if NRF_MODULE_ENABLED(APP_TIMER)
#include "app_timer.h"
#include <stdlib.h>
#include "nrf_soc.h"
#include "app_error.h"
//#include "nrf_delay.h"
#include "app_util_platform.h"
#if APP_TIMER_CONFIG_USE_SCHEDULER
#include "app_scheduler.h"
#endif


extern void app_rtos_tmr_create(app_timer_timeout_handler_t p_timeout_handler);
extern uint32_t app_rtos_tmr_start(uint32_t timeout_ticks);
extern uint32_t app_rtos_tmr_stop(void);


ret_code_t app_timer_init(void)
{
    return NRF_SUCCESS;
}


ret_code_t app_timer_create(app_timer_id_t const *      p_timer_id,
                            app_timer_mode_t            mode,
                            app_timer_timeout_handler_t timeout_handler)
{
	app_rtos_tmr_create(timeout_handler);

	UNUSED_PARAMETER(p_timer_id);
	UNUSED_PARAMETER(mode);
	UNUSED_PARAMETER(timeout_handler);
	return NRF_SUCCESS;
}

ret_code_t app_timer_start(app_timer_id_t timer_id, uint32_t timeout_ticks, void * p_context)
{
//	app_rtos_tmr_start(timeout_ticks);

	UNUSED_PARAMETER(timer_id);
	UNUSED_PARAMETER(timeout_ticks);
	UNUSED_PARAMETER(p_context);
	return NRF_SUCCESS;
}


ret_code_t app_timer_stop(app_timer_id_t timer_id)
{
//	app_rtos_tmr_stop();

	UNUSED_PARAMETER(timer_id);
	return NRF_SUCCESS;
}


ret_code_t app_timer_stop_all(void)
{
//    // Check state
//    VERIFY_MODULE_INITIALIZED();
//
//    return timer_stop_op_schedule(NULL, TIMER_USER_OP_TYPE_STOP_ALL);
	return NRF_SUCCESS;
}


uint32_t app_timer_cnt_get(void)
{
//    return rtc1_counter_get();
	return 1;
}


uint32_t app_timer_cnt_diff_compute(uint32_t   ticks_to,
                                    uint32_t   ticks_from)
{
//    return ticks_diff_get(ticks_to, ticks_from);

	UNUSED_PARAMETER(ticks_to);
	UNUSED_PARAMETER(ticks_from);
	return 1;
}

#if APP_TIMER_WITH_PROFILER
uint8_t app_timer_op_queue_utilization_get(void)
{
    return m_max_user_op_queue_utilization;
}
#endif

void app_timer_pause(void)
{
//    NRF_RTC1->TASKS_STOP = 1;
}

void app_timer_resume(void)
{
//    NRF_RTC1->TASKS_START = 1;
}

#endif //NRF_MODULE_ENABLED(APP_TIMER)
