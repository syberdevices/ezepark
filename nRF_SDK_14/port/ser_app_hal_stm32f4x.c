/**
 * Copyright (c) 2014 - 2017, Nordic Semiconductor ASA
 * 
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 
 * 2. Redistributions in binary form, except as embedded into a Nordic
 *    Semiconductor ASA integrated circuit in a product or a software update for
 *    such product, must reproduce the above copyright notice, this list of
 *    conditions and the following disclaimer in the documentation and/or other
 *    materials provided with the distribution.
 * 
 * 3. Neither the name of Nordic Semiconductor ASA nor the names of its
 *    contributors may be used to endorse or promote products derived from this
 *    software without specific prior written permission.
 * 
 * 4. This software, with or without modification, must only be used with a
 *    Nordic Semiconductor ASA integrated circuit.
 * 
 * 5. Any software provided in binary form under this license must not be reverse
 *    engineered, decompiled, modified and/or disassembled.
 * 
 * THIS SOFTWARE IS PROVIDED BY NORDIC SEMICONDUCTOR ASA "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY, NONINFRINGEMENT, AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL NORDIC SEMICONDUCTOR ASA OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
 * OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 */
#include <nrf.h>

#include "app_util_platform.h"
#include "ser_app_hal.h"
#include "nrf_soc.h"
#include "ser_phy.h"

#include "stm32f4xx.h"
#include "stm32f4xx_hal_def.h"
#include "stm32f4xx_hal_gpio.h"

#define SOFTDEVICE_EVT_IRQ      SD_EVT_IRQn         /**< SoftDevice Event IRQ number. Used for both protocol events and SoC events. */
//#define FLASH_WRITE_MAX_LENGTH  ((uint16_t)NRF_FICR->CODEPAGESIZE)
//#define BLE_FLASH_PAGE_SIZE     ((uint16_t)NRF_FICR->CODEPAGESIZE)  /**< Size of one flash page. */

#define BLE_RESET_PORT		GPIOA
#define BLE_RESET_PIN		GPIO_PIN_8
#define BLE_DFU_PORT		GPIOG
#define BLE_DFU_PIN			GPIO_PIN_8


void unused(void) {
#if defined(DEBUG)
	__BKPT(0);
#endif
}

static ser_app_hal_flash_op_done_handler_t m_flash_op_handler;

static void gpio_clk_enable(uint32_t periph_addr) {
	switch(periph_addr) {
	case GPIOA_BASE: __HAL_RCC_GPIOA_CLK_ENABLE(); break;
	case GPIOB_BASE: __HAL_RCC_GPIOB_CLK_ENABLE(); break;
	case GPIOC_BASE: __HAL_RCC_GPIOC_CLK_ENABLE(); break;
	case GPIOD_BASE: __HAL_RCC_GPIOD_CLK_ENABLE(); break;
	case GPIOE_BASE: __HAL_RCC_GPIOE_CLK_ENABLE(); break;
	case GPIOF_BASE: __HAL_RCC_GPIOF_CLK_ENABLE(); break;
	case GPIOG_BASE: __HAL_RCC_GPIOG_CLK_ENABLE(); break;
	case GPIOH_BASE: __HAL_RCC_GPIOH_CLK_ENABLE(); break;
	case GPIOI_BASE: __HAL_RCC_GPIOI_CLK_ENABLE(); break;
	default: break;
	}
}

void ser_app_hal_nrf_dfu_pin_set();

uint32_t ser_app_hal_hw_init(ser_app_hal_flash_op_done_handler_t handler)
{
	m_flash_op_handler = handler;

	gpio_clk_enable((uint32_t)BLE_RESET_PORT);
	gpio_clk_enable((uint32_t)BLE_DFU_PORT);

	GPIO_InitTypeDef GPIO_InitStruct;
	GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
	GPIO_InitStruct.Pull = GPIO_PULLUP;
	GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_MEDIUM;

	GPIO_InitStruct.Pin = BLE_RESET_PIN;
	HAL_GPIO_Init(BLE_RESET_PORT, &GPIO_InitStruct);

	GPIO_InitStruct.Pin = BLE_DFU_PIN;
	HAL_GPIO_Init(BLE_DFU_PORT, &GPIO_InitStruct);

	ser_app_hal_nrf_dfu_pin_set();

    return NRF_SUCCESS;
}

void ser_app_hal_delay(uint32_t ms)
{
	HAL_Delay(ms);
}

void ser_app_hal_nrf_reset_pin_clear()
{
	HAL_GPIO_WritePin(BLE_RESET_PORT, BLE_RESET_PIN, GPIO_PIN_RESET);
}

void ser_app_hal_nrf_reset_pin_set()
{
	HAL_GPIO_WritePin(BLE_RESET_PORT, BLE_RESET_PIN, GPIO_PIN_SET);
}

void ser_app_hal_nrf_dfu_pin_clear()
{
	HAL_GPIO_WritePin(BLE_DFU_PORT, BLE_DFU_PIN, GPIO_PIN_RESET);
}

void ser_app_hal_nrf_dfu_pin_set()
{
	HAL_GPIO_WritePin(BLE_DFU_PORT, BLE_DFU_PIN, GPIO_PIN_SET);
}

void ser_app_hal_nrf_evt_irq_priority_set()
{
	HAL_NVIC_SetPriority(SOFTDEVICE_EVT_IRQ, 5, 0);
}

void ser_app_hal_nrf_evt_pending()
{
    NVIC_SetPendingIRQ(SOFTDEVICE_EVT_IRQ);
}

uint32_t sd_ppi_channel_enable_get(uint32_t * p_channel_enable)
{
	unused();
//    *p_channel_enable = NRF_PPI->CHEN;
    return NRF_SUCCESS;
}

uint32_t sd_ppi_channel_enable_set(uint32_t channel_enable_set_msk)
{
	unused();
//    NRF_PPI->CHEN = channel_enable_set_msk;
    return NRF_SUCCESS;
}

uint32_t sd_ppi_channel_assign(uint8_t               channel_num,
                               const volatile void * evt_endpoint,
                               const volatile void * task_endpoint)
{
	unused();
//    NRF_PPI->CH[channel_num].TEP = (uint32_t)task_endpoint;
//    NRF_PPI->CH[channel_num].EEP = (uint32_t)evt_endpoint;
    return NRF_SUCCESS;
}

uint32_t sd_flash_page_erase(uint32_t page_number)
{
	unused();
//    uint32_t * p_page = (uint32_t *)(BLE_FLASH_PAGE_SIZE * page_number);
//
//    if (!addr_is_in_FLASH(p_page))
//    {
//        return NRF_ERROR_INVALID_ADDR;
//    }
//
//    nrf_nvmc_page_erase((uint32_t) p_page);
//    m_flash_op_handler(true);
    return NRF_SUCCESS;
}

uint32_t sd_flash_write(uint32_t * const p_dst, uint32_t const * const p_src, uint32_t size)
{
	unused();
//    if (size > FLASH_WRITE_MAX_LENGTH)
//    {
//        return NRF_ERROR_INVALID_LENGTH;
//    }
//
//    if (!addr_is_in_FLASH(p_dst))
//    {
//        return NRF_ERROR_INVALID_ADDR;
//    }
//
//    nrf_nvmc_write_words((uint32_t) p_dst, p_src, size);
//    m_flash_op_handler(true);
    return NRF_SUCCESS;
}
