/* Copyright (c) 2012 Nordic Semiconductor. All Rights Reserved.
 *
 * The information contained herein is property of Nordic Semiconductor ASA.
 * Terms and conditions of usage are described in detail in NORDIC
 * SEMICONDUCTOR STANDARD SOFTWARE LICENSE AGREEMENT.
 *
 * Licensees are granted free, non-transferable use of the information. NO
 * WARRANTY of ANY KIND is provided. This heading must NOT be removed from
 * the file.
 *
 */

/** @file
 *
 * @brief Barrier control module.
 *
 * @details This module implements the barrier control module with two characteristics: command and response.

 * @note The application must propagate BLE stack events to the Service module by calling
 *       ble_barrier_on_ble_evt() from the from the @ref ble_stack_handler callback.
 *
 * @note Attention! 
 *  To maintain compliance with Nordic Semiconductor ASA Bluetooth profile 
 *  qualification listings, this section of source code must not be modified.
 */

#ifndef BLE_DOS_H__
#define BLE_DOS_H__

#include <stdint.h>
#include <stdbool.h>
#include "ble.h"
#include "ble_srv_common.h"

#ifdef __cplusplus
extern "C" {
#endif

#define BLE_DOS_BLE_OBSERVER_PRIO		2

/**@brief   Macro for defining a ble_bcs instance.
 *
 * @param   _name   Name of the instance.
 * @hideinitializer
 */
#define BLE_DOS_DEF(_name)                                                                          \
static ble_dos_t _name;                                                                             \
NRF_SDH_BLE_OBSERVER(_name ## _obs,                                                                 \
                     BLE_DOS_BLE_OBSERVER_PRIO,                                                     \
                     ble_dos_on_ble_evt, &_name)

#define DOS_UUID_BASE {0x23, 0xD1, 0xBC, 0xEA, 0x5F, 0x78, 0x23, 0x15, 0xDE, 0xEF, 0x12, 0x12, 0x00, 0x00, 0x00, 0x00}
#define DOS_UUID_SERVICE 	0x0600

#define SELFTEST_UUID_CHAR		0x0601
#define SELFTEST_UUID_CHAR_LEN	1

#define MODE_UUID_CHAR			0x0602
#define MODE_UUID_CHAR_LEN		1

#define LOGGING_UUID_CHAR		0x0603
#define LOGGING_UUID_CHAR_LEN	1

	
/**@brief  Service event type. */
typedef enum
{
    BLE_DOS_EVT_NOTIFICATION_ENABLED,                             
    BLE_DOS_EVT_NOTIFICATION_DISABLED                             
} ble_dos_evt_type_t;

/**@brief Service event. */
typedef struct
{
    ble_dos_evt_type_t evt_type;                                  /**< Type of event. */
} ble_dos_evt_t;

// Forward declaration of the ble_dos_t type. 
typedef struct ble_dos_s ble_dos_t;

/**@brief Service event handler type. */
typedef void (*ble_dos_evt_handler_t) (ble_dos_t * p_dos, ble_gatts_evt_write_t const * p_evt_write);

/**@brief Service init structure. This contains all options and data needed for
 *        initialization of the service.*/
typedef struct
{
    ble_dos_evt_handler_t     	  dos_write_handler;            /**< Event handler to be called for handling events in Service. */
    bool                          support_notification;           /**< TRUE if notification is supported. */
    ble_srv_report_ref_t *        p_report_ref;
    uint8_t             *         initial_value;             /**< Initial value  */
    ble_srv_cccd_security_mode_t  dos_char_attr_md;     /**< Initial security level for characteristics attribute */
    ble_gap_conn_sec_mode_t       dos_report_read_perm; /**< Initial security level for report read attribute */
} ble_dos_init_t;

/**@brief Service structure. This contains various status information for the service. */
typedef struct ble_dos_s
{		
    ble_dos_evt_handler_t    	  	dos_evt_handler;            /**< Event handler to be called for handling events in the Service. */		
    uint16_t                      	service_handle;                 /**< Handle of  Service (as provided by the BLE stack). */
    ble_gatts_char_handles_t      	logging_handles;
	ble_gatts_char_handles_t      	mode_handles;
	ble_gatts_char_handles_t      	selftest_handles;
	uint8_t 						uuid_type;
    uint16_t                      	report_ref_handle;              /**< Handle of the Report Reference descriptor. */
    uint16_t                      	conn_handle;                    /**< Handle of the current connection (as provided by the BLE stack, is BLE_CONN_HANDLE_INVALID if not in a connection). */
    bool                          	is_notification_supported;      /**< TRUE if notification of  Level is supported. */		
	ble_dos_evt_handler_t			dos_write_handler;
} ble_dos_t;

/**@brief Function for initializing the  Service.
 *
 * @param[out]  p_dos       Service structure. This structure will have to be supplied by
 *                          the application. It will be initialized by this function, and will later
 *                          be used to identify this particular service instance.
 * @param[in]   p_dos_init  Information needed to initialize the service.
 *
 * @return      NRF_SUCCESS on successful initialization of service, otherwise an error code.
 */
uint32_t ble_dos_init(ble_dos_t * p_dos, const ble_dos_init_t * p_dos_init);

/**@brief Function for handling the Application's BLE Stack events.
 *
 * @details Handles all events from the BLE stack of interest to the  Service.
 *
 * @param[in]   p_dos      Service structure.
 * @param[in]   p_context  Device Operation Service structure.
 */
void ble_dos_on_ble_evt(ble_evt_t const * p_ble_evt, void * p_context);

/**@brief Function for updating temperature value.
 *

 *
 * @return      NRF_SUCCESS on success, otherwise an error code.
 */
uint32_t ble_selftest_result_update(ble_dos_t * p_dos, uint8_t result);
#ifdef __cplusplus
}
#endif

#endif // BLE_BLE_DOS_H__

/** @} */
