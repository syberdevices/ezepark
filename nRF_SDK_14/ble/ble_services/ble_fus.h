/* Copyright (c) 2012 Nordic Semiconductor. All Rights Reserved.
 *
 * The information contained herein is property of Nordic Semiconductor ASA.
 * Terms and conditions of usage are described in detail in NORDIC
 * SEMICONDUCTOR STANDARD SOFTWARE LICENSE AGREEMENT.
 *
 * Licensees are granted free, non-transferable use of the information. NO
 * WARRANTY of ANY KIND is provided. This heading must NOT be removed from
 * the file.
 *
 */

/** @file
 *
 * @brief Barrier control module.
 *
 * @details This module implements the barrier control module with two characteristics: command and response.

 * @note The application must propagate BLE stack events to the Service module by calling
 *       ble_barrier_on_ble_evt() from the from the @ref ble_stack_handler callback.
 *
 * @note Attention! 
 *  To maintain compliance with Nordic Semiconductor ASA Bluetooth profile 
 *  qualification listings, this section of source code must not be modified.
 */

#ifndef BLE_FUS_H__
#define BLE_FUS_H__

#include <stdint.h>
#include <stdbool.h>
#include "ble.h"
#include "ble_srv_common.h"

#ifdef __cplusplus
extern "C" {
#endif

#define BLE_FUS_BLE_OBSERVER_PRIO		2

/**@brief   Macro for defining a ble_bcs instance.
 *
 * @param   _name   Name of the instance.
 * @hideinitializer
 */
#define BLE_FUS_DEF(_name)                                                                          \
static ble_fus_t _name;                                                                             \
NRF_SDH_BLE_OBSERVER(_name ## _obs,                                                                 \
                     BLE_FUS_BLE_OBSERVER_PRIO,                                                     \
                     ble_fus_on_ble_evt, &_name)

#define FUS_UUID_BASE {0x23, 0xD1, 0xBC, 0xEA, 0x5F, 0x78, 0x23, 0x15, 0xDE, 0xEF, 0x12, 0x12, 0x00, 0x00, 0x00, 0x00}
#define FUS_UUID_SERVICE 	0x0400

#define FWDATA_UUID_CHAR		0x0401
#define FWDATA_UUID_CHAR_LEN	20

#define FWSTATUS_UUID_CHAR		0x0402
#define FWSTATUS_UUID_CHAR_LEN	1

#define FWVER_UUID_CHAR			0x0403
#define FWVER_UUID_CHAR_LEN		3

	
/**@brief  Service event type. */
typedef enum
{
    BLE_FUS_EVT_NOTIFICATION_ENABLED,                             
    BLE_FUS_EVT_NOTIFICATION_DISABLED                             
} ble_fus_evt_type_t;

/**@brief Service event. */
typedef struct
{
    ble_fus_evt_type_t evt_type;                                  /**< Type of event. */
} ble_fus_evt_t;

// Forward declaration of the ble_fus_t type. 
typedef struct ble_fus_s ble_fus_t;

/**@brief Service event handler type. */
typedef void (*ble_fus_evt_handler_t) (ble_fus_t * p_fus, ble_gatts_evt_write_t const * p_evt_write);

/**@brief Service init structure. This contains all options and data needed for
 *        initialization of the service.*/
typedef struct
{
    ble_fus_evt_handler_t     	  fus_write_handler;            /**< Event handler to be called for handling events in Service. */
    bool                          support_notification;           /**< TRUE if notification is supported. */
    ble_srv_report_ref_t *        p_report_ref;
    uint8_t             *         initial_value;             /**< Initial value  */
    ble_srv_cccd_security_mode_t  fus_char_attr_md;     /**< Initial security level for characteristics attribute */
    ble_gap_conn_sec_mode_t       fus_report_read_perm; /**< Initial security level for report read attribute */
} ble_fus_init_t;

/**@brief Service structure. This contains various status information for the service. */
typedef struct ble_fus_s
{		
    ble_fus_evt_handler_t    	  	fus_evt_handler;            /**< Event handler to be called for handling events in the Service. */		
    uint16_t                      	service_handle;                 /**< Handle of  Service (as provided by the BLE stack). */
    ble_gatts_char_handles_t      	fwdata_handles;
    ble_gatts_char_handles_t      	fwstatus_handles;
	ble_gatts_char_handles_t      	fwver_handles;
	uint8_t 						uuid_type;
    uint16_t                      	report_ref_handle;              /**< Handle of the Report Reference descriptor. */
    uint16_t                      	conn_handle;                    /**< Handle of the current connection (as provided by the BLE stack, is BLE_CONN_HANDLE_INVALID if not in a connection). */
    bool                          	is_notification_supported;      /**< TRUE if notification of  Level is supported. */		
	ble_fus_evt_handler_t			fus_write_handler;
} ble_fus_t;

/**@brief Function for initializing the  Service.
 *
 * @param[out]  p_fus       Service structure. This structure will have to be supplied by
 *                          the application. It will be initialized by this function, and will later
 *                          be used to identify this particular service instance.
 * @param[in]   p_fus_init  Information needed to initialize the service.
 *
 * @return      NRF_SUCCESS on successful initialization of service, otherwise an error code.
 */
uint32_t ble_fus_init(ble_fus_t * p_fus, const ble_fus_init_t * p_fus_init);

/**@brief Function for handling the Application's BLE Stack events.
 *
 * @details Handles all events from the BLE stack of interest to the  Service.
 *
 * @param[in]   p_ble_evt  Event received from the BLE stack.
 * @param[in]   p_context  Firmware Update Service structure.
 */
void ble_fus_on_ble_evt(ble_evt_t const * p_ble_evt, void * p_context);

uint32_t ble_fus_status_update(ble_fus_t * p_ble_fus, uint8_t status);

#ifdef __cplusplus
}
#endif

#endif // BLE_BLE_FUS_H__

/** @} */
