/* Copyright (c) 2012 Nordic Semiconductor. All Rights Reserved.
 *
 * The information contained herein is property of Nordic Semiconductor ASA.
 * Terms and conditions of usage are described in detail in NORDIC
 * SEMICONDUCTOR STANDARD SOFTWARE LICENSE AGREEMENT.
 *
 * Licensees are granted free, non-transferable use of the information. NO
 * WARRANTY of ANY KIND is provided. This heading must NOT be removed from
 * the file.
 *
 */

/* Attention! 	
*  To maintain compliance with Nordic Semiconductor ASA�s Bluetooth profile 
*  qualification listings, this section of source code must not be modified.
*/

#include "ble_sss.h"
#include <string.h>
#include "nordic_common.h"
#include "ble_srv_common.h"
#include "app_util.h"


/**@brief Function for handling the Connect event.
 *
 * @param[in]   p_ble_sss       Service structure.
 * @param[in]   p_ble_evt   Event received from the BLE stack.
 */
static void on_connect(ble_sss_t * p_ble_sss, ble_evt_t const * p_ble_evt)
{
    p_ble_sss->conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
}

/**@brief Disconnect event handler.
 *
 * @param[in]   p_lbs       LEDButton Service structure.
 * @param[in]   p_ble_evt   Event received from the BLE stack.
 */
static void on_disconnect(ble_sss_t * p_ble_sss, ble_evt_t const * p_ble_evt)
{
    UNUSED_PARAMETER(p_ble_evt);
    p_ble_sss->conn_handle = BLE_CONN_HANDLE_INVALID;
}

/**@brief Function for handling the Write event.
 *
 * @param[in]   p_ble_sss   Service structure.
 * @param[in]   p_ble_evt   Event received from the BLE stack.
 */
static void on_write(ble_sss_t * p_ble_sss, ble_evt_t const * p_ble_evt)
{
	ble_gatts_evt_write_t const * p_evt_write = &p_ble_evt->evt.gatts_evt.params.write;

	if ((p_evt_write->len > 0) && (p_ble_sss->sss_write_handler != NULL)) {
		p_ble_sss->sss_write_handler(p_ble_sss, p_evt_write);
	}
}

void ble_sss_on_ble_evt(ble_evt_t const * p_ble_evt, void * p_context)
{
	ble_sss_t * p_sss = (ble_sss_t *) p_context;

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
            on_connect(p_sss, p_ble_evt);
            break;

        case BLE_GAP_EVT_DISCONNECTED:
            on_disconnect(p_sss, p_ble_evt);
            break;

        case BLE_GATTS_EVT_WRITE:
            on_write(p_sss, p_ble_evt);
            break;

        default:
            // No implementation needed.
            break;
    }
}

/**@brief Add transmitter power characteristic.
 *
 * @param[in]   p_ble_sss        Service structure.
 * @param[in]   p_ble_sss_init   Information needed to initialize the service.
 *
 * @return      NRF_SUCCESS on success, otherwise an error code.
 */
static uint32_t txpower_char_add(ble_sss_t * p_ble_sss, const ble_sss_init_t * p_ble_sss_init)
{
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;
//	ble_gatts_attr_md_t cccd_md;
	
    memset(&char_md, 0, sizeof(char_md));
    char_md.char_props.write  = 1;
    char_md.char_props.read	  = 1;
    char_md.p_char_user_desc  = NULL;
    char_md.p_char_pf         = NULL;
    char_md.p_user_desc_md    = NULL;
    char_md.p_sccd_md         = NULL;
    
    ble_uuid.type = p_ble_sss->uuid_type;
    ble_uuid.uuid = TXPOWER_UUID_CHAR;
    
    memset(&attr_md, 0, sizeof(attr_md));
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);
    attr_md.vloc       = BLE_GATTS_VLOC_STACK;
    attr_md.rd_auth    = 0;
    attr_md.wr_auth    = 0;
    attr_md.vlen       = 0;
    
    memset(&attr_char_value, 0, sizeof(attr_char_value));
    attr_char_value.p_uuid       = &ble_uuid;
    attr_char_value.p_attr_md    = &attr_md;
    attr_char_value.init_len     = 0;
    attr_char_value.init_offs    = 0;
    attr_char_value.max_len      = TXPOWER_UUID_CHAR_LEN;
    attr_char_value.p_value      = NULL;
    
    return sd_ble_gatts_characteristic_add(p_ble_sss->service_handle, &char_md,
                                               &attr_char_value,
                                               &p_ble_sss->txpower_handles);
}

/**@brief Add beacon power threshold characteristic.
 *
 * @param[in]   p_ble_sss        Service structure.
 * @param[in]   p_ble_sss_init   Information needed to initialize the service.
 *
 * @return      NRF_SUCCESS on success, otherwise an error code.
 */
static uint32_t beaconthr_char_add(ble_sss_t * p_ble_sss, const ble_sss_init_t * p_ble_sss_init)
{
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;
//	ble_gatts_attr_md_t cccd_md;
	
    memset(&char_md, 0, sizeof(char_md));
    char_md.char_props.write  = 1;
    char_md.char_props.read	  = 1;
    char_md.p_char_user_desc  = NULL;
    char_md.p_char_pf         = NULL;
    char_md.p_user_desc_md    = NULL;
    char_md.p_sccd_md         = NULL;
    
    ble_uuid.type = p_ble_sss->uuid_type;
    ble_uuid.uuid = BEACONTHR_UUID_CHAR;
    
    memset(&attr_md, 0, sizeof(attr_md));
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);
    attr_md.vloc       = BLE_GATTS_VLOC_STACK;
    attr_md.rd_auth    = 0;
    attr_md.wr_auth    = 0;
    attr_md.vlen       = 0;
    
    memset(&attr_char_value, 0, sizeof(attr_char_value));
    attr_char_value.p_uuid       = &ble_uuid;
    attr_char_value.p_attr_md    = &attr_md;
    attr_char_value.init_len     = 0;
    attr_char_value.init_offs    = 0;
    attr_char_value.max_len      = BEACONTHR_UUID_CHAR_LEN;
    attr_char_value.p_value      = NULL;
    
    return sd_ble_gatts_characteristic_add(p_ble_sss->service_handle, &char_md,
                                               &attr_char_value,
                                               &p_ble_sss->beaconthr_handles);
}

uint32_t ble_sss_init(ble_sss_t * p_ble_sss, const ble_sss_init_t * p_ble_sss_init)
{
  uint32_t   err_code;
  ble_uuid_t ble_uuid;
	
	//Initialize service structure
	p_ble_sss->sss_write_handler = p_ble_sss_init->sss_write_handler;
	
	ble_uuid128_t base_uuid = {SSS_UUID_BASE};
	p_ble_sss->conn_handle = BLE_CONN_HANDLE_INVALID;
	err_code = sd_ble_uuid_vs_add(&base_uuid, &p_ble_sss->uuid_type);
	if (err_code != NRF_SUCCESS) {
		goto exit;
	}
	
	ble_uuid.type = p_ble_sss->uuid_type;
	ble_uuid.uuid = SSS_UUID_SERVICE;
	err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY, &ble_uuid,	&p_ble_sss->service_handle);
	if (err_code != NRF_SUCCESS) {
		goto exit;
	}
	
	/* Add transmitter power characteristic */
	err_code = txpower_char_add(p_ble_sss, p_ble_sss_init);
	if (err_code != NRF_SUCCESS) {
		goto exit;
	}
	
	/* Add beacon power threshold characteristic */
	err_code = beaconthr_char_add(p_ble_sss, p_ble_sss_init);
	if (err_code != NRF_SUCCESS) {
		goto exit;
	}
	
	exit:

	return err_code;
}
