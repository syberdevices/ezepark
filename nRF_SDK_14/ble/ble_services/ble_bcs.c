/* Copyright (c) 2012 Nordic Semiconductor. All Rights Reserved.
 *
 * The information contained herein is property of Nordic Semiconductor ASA.
 * Terms and conditions of usage are described in detail in NORDIC
 * SEMICONDUCTOR STANDARD SOFTWARE LICENSE AGREEMENT.
 *
 * Licensees are granted free, non-transferable use of the information. NO
 * WARRANTY of ANY KIND is provided. This heading must NOT be removed from
 * the file.
 *
 */

/* Attention! 	
*  To maintain compliance with Nordic Semiconductor ASA�s Bluetooth profile 
*  qualification listings, this section of source code must not be modified.
*/

#include "ble_bcs.h"
#include <string.h>
#include "nordic_common.h"
#include "ble_srv_common.h"
#include "app_util.h"

/**@brief Function for handling the Connect event.
 *
 * @param[in]   p_ble_bcs       Service structure.
 * @param[in]   p_ble_evt   Event received from the BLE stack.
 */
static void on_connect(ble_bcs_t * p_ble_bcs, ble_evt_t const * p_ble_evt)
{
    p_ble_bcs->conn_handle = p_ble_evt->evt.gap_evt.conn_handle;
}

/**@brief Disconnect event handler.
 *
 * @param[in]   p_lbs       LEDButton Service structure.
 * @param[in]   p_ble_evt   Event received from the BLE stack.
 */
static void on_disconnect(ble_bcs_t * p_ble_bcs, ble_evt_t const * p_ble_evt)
{
    UNUSED_PARAMETER(p_ble_evt);
    p_ble_bcs->conn_handle = BLE_CONN_HANDLE_INVALID;
}

/**@brief Function for handling the Write event.
 *
 * @param[in]   p_ble_bcs    Service structure.
 * @param[in]   p_ble_evt   Event received from the BLE stack.
 */
static void on_write(ble_bcs_t * p_ble_bcs, ble_evt_t const * p_ble_evt)
{
	ble_gatts_evt_write_t const * p_evt_write = &p_ble_evt->evt.gatts_evt.params.write;

	if ((p_evt_write->len > 0) && (p_ble_bcs->bcs_write_handler != NULL)) {
		p_ble_bcs->bcs_write_handler(p_ble_bcs, p_evt_write);
	}
}

void ble_bcs_on_ble_evt(ble_evt_t const * p_ble_evt, void * p_context)
{
	ble_bcs_t * p_bcs = (ble_bcs_t *) p_context;

    switch (p_ble_evt->header.evt_id)
    {
        case BLE_GAP_EVT_CONNECTED:
            on_connect(p_bcs, p_ble_evt);
            break;

        case BLE_GAP_EVT_DISCONNECTED:
            on_disconnect(p_bcs, p_ble_evt);
            break;

        case BLE_GATTS_EVT_WRITE:
            on_write(p_bcs, p_ble_evt);
            break;

        default:
            // No implementation needed.
            break;
    }
}

/**@brief Add command characteristic.
 *
 * @param[in]   p_ble_bcs        Service structure.
 * @param[in]   p_ble_bcs_init   Information needed to initialize the service.
 *
 * @return      NRF_SUCCESS on success, otherwise an error code.
 */
static uint32_t command_char_add(ble_bcs_t * p_ble_bcs, const ble_bcs_init_t * p_ble_bcs_init)
{
    ble_gatts_char_md_t char_md;
    ble_gatts_attr_t    attr_char_value;
    ble_uuid_t          ble_uuid;
    ble_gatts_attr_md_t attr_md;
//	ble_gatts_attr_md_t cccd_md;
    
    memset(&char_md, 0, sizeof(char_md));
    char_md.char_props.write  = 1;
    char_md.char_props.notify = 0;
    char_md.p_char_user_desc  = NULL;
    char_md.p_char_pf         = NULL;
    char_md.p_user_desc_md    = NULL;
    char_md.p_cccd_md         = /*(p_ble_bcs->is_notification_supported) ? &cccd_md :*/ NULL;
    char_md.p_sccd_md         = NULL;
    
    ble_uuid.type = p_ble_bcs->uuid_type;
    ble_uuid.uuid = COMMAND_UUID_CHAR;
    
    memset(&attr_md, 0, sizeof(attr_md));
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
    BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);
    attr_md.vloc       = BLE_GATTS_VLOC_STACK;
    attr_md.rd_auth    = 0;
    attr_md.wr_auth    = 0;
    attr_md.vlen       = 1;
    
    memset(&attr_char_value, 0, sizeof(attr_char_value));
    attr_char_value.p_uuid       = &ble_uuid;
    attr_char_value.p_attr_md    = &attr_md;
    attr_char_value.init_len     = 1;
    attr_char_value.init_offs    = 0;
    attr_char_value.max_len      = COMMAND_UUID_CHAR_LEN;
    attr_char_value.p_value      = NULL;
    
    return sd_ble_gatts_characteristic_add(p_ble_bcs->service_handle, &char_md,
                                               &attr_char_value,
                                               &p_ble_bcs->command_handles);
}

/**@brief Add response characteristic.
 *
 * @param[in]   p_ble_bcs        Service structure.
 * @param[in]   p_ble_bcs_init   Information needed to initialize the service.
 *
 * @return      NRF_SUCCESS on success, otherwise an error code.
 */
static uint32_t response_char_add(ble_bcs_t * p_ble_bcs, const ble_bcs_init_t * p_ble_bcs_init, uint16_t max_len)
{
	ble_gatts_char_md_t char_md;
	ble_gatts_attr_t    attr_char_value;
	ble_uuid_t          ble_uuid;
	ble_gatts_attr_md_t attr_md;
	ble_gatts_attr_md_t cccd_md;

	if (p_ble_bcs->is_notification_supported)
	{
		memset(&cccd_md, 0, sizeof(cccd_md));

		// According to BAS_SPEC_V10, the read operation on cccd should be possible without
		// authentication.
		BLE_GAP_CONN_SEC_MODE_SET_OPEN(&cccd_md.read_perm);
		cccd_md.write_perm = p_ble_bcs_init->bcs_char_attr_md.cccd_write_perm;
		cccd_md.vloc       = BLE_GATTS_VLOC_STACK;
	}

	memset(&char_md, 0, sizeof(char_md));
	char_md.char_props.read   = 1;
	char_md.char_props.notify = 1;
	char_md.p_char_user_desc  = NULL;
	char_md.p_char_pf         = NULL;
	char_md.p_user_desc_md    = NULL;
	char_md.p_cccd_md         = (p_ble_bcs->is_notification_supported) ? &cccd_md : NULL;
	char_md.p_sccd_md         = NULL;

	ble_uuid.type = p_ble_bcs->uuid_type;
	ble_uuid.uuid = RESPONSE_UUID_CHAR;

	memset(&attr_md, 0, sizeof(attr_md));
	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.read_perm);
	BLE_GAP_CONN_SEC_MODE_SET_OPEN(&attr_md.write_perm);
	attr_md.vloc       = BLE_GATTS_VLOC_STACK;
	attr_md.rd_auth    = 0;
	attr_md.wr_auth    = 0;
	attr_md.vlen       = 0;

	memset(&attr_char_value, 0, sizeof(attr_char_value));
	attr_char_value.p_uuid       = &ble_uuid;
	attr_char_value.p_attr_md    = &attr_md;
	attr_char_value.init_len     = 1;
	attr_char_value.init_offs    = 0;
	attr_char_value.max_len      = max_len;
	attr_char_value.p_value      = NULL;

	return sd_ble_gatts_characteristic_add(p_ble_bcs->service_handle, &char_md,
												&attr_char_value,
												&p_ble_bcs->response_handles);
}

uint32_t ble_bcs_init(ble_bcs_t * p_ble_bcs, ble_bcs_init_t const * p_ble_bcs_init, uint16_t response_char_max_len)
{
	uint32_t   err_code;
	ble_uuid_t ble_uuid;

	p_ble_bcs->bcs_write_handler = p_ble_bcs_init->bcs_write_handler;

	ble_uuid128_t base_uuid = {BCS_UUID_BASE};
	p_ble_bcs->conn_handle = BLE_CONN_HANDLE_INVALID;
	err_code = sd_ble_uuid_vs_add(&base_uuid, &p_ble_bcs->uuid_type);
	p_ble_bcs->is_notification_supported = p_ble_bcs_init->support_notification;
	if (err_code != NRF_SUCCESS) {
		goto exit;
	}

	ble_uuid.type = p_ble_bcs->uuid_type;
	ble_uuid.uuid = BCS_UUID_SERVICE;
	err_code = sd_ble_gatts_service_add(BLE_GATTS_SRVC_TYPE_PRIMARY, &ble_uuid,	&p_ble_bcs->service_handle);
	if (err_code != NRF_SUCCESS) {
		goto exit;
	}

	/* Add command characteristic */
	err_code = command_char_add(p_ble_bcs, p_ble_bcs_init);
	if (err_code != NRF_SUCCESS) {
		goto exit;
	}

	/* Add response data characteristic */
	err_code = response_char_add(p_ble_bcs, p_ble_bcs_init, response_char_max_len);
	if (err_code != NRF_SUCCESS) {
		goto exit;
	}
	
	exit:

	return err_code;
}

uint32_t ble_bcs_status_update(ble_bcs_t * p_ble_bcs, uint8_t status)
{
    uint32_t err_code = NRF_SUCCESS;		
    ble_gatts_value_t gatts_value;
	
    // Initialize value struct.
	memset(&gatts_value, 0, sizeof(gatts_value));

	gatts_value.len     = 1;
	gatts_value.offset  = 0;
	gatts_value.p_value = &status;

	// Update database
	err_code = sd_ble_gatts_value_set(p_ble_bcs->conn_handle, p_ble_bcs->response_handles.value_handle, &gatts_value);
	if (err_code != NRF_SUCCESS) {
		return err_code;
	}

	// Send value if connected and notifying
	if ((p_ble_bcs->conn_handle != BLE_CONN_HANDLE_INVALID) && p_ble_bcs->is_notification_supported) {
		ble_gatts_hvx_params_t hvx_params;

		memset(&hvx_params, 0, sizeof(hvx_params));
		hvx_params.handle = p_ble_bcs->response_handles.value_handle;
		hvx_params.type   = BLE_GATT_HVX_NOTIFICATION;
		hvx_params.offset = 0;
		hvx_params.p_len  = &gatts_value.len;
		hvx_params.p_data = gatts_value.p_value;
		err_code = sd_ble_gatts_hvx(p_ble_bcs->conn_handle, &hvx_params);
	}
	else {
		err_code = NRF_ERROR_INVALID_STATE;
	}

    return err_code;
}
